/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.filechooser';

angular.module(moduleName, [
  'angular-loading-bar'
]);
angular.module(moduleName).directive('faFileChooser', FileChooser);

FileChooser.$inject = [
  '$window',
  'ADMIN_APP_PREFIX',
  'API_PREFIX',
  'cfpLoadingBar',
  'Files'
];

function FileChooser($window,
                     ADMIN_APP_PREFIX,
                     API_PREFIX,
                     cfpLoadingBar,
                     Files) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX + 'components/filechooser/fa.filechooser.tpl.html',
    scope: {
      articleFiles: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.fileApi = API_PREFIX + '/files/';
    scope.files = [];
    scope.addFile = addFile;

    activate();

    function activate() {
      cfpLoadingBar.start();
      Files.getAll(function(files) {
        scope.files = files;
        cfpLoadingBar.complete();
      });
    }

    function addFile(file) {
//       if (!scope.articleLinks) {
//         scope.articleLinks = [];
//       }
//       scope.articleLinks.push(link);
     copyToClipboard(scope.fileApi + file.id);
    }

    function copyToClipboard(text) {
      $window.prompt("In Zwischenablage kopieren: STRG + C, Enter", text);
    }
  }
}

})();
/* eof */
/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.imagechooser';

angular.module(moduleName, [
  'angular-loading-bar'
]);
angular.module(moduleName).directive('faImageChooser', ImageChooser);

ImageChooser.$inject = [
  'ADMIN_APP_PREFIX',
  'API_PREFIX',
  'cfpLoadingBar',
  'Images'
];

function ImageChooser(ADMIN_APP_PREFIX,
                      API_PREFIX,
                      cfpLoadingBar,
                      Images) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX +
      'components/imagechooser/fa.imagechooser.tpl.html',
    scope: {
      myImage: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.imageApi = API_PREFIX + '/images/';
    scope.images = [];
    scope.addImage = addImage;

    activate();

    function activate() {
      cfpLoadingBar.start();
      Images.getAll(function(images) {
        scope.images = images;
        cfpLoadingBar.complete();
      });
    }

    function addImage(image) {
      //scope.myImage = image.id;
      scope.myImage = image;
    }
  }
}

})();
/* eof */
/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.linkchooser';

angular.module(moduleName, [
  'angular-loading-bar'
]);
angular.module(moduleName).directive('faLinkChooser', LinkChooser);

LinkChooser.$inject = [
  '$window',
  'ADMIN_APP_PREFIX',
  'API_PREFIX',
  'cfpLoadingBar',
  'Links'
];

function LinkChooser($window,
                     ADMIN_APP_PREFIX,
                     API_PREFIX,
                     cfpLoadingBar,
                     Links) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX +
      'components/linkchooser/fa.linkchooser.tpl.html',
    scope: {
      articleLinks: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.linkApi = API_PREFIX + '/links/';
    scope.links = [];
    scope.addLink = addLink;
    scope.followLink = followLink;

    activate();

    function activate() {
      cfpLoadingBar.start();
      Links.getAll(function(links) {
        scope.links = links;
        cfpLoadingBar.complete();
      });
    }

    function addLink(link) {
//       if (!scope.articleLinks) {
//         scope.articleLinks = [];
//       }
//       scope.articleLinks.push(link);
      copyToClipboard(link.url);
    }

    function copyToClipboard(text) {
      $window.prompt("In Zwischenablage kopieren: STRG + C, Enter", text);
    }

    function followLink(link) {
      $window.open(link.url, '_blank');
    }
  }
}

})();
/* eof */
/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.articlepanel';

angular.module(moduleName, []);
angular.module(moduleName).directive('faArticlePanel', ArticlePanel);

ArticlePanel.$inject = [
  'ADMIN_APP_PREFIX',
  'Messages'
];

function ArticlePanel(ADMIN_APP_PREFIX, Messages) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX + 'components/articlepanel/fa.articlepanel.tpl.html',
    scope: {
      articles: '=',
      heading: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.edit = function(article) {
      Messages.broadcast('article:edit', {
        article: article
      });
    }

    scope.remove = function(index, article) {
      Messages.broadcast('article:remove', {
        article: article,
        index: index,
      });
    }
  }
}

})();
/* eof */
 
/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.linkcreator';

angular.module(moduleName, [
  'angular-loading-bar'
]);
angular.module(moduleName).directive('faLinkCreator', LinkCreator);

LinkCreator.$inject = [
  '$window',
  'ADMIN_APP_PREFIX',
  'API_PREFIX',
  'cfpLoadingBar',
  'Links'
];

function LinkCreator($window,
                     ADMIN_APP_PREFIX,
                     API_PREFIX,
                     cfpLoadingBar,
                     Links) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX +
      'components/linkcreator/fa.linkcreator.tpl.html',
    scope: {
      articleLinks: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.linkApi = API_PREFIX + '/links/';
    scope.newLink = {
      name: '',
      url: ''
    };
    scope.saveLink = saveLink;
    scope.resetLink = resetLink;
    scope.toggleFiles = toggleFiles;

    function saveLink() {
      //console.log(scope.newLink);
      if (scope.newLink.name != '' && scope.newLink.url != '') {
//         Links.create(scope.newLink, function(link) {
//           if (!scope.articleLinks) {
//             scope.articleLinks = [];
//           }
//           scope.articleLinks.push(link);
//           resetLink();
//         });
        if (!scope.articleLinks) {
          scope.articleLinks = [];
        }
        scope.articleLinks.push(scope.newLink);
        resetLink();
      }
    }

    function resetLink() {
      scope.newLink = {
        name: '',
        url: ''
      };
      scope.linkform.$setUntouched();
      scope.linkform.$setPristine();
      scope.linkform.$rollbackViewValue();
    }

    function toggleFiles() {
      scope.filesVisible = !scope.filesVisible;
    }
  }
}

})();
/* eof */
(function() {
'use strict';

angular
.module('fa.admin.articles')
.factory('ArticlesActions', ArticlesActions);

ArticlesActions.$inject = [];

function ArticlesActions() {
  return {
    sayHello: sayHello
  };

  function sayHello() {
    //console.log('Hello from ArticlesActions');
  }

  function create() {

  }

  function read() {

  }

  function update() {

  }

  function remove() {

  }
}


})();
/* bof */
(function() {
'use strict';

var moduleName = 'fa.admin.articles';
angular.module(moduleName, [
  'ngSanitize',
  'ui.tinymce',
  'fa.shared.constants'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('ArticleController', ArticleController);


ArticleController.$inject = [
  '$state',
  '$window',
  'Articles',
  'ArticleLinks',
  'Messages'
];

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('articles', {
    url: '/articles',
    templateUrl: ADMIN_APP_PREFIX + 'pages/articles/articles.tpl.html',
    controller: 'ArticleController as vm'
  })
  .state('articles.editor', {
    url: '/editor',
    template: '<fa-articles-page-editor></fa-articles-page-editor>',
    params: {data: null}
  })
  .state('articles.list', {
    url: '/list',
    template: '<fa-articles-page-list></fa-articles-page-list>',
  })
  ;
}

/******************************************************************************
 * ArticleController
 ******************************************************************************/
function ArticleController($state,
                           $window,
                           Articles,
                           ArticleLinks,
                           Messages) {
  var vm = this;
  vm.remove = remove;

  activate();

  function activate() {

    Messages.listen('article:edit', function(articleData) {
      $state.go('articles.editor', {data: articleData}, {});
    });

    Messages.listen('article:remove', function onSuccess(data) {
      remove(data.index, data.article);
      //$state.go('articles.editor');
    });
  }

  function remove(index, article) {
    if($window.confirm('Diesen Artikel wirklich löschen?')) {
      Articles.remove(article.id, function() {
        ArticleLinks.removeArticle(article.id, function() {
          Messages.broadcast('articles:updated', {});
        });
      });
    }
  }
}

})();
/* eof */
/* bof */
(function() {
'use strict';

var moduleName = 'fa.admin.articles';

angular.module(moduleName).factory('ArticleTemplates', ArticleTemplates);

ArticleTemplates.$inject = [];

function ArticleTemplates() {

  return {
    getTypes: getTypes,
    getTemplate: getTemplate
  };

  function getTypes(cb) {
    cb([
      {
        name: 'market',
        text: 'Markt'
      }, {
        name: 'personal',
        text: 'Personal'
      }, {
        name: 'protocol',
        text: 'Protokoll'
      }, {
        name: 'other',
        text: 'Sonstige'
      }
    ]);
  }

  function getTemplate(tplType, cb) {
    var tpl = {};
    switch (tplType) {
      case 'market': {
        tpl = getMarketTemplate();
        break;
      }
      case 'personal': {
        tpl = getPersonalTemplate();
        break;
      }
      case 'protocol': {
        tpl = getProtocolTemplate();
        break;
      }
      case 'other': {
        tpl = getOtherTemplate();
        break;
      }
      default: {
        tpl = getOtherTemplate();
      }
    }
    cb(tpl);
  }

  function getAbstractTemplate() {
    return {
      title: '',
      type: '',
      shortText: '',
      longText: '',
      createdAt: '',
      updatedAt: '',
      department: {id: 0},
      files: []
    };
  }

  function getMarketTemplate() {
    var tpl = getAbstractTemplate();
    tpl.title = 'Verkaufe Gegenstand';
    tpl.shortText = 'Verkaufe folgende Gegenstände: (Gegenstände).<br/> Tel: (Durchwahl)';
    tpl.type = 'market';
    return tpl;
  }

  function getPersonalTemplate() {
    var tpl = getAbstractTemplate();
    tpl.title = 'Personalinformationen';
    tpl.type = 'personal';
    tpl.shortText = 'Dokumentation der Personaländerungen.';
    return tpl;
  }

  function getProtocolTemplate() {
    var today = moment().format('L');
    var tpl = getAbstractTemplate();
    tpl.title = 'Dienstbesprechung SG ...';
    tpl.shortText = 'Protokoll der Dienstbesprechung vom ' + today;
    tpl.type = 'protocol';
    tpl.department = null;
    return tpl;
  }

  function getOtherTemplate() {
    var tpl = getAbstractTemplate();
    tpl.title = 'Sonstiges';
    tpl.type = 'other';
    return tpl;
  }

}

})();
/* eof */
(function() {
'use strict';

angular
.module('fa.admin.articles')
.directive('faArticlesPageEditor', ArticlesPageEditor);

ArticlesPageEditor.$inject = ['ADMIN_APP_PREFIX'];

function ArticlesPageEditor(ADMIN_APP_PREFIX) {

  ArticlePageEditorController.$inject = [
    '$sanitize',
    '$scope',
    '$state',
    '$stateParams',
    '$window',
    'Articles',
    'ArticlesActions',
    'ArticleLinks',
    'ArticleTemplates',
    'Departments',
    'Links'
  ];

 /******************************************************************************
  * ArticlePageEditorController
  *****************************************************************************/
  function ArticlePageEditorController($sanitize,
                                       $scope,
                                       $state,
                                       $stateParams,
                                       $window,
                                       Articles,
                                       ArticlesActions,
                                       ArticleLinks,
                                       ArticleTemplates,
                                       Departments,
                                       Links) {

    var vm = this;
    vm.article = {};
    vm.articleTypes = [];
    vm.departments = [];
    vm.selectedType = {};
    vm.updating = false;
    vm.resultMessage = {
      type: '',
      text: ''
    }

    vm.cancel = cancel;
    vm.create = create;
    vm.followLink = followLink;
    vm.hideMessage = hideMessage;
    vm.update = update;
    vm.updateDepartment = updateDepartment;
    vm.updateForm = updateForm;
    vm.removeLink = removeLink;
    vm.showMessage = showMessage;

    activate();

    function activate() {
      if ($stateParams.data) {
        vm.article = $stateParams.data.article;
        vm.updating = true;
      }
      ArticleTemplates.getTypes(function(types) {
        vm.articleTypes = types;
      });
      Departments.getAll(function(departments) {
        vm.departments = departments;
      });
    }

    function cancel() {
      vm.updating = false;
      reset();
    }

    function create() {
      var sanitizedArticle = sanitizeArticle(vm.article);
      console.log(sanitizedArticle);
      Articles
      .create(sanitizedArticle)
      .then((article) => {
        showMessage('is-success', 'Speichern erfolgreich.');
        reset();
      })
      .catch(function(err) {
        console.error(err);
        showMessage(
          'is-danger',
          'Ein Fehler ist aufgetreten. Speichern nicht erfolgreich.'
        );
      });
    }

    function sanitizeArticle(article) {
      var sanitizedProps = {
        title: encodeURIComponent(article.title),
        shortText : encodeURIComponent(article.shortText)
      };
      var sanitizedArticle = {};
      angular.merge(sanitizedArticle, article, sanitizedProps);
      return sanitizedArticle;
    }

    function update() {
      vm.article.updatedAt = Date.now();
      var sanitizedArticle = sanitizeArticle(vm.article);
      Articles
      .update(sanitizedArticle)
      .then((article) => {
        showMessage('is-success', 'Speichern erfolgreich.');
        vm.updating = false;
        reset();
      })
      .catch((err) => {
        console.error(err);
        showMessage(
          'is-danger',
          'Ein Fehler ist aufgetreten. Änderung nicht erfolgreich.'
        );
      });
    }

    function createLinksFromArticle(response) {
      var newArticle = response.data;
      var newLinks = [];
      // create links
      vm.article.links.forEach(function(item) {
        Links.create(item, function(createdLink) {
          newLinks.push(createdLink);
        });
      });
      newArticle.links = newLinks;
      return newArticle;
    }

    function associateArticleWithLinks(article) {
      return ArticleLinks.appendLinks(article.id, article.links);
    }

    function edit(article) {
      ArticleLinks.getLinksForArticle(article.id, function(links) {
        article.links = links;
        vm.article = article;
        vm.updating = true;
      });
    }

    function followLink(link) {
      $window.open(link.url, '_blank');
    }

    function hideMessage() {
      vm.resultMessage = {type: '', text: ''}
    }

    function removeLink(index, link) {
      vm.article.links.splice(index, 1);
    }

    function reset() {
      vm.article = {
        title: '',
        type: '',
        shortText: '',
        longText: '',
        createdAt: '',
        updatedAt: '',
        department: {id:0},
        links: []
      };
      vm.article.links = [];
      vm.filesVisible = false;
      vm.selectedType = {};
      $scope.form.$setUntouched();
      $scope.form.$setPristine();
      $scope.form.$rollbackViewValue();
    }

    function toggleFiles() {
      vm.filesVisible = !vm.filesVisible;
    }

    function updateDepartment(department) {
      vm.article.title =
       'Dienstbesprechung des SG ' +
       vm.article.department.numeral;
    }

    function updateForm() {
      //console.log(vm.selectedType);
      ArticleTemplates.getTemplate(
        vm.selectedType.name,
        function(template) {
          //console.log(template);
          vm.article = template;
          vm.article.createdAt = Date.now();
          vm.article.updatedAt = Date.now();
          vm.article.links = [];
        }
      );
    }

    function showMessage(type, msg) {
      vm.resultMessage = {type: type, text: msg};
    }
  }

  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX + 'pages/articles/articles.editor.tpl.html',
    scope: {},
    controller: ArticlePageEditorController,
    controllerAs: 'vm'
  };
}

})();(function() {
'use strict';

angular
.module('fa.admin.articles')
.directive('faArticlesPageList', ArticlesPageList);

ArticlesPageList.$inject = ['ADMIN_APP_PREFIX', 'Articles'];

function ArticlesPageList(ADMIN_APP_PREFIX, Articles) {

  ArticlePageListController.$inject = ['Articles', 'Messages'];

  function ArticlePageListController(Articles, Messages) {
    var vm = this;
    vm.articles = [];
    vm.edit = edit;
    vm.heading = 'Vorhandene Artikel';
    vm.remove = remove;
    vm.today = '';

    activate();

    function activate() {
      Messages.listen('articles:updated', function() {
        getAll();
      });
      getAll();
    }

    function edit(article) {
      Messages.broadcast('article:edit', {
        article: article
      });
    }

    function getAll() {
      Articles.getAll(articles => {
        vm.articles = articles;
      });
    }

    function remove(article, index) {
      Messages.broadcast('article:remove', {
        article: article,
        index: index,
      });
    }
  }

  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX + 'pages/articles/articles.list.tpl.html',
    scope: {},
    controller: ArticlePageListController,
    controllerAs: 'vm'
  };

}

})();/* bof */
(function() {

angular.module('fa.admin.events', [
  'fa.shared.services',
  'fa.shared.constants'
]);

angular
.module('fa.admin.events')
.config(Routes);

angular
.module('fa.admin.events')
.controller('EventsController', EventsController);

angular
.module('fa.admin.events')
.directive('faCreateEvent', FACreateEvent);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

EventsController.$inject = [
  '$window',
  'CalendarService',
  'EventService',
  'RoomService'
];

FACreateEvent.$inject = [
  'ADMIN_APP_PREFIX',
  'EventService',
  'RoomService'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('events', {
    url: '/events',
    templateUrl: ADMIN_APP_PREFIX + 'pages/events/events.tpl.html',
    controller: 'EventsController as vm'
    //template: '<events-app></events-app>'
  });
}

function EventsController($window,
                          CalendarService,
                          EventService,
                          RoomService) {
  var vm = this;
  vm.editable = false;
  vm.saving = false;
  vm.updating = false;
  vm.editEvent = false,
  vm.events = [];
  vm.rooms = [];

  vm.currentEvent = {};
  vm.message = '';
  vm.years = [];

  vm.monthsInYear = [];
  vm.currentMonth = '1';
  vm.currentYear = '2016';

  vm.refreshList = refreshList;
  vm.getAllEvents = getAllEvents;
  vm.toggleEditable = toggleEditable;
  vm.removeEvent = removeEvent;
  vm.editEvent = editEvent;
  vm.cancel = cancel;

  activate();

  function activate() {
    initYears();
    initMonths();
    resetEvent();
    getAllEvents();
    getRooms();
  }

  function initYears() {
    var curr = moment().year();
    vm.years = [curr, curr + 1, curr + 2, curr + 3, curr + 4];
    vm.currentYear = curr;
    vm.currentMonth = moment().month();
  }

  function initMonths(cb) {
    CalendarService.getMonthsInYear(function(months) {
      vm.monthsInYear = months;
      // set selected month to current month using moment.js
      vm.currentMonth = moment().month();
      if (cb) { cb(); }
    });
  }

  function refreshList() {
    EventService.getByMonth(
      parseInt(vm.currentMonth),
      parseInt(vm.currentYear),
      function(result) {
        vm.events = result;
      }
    );
  }

  function toggleEditable() {
    vm.editable = !vm.editable;
    vm.saving = true;
    vm.updating = false;
  }

  function getAllEvents() {
    EventService.getAll(function(result) {
      vm.events = result;
    });
  }

  function getRooms() {
    RoomService.getAll(function(rooms) {
      vm.rooms = rooms;
    });
  }

  function resetEvent() {
    vm.currentEvent = {
      name: '',
      contact: '',
      date: '',
      timeBegin: '',
      timeEnd: '',
      roomId: 1
    };
  }

  function removeEvent(index, event) {
    if($window.confirm('Diese Veranstaltung wirklich löschen?')) {
      EventService.remove(index, event, function(events) {
        vm.getAllEvents();
      });
    }
  }

  function editEvent(index, event) {
    vm.editable = true;
    vm.saving = false;
    vm.updating = true;
    var timeBegin = getTimeString(moment(parseInt(event.timeBegin)));
    var timeEnd = getTimeString(moment(parseInt(event.timeEnd)));
    var dateStart = moment(parseInt(event.date));
    var dateString = getDateString(dateStart);
    vm.currentEvent = {
      id: event.id,
      name: event.name,
      contact: event.contact,
      day: parseInt(event.day),
      month: parseInt(event.month),
      year: parseInt(event.year),
      timeBegin: timeBegin,
      timeEnd: timeEnd,
      room: findRoomById(event.roomId),
      date: {
        start: dateString
      }
    };
  }

  function getTimeString(time) {
    var timeString = (time.hour() < 10 ? '0' : '');
    timeString += time.hour() + ':';
    timeString += (time.minute() < 10 ? '0' : '');
    timeString += time.minute();
    return timeString;
  }

  function getDateString(date) {
    var month = (date.month() < 10 ? '0' : '');
    month += (date.month() + 1);
    var dateString =
      date.date() + '.'+
      month + '.' +
      date.year();
    return dateString;
  }

  function cancel() {
    vm.updating = false;
    vm.saving = false;
    vm.editable = false;
    resetEvent();
  }

  function findRoomById(roomId) {
    var result = null;
    for (var i = 0; i< vm.rooms.length; i++) {
      if (vm.rooms[i].id == roomId) {
        result = vm.rooms[i];
        break;
      }
    }
    return result;
  }
}

/* DIRECTIVE fa-create-event */
function FACreateEvent(ADMIN_APP_PREFIX, EventService, RoomService) {
  return {
    templateUrl: ADMIN_APP_PREFIX + 'pages/events/create-event.tpl.html',
    scope: {
      myEvent: '=',
      rooms: '=',
      creating: '=',
      updating: '=',
      refresh: '='
    },
    restrict: 'E',
    link: link
  };

  function link(scope, element, attrs) {
    scope.saveEvent = saveEvent;
    scope.updateEvent = updateEvent;
    scope.cancel = cancel;
    scope.multiDay = false;

    //scope.myEvent.date = {};

    function saveEvent() {
      if (scope.multiDay) {
        saveMultiDayEvent();
      } else {
        saveSingleDayEvent();
      }

    }

    function processFormContent() {
      scope.myEvent.date = createEventDate(scope.myEvent.date.start);
      scope.myEvent.timeBegin = createEventBegin(scope.myEvent.timeBegin);
      scope.myEvent.timeEnd = createEventEnd(scope.myEvent.timeEnd);
      scope.myEvent.roomId = scope.myEvent.room.id;
    }

    function saveSingleDayEvent() {
      console.log('saving single day event...');
      processFormContent();

      EventService.save(scope.myEvent, function() {
        resetForm();
        scope.refresh();
      });
    }

    function saveMultiDayEvent() {
      console.log('saving multi day event...');
      console.log(scope.myEvent);
    }

    function updateEvent() {
      console.log('updating event...');
      processFormContent();
      EventService.update(scope.myEvent, function() {
        resetForm();
        scope.refresh();
      });
    }

    function cancel() {
      resetForm();
    }

    /**
     * Creates a date object from the given date string.
     * @param dateString - a date string in format dd.mm.yyyy
     */
    function createEventDate(dateString) {
      var parts = dateString.split('.'); // [day, month, year]
      scope.myEvent.day = parts[0];
      scope.myEvent.month = parts[1];
      scope.myEvent.year = parts[2];

      return new Date(
        scope.myEvent.year,
        scope.myEvent.month - 1,
        scope.myEvent.day,
        0,0,0,0);
    }

    /**
     * Creates a date object from the given time string.
     * @param timeString - a time string in format HH:MM
     */
    function createEventBegin(timeString) {
      var parts = timeString.split(':'); // [hours, minutes]

      if (!scope.myEvent.begin) {
        scope.myEvent.begin = {};
      }

      scope.myEvent.begin.hour = parts[0];
      scope.myEvent.begin.minute = parts[1];

      return new Date(
        scope.myEvent.year,
        scope.myEvent.month -1,
        scope.myEvent.day,
        scope.myEvent.begin.hour,
        scope.myEvent.begin.minute,
        0,0);
    }

    /**
     * Creates a date object from the given time string.
     * @param timeString - a time string in format HH:MM
     */
    function createEventEnd(timeString) {
      var parts = timeString.split(':'); // [hours, minutes]

      if (!scope.myEvent.end) {
        scope.myEvent.end = {};
      }

      scope.myEvent.end.hour = parts[0];
      scope.myEvent.end.minute = parts[1];

      return new Date(
        scope.myEvent.year,
        scope.myEvent.month - 1,
        scope.myEvent.day,
        scope.myEvent.end.hour,
        scope.myEvent.end.minute,
        0,0);
    }

    function resetForm() {
      scope.myEvent = {
        id: '',
        name: '',
        contact: '',
        begin: {},
        end:{},
        day: '',
        month: '',
        year: '',
        room: {id:1}
      };
      scope.form.$setUntouched();
      scope.form.$setPristine();
      scope.form.$rollbackViewValue();
    }
  }
}

})();
/* eof */
(function() {
const moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsApp', {

  bindings: {},

  template: `
    <events-app-heading title="Veranstaltungen"></events-app-heading>
    <div class="columns">
      <div class="column">
        <events-app-controls></events-app-controls>
      </div>
    </div>
    <div class="columns">
      <div class="column">
        <events-app-calendar month="$ctrl.month"
                             events="$ctrl.events">
        </events-app-calendar>
        <events-app-modal modal-status="$ctrl.modalStatus"
                          event="$ctrl.event">
        </events-app-modal>
      </div>
    </div>
  `,

  controller: [
    '$window', 'Events', 'Messages',
    function($window, Events, Messages) {

      let ctrl = this;
      ctrl.events = [];
      ctrl.modalStatus = '';
      ctrl.month = {};

      ctrl.getEvents = getEvents;

      ctrl.$onInit = () => {
        ctrl.month = moment();
        //ctrl.day = ctrl.month;
        getEvents();
      };

      function getEvents() {
        Events.get(ctrl.month.month() + 1, ctrl.month.year())
        .then(response => {
          ctrl.events = response.data;
        })
        .catch(handleErrors);
      }

      function handleErrors(error) {
        console.error(error);
      }

      function update(obj, newData) {
//         console.log('##### update state #####');
//         console.log('old state');
//         console.log(obj);
//         console.log('extension');
//         console.log(newData);
        let dst = {};
        angular.merge(dst, obj, newData);
//         console.log('new state');
//         console.log(dst);
        return dst;
      }

      // register event listeners
      Messages.listen('events:lastMonth:request', messageData => {
        ctrl.month = moment(ctrl.month).subtract(1, 'month');
        getEvents();
      });

      Messages.listen('events:nextMonth:request', messageData => {
        ctrl.month = moment(ctrl.month).add(1, 'month');
        getEvents();
      });

      Messages.listen('events:today:request', messageData => {
        ctrl.month = moment();
        getEvents();
      });

      Messages.listen('events:create:request', data => {
        ctrl.modalStatus = 'is-active';
        // reset event object on create request to clear form
        ctrl.event = {};
        ctrl.event.day = ctrl.month.date(data.day.nr);
      });

      Messages.listen('events:edit:request', data => {
        console.log(data);
        ctrl.modalStatus = 'is-active';
        ctrl.event = data.event;
        ctrl.event.day = ctrl.month.date(data.day.nr);
      });

      Messages.listen('events:form:hide:request', data => {
        ctrl.modalStatus = 'is-inactive';
        ctrl.event = {};
      });

      Messages.listen('events:form:name:update', data => {
        //console.log(data);
        ctrl.event = update(ctrl.event, data);
      });

      Messages.listen('events:form:contact:update', data => {
        ctrl.event = update(ctrl.event, data);
      });

      Messages.listen('events:form:isFullDay:update', data => {
        //console.log('##### reacting to fullday update #####')
        let timeData = {};
        if (data.isFullDay) {
          let timeBeginMoment = moment(ctrl.event.day).hours(8).minutes(0);
          let timeEndMoment = moment(ctrl.event.day).hours(16).minutes(0);
          let time = {
            timeBegin: timeBeginMoment,
            timeEnd: timeEndMoment
          }
          timeData = update(data, time);
        } else {
          timeData = update(data, {});
        }
        ctrl.event = update(ctrl.event, timeData);
      });

      Messages.listen('events:form:time:update', data => {
        console.log(data);
      });
    }
  ]

});

})();(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppHeading', {

  bindings: {
    title: '@'
  },
  template: `
    <div class="columns">
      <div class="column">
        <div class="heading">
          <h1 class="title">{{$ctrl.title}}</h1>
        </div>
      </div>
    </div>
  `
});

})();(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppControls', {

  bindings: {}

  , template: `
    <div class="tabs is-centered is-fullwidth">
      <ul>
        <li>
          <a ng-click="$ctrl.lastMonth()">
            <span class="icon is-small">
              <i class="fa fa-chevron-left"></i>
            </span>
            <span>zurück</span>
          </a>
        </li>
        <li>
          <a ng-click="$ctrl.today()">
            <span class="icon is-small">
              <i class="fa fa-chevron-down"></i>
            </span>
            <span>Heute</span>
          </a>
        </li>
        <li>
          <a ng-click="$ctrl.nextMonth()">
            <span class="icon is-small">
              <i class="fa fa-chevron-right"></i>
            </span>
            <span>vor</span>
          </a>
        </li>
      </ul>
    </div>
  `

  , controller: [ 'Messages', function(Messages) {

    this.lastMonth = lastMonth;
    this.nextMonth = nextMonth;
    this.today = today;

    function lastMonth() {
      Messages.broadcast('events:lastMonth:request', {});
    }

    function nextMonth() {
      Messages.broadcast('events:nextMonth:request', {});
    }

    function today() {
      Messages.broadcast('events:today:request', {});
    }

  }]

});

})();(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppCalendar', {

  bindings: {
    month: '<'
  , events: '<'
  }

  , template: `
    <h2 class="title has-text-centered">{{$ctrl.month.format('MMMM YYYY')}}</h2>

    <div class="is-fullwidth">
      <ul class="weekdays">
        <li ng-repeat="weekday in $ctrl.weekdays">{{weekday}}</li>
      </ul>
    </div>

    <div class="is-fullwidth">

      <div class="columns is-multiline is-gapless"
           id="calendar">

        <div class="column" ng-repeat="day in $ctrl.daysInMonth">

          <events-app-day day="day"></events-app-day>

        </div><!-- .column -->

      </div><!-- .columns -->

    </div><!-- .is-fullwidth -->
  `

  , controller: [ 'Messages', function(Messages) {

    var ctrl = this;
    ctrl.weekdays = [];
    ctrl.daysInMonth = [];

    ctrl.$onInit = () => {
      ctrl.weekdays = [
        'Montag'
      , 'Dienstag'
      , 'Mittwoch'
      , 'Donnerstag'
      , 'Freitag'
      , 'Samstag'
      , 'Sonntag'
      ];

      //console.log(ctrl.events);
    };

    ctrl.$onChanges = (changes) => {
      if (changes.month) {
        ctrl.daysInMonth =
          createDaysArray(changes.month.currentValue);
      }
      if (changes.events) {
        let arr = combineDaysAndEvents(
          ctrl.daysInMonth,
          changes.events.currentValue
        );
        ctrl.daysInMonth = arr;
      }
    }

  }]

});

function createDaysArray(month) {
  let result = [];

  result = createWeekdayPadding(result, month);
  result = createDays(result, month);

  return result;
}

function createWeekdayPadding(array, month) {
  let res = [].concat(array);
  let weekdayOfFirstInMonth = month.date(1).weekday();
  if (weekdayOfFirstInMonth != 0) {
    for (let i = 0; i < weekdayOfFirstInMonth; i++) {
      res.push({nr: 0});
    }
  }
  return res;
}

function createDays(array, month) {
  let res = [].concat(array);
  let daysInMonth = month.daysInMonth();
  for(let i = 1; i <= daysInMonth; i++) {
    res.push({nr: i});
  }
  return res;
}

function combineDaysAndEvents(days, events) {
  let result = days.map(day => {
    let newDay = angular.copy(day);
    newDay.events = [];

    events.forEach(event => {
      if (parseInt(event.day) === day.nr) {
        newDay.events.push(event);
      }
    });
    return newDay;
  });
  return result;
}

})();(function() {
const moduleName = 'fa.admin.events';

angular.module(moduleName).factory('Events', Events);

Events.$inject = ['$http', 'API_PREFIX'];

function Events($http, API_PREFIX) {
  return {
    get : get,
    getAll : getAll
  };

  function get(month, year) {
    let req = {
      method: 'GET',
      url: API_PREFIX + '/events?month=' + month + '&year=' + year
    };
    return $http(req);
  }

  function getAll() {
    let req = {
      method: 'GET',
      url: API_PREFIX + '/events'
    };
    return $http(req);
  }
}

})();(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppForm', {

  bindings: {
    event: '<'
  },

  template: `
    <div class="heading">
      <h1 class="title">
      Termin {{$ctrl.message}} am
      <span ng-bind="::$ctrl.event.day.format('DD.MM.YYYY')"></span>
      </h1>
    </div>

    <br/>

    <form class="event-form"
          name="form"
          novalidate>

      <div class="columns">
        <div class="column">

          <events-app-form-name event-name="$ctrl.event.name">
          </events-app-form-name>

          <events-app-form-contact event-contact="$ctrl.event.contact">
          </events-app-form-contact>

          <events-app-form-full-day is-full-day="$ctrl.event.isFullDay">
          </events-app-form-full-day>

          <div class="columns">

            <div class="column">
              <events-app-form-time
                field="timeBegin"
                is-full-day="$ctrl.event.isFullDay"
                label="Beginn"
                time-string="$ctrl.event.timeBegin">
              </events-app-form-time>
            </div>

            <div class="column">
              <events-app-form-time
                field="timeEnd"
                is-full-day="$ctrl.event.isFullDay"
                label="Ende"
                time-string="$ctrl.event.timeEnd">
              </events-app-form-time>
            </div>
          </div>


        </div>
      </div>
    </form>
  `,

  controller: [ 'Messages', function(Messages) {

    let ctrl = this;
    ctrl.message = 'anlegen';

    ctrl.$onChanges = changes => {
      if (changes.event.currentValue) {
        if (changes.event.currentValue.id) {
          ctrl.message = 'ändern';
        } else {
          ctrl.message = 'anlegen';
        }
      }
    };

  }]

});

})();(function() {
  var moduleName = 'fa.admin.events';

  angular.module(moduleName).component('eventsAppFormTime', {
    bindings: {
      field: '@',
      isFullDay: '<',
      label: '@',
      timeString: '<'
    },

    template: `
      <ng-form name="timeForm">
        <label
          class="label"
          ng-bind="::$ctrl.label">
        </label>
        <p class="control has-icon has-icon-right"
           style="margin-bottom: 10px;">
          <input
            type="text"
            name="timeTF"
            class="input"
            placeholder="Zeit (Format: HH:MM)"
            required
            ng-disabled="$ctrl.isFullDay"
            ng-class="!timeForm.timeTF.$touched ? ''
                      : (timeForm.timeTF.$valid ? 'is-success' : 'is-danger')"
            ng-required="true"
            ng-model="$ctrl.timeString"
            ng-model-options="{updateOn: 'blur'}"
            ng-change="$ctrl.update()"
            ng-pattern="/^(0[1-9]|1[0-9]|2[0-3]):([0-5][0-9])$/">
          <i
            class="fa fa-warning"
            ng-if="timeForm.timeTF.$touched && !timeForm.timeTF.$valid">
          </i>
          <span
            class="help is-danger is-pulled-right"
            ng-if="timeForm.timeTF.$touched && !timeForm.timeTF.$valid">
            Zeit fehlt oder hat falsches Format.
          </span>
        </p>
      </ng-form>
    `,

    controller: ['$scope', 'Messages', function($scope, Messages) {
      var ctrl = this;
      ctrl.field = 'time';
      ctrl.isFullDay = false;
      ctrl.label = 'Zeit';
      ctrl.timeString = '08:00';
      ctrl.hours = '08';
      ctrl.minutes = '00';
      ctrl.update = update;

      ctrl.$onChanges = changes => {
        //console.log('TimeForm says:');
        //console.log(changes);
        if (changes.timeString) {
          if (changes.timeString.currentValue) {
            let newMoment = moment(changes.timeString.currentValue);
            ctrl.hours = getPaddedValue(newMoment.hours());
            ctrl.minutes = getPaddedValue(newMoment.minutes());
            ctrl.timeString = ctrl.hours + ':' + ctrl.minutes;
          }
        }
      };

      function update() {
        if (ctrl.timeString) {
          ctrl.hours = ctrl.timeString.split(':')[0];
          ctrl.minutes = ctrl.timeString.split(':')[1];
        }
        if ($scope.timeForm.$valid) {
          Messages.broadcast('events:form:time:update', {
            field: ctrl.field,
            hours: ctrl.hours,
            minutes: ctrl.minutes
          });
        }
      };

      function getPaddedValue(number) {
        return (number < 10) ? '0' + number : number;
      }
    }]
  });

})();
(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppModal', {

  bindings: {
    modalStatus: '<',
    event: '<'
  },

  template: `
    <div class="modal"
         ng-class="$ctrl.modalStatus"
         id="event-dialog">
      <div class="modal-background"></div>
      <div class="modal-card">
        <header class="modal-card-head">
          <p class="modal-card-title">Veranstaltung</p>
          <button class="delete" ng-click="$ctrl.hideForm()"></button>
        </header>
        <div class="modal-card-body">
          <events-app-form event="$ctrl.event"></events-app-form>
        </div>
      </div> <!-- .modal-card -->
      <button class="modal-close" ng-click="$ctrl.hideForm()"></button>
    </div><!-- .modal -->
  `,

  controller: [ 'Messages', function(Messages) {

    let ctrl = this;
    ctrl.hideForm = hideForm;

    function hideForm() {
      Messages.broadcast('events:form:hide:request', {});
    }

  }]

});

})();(function() {

var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppFormFullDay', {
  bindings: {
    isFullDay: '<'
  },

  template: `
    <ng-form name="eventFormFullDay">
      <label class="label">Uhrzeit</label>
      <p class="control"
         style="margin-bottom: 10px;">
        <label class="checkbox">
          <input type="checkbox"
                 ng-model="$ctrl.isFullDay"
                 ng-change="$ctrl.update()">
          Ganztägig
        </label>
      </p>
    </ng-form>
  `,

  controller: ['Messages', function(Messages) {
    var ctrl = this;

    ctrl.$onChanges = changes => {
      //console.log(changes);
    };

    ctrl.update = () => {
      Messages.broadcast('events:form:isFullDay:update', {
        isFullDay: ctrl.isFullDay
      });
    };
  }]
});

})();(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppFormName', {
  bindings: {
    eventName: '<'
  },
  template: `
    <ng-form name="eventFormName">
    <label class="label" for="eventNameTf">Bezeichnung</label>
    <p class="control has-icon has-icon-right"
       style="margin-bottom: 10px;">
      <input type="text"
             name="eventNameTF"
             class="input"
             ng-class="!eventFormName.eventNameTF.$touched ? '' : (eventFormName.eventNameTF.$valid ? 'is-success' : 'is-danger')"
             style="max-width: 100%;"
             id="eventNameTf"
             ng-model="$ctrl.eventName"
             ng-model-options="{updateOn: 'blur'}"
             ng-change="$ctrl.update()"
             placeholder="Bezeichnung"
             ng-required="true"
             required autofocus>
      <i class="fa fa-warning"
         ng-if="eventFormName.eventNameTF.$touched && !eventFormName.eventNameTF.$valid">
      </i>
      <span class="help is-danger is-pulled-right"
            ng-if="eventFormName.eventNameTF.$touched && !eventFormName.eventNameTF.$valid">
        Bezeichnung ist erforderlich.
      </span>
    </p>
    </ng-form>
  `,
  controller: ['Messages', function(Messages) {
    var ctrl = this;

    ctrl.$onChanges = changes => {
      ctrl.eventName = changes.eventName.currentValue;
    };

    ctrl.update = () => {
      Messages.broadcast('events:form:name:update', {
        eventName: ctrl.eventName
      });
    };
  }]
});

})();(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppDay', {

  bindings: {
    day: '<'
  },

  template: `
    <div ng-hide="$ctrl.day.nr == 0"
         class="calendar-day">

      <div class="calendar-day-heading">
        {{$ctrl.day.nr}}
      </div>

      <div class="calendar-day-body">
        <p ng-repeat="event in $ctrl.day.events">
          <a ng-click="$ctrl.editEvent(event)"
             style="min-width:100%;max-width: 100%;">
            {{event.timeBegin | date: 'shortTime' }} <br/> {{event.name}}
          </a>
        </p>
      </div>

      <div class="overlay">
        <div class="">
          <a class="button is-primary is-outlined is-fullwidth"
             style="line-height: 2em"
             ng-click="$ctrl.openForm()">
            Termin hinzufügen
          </a>
        </div>
      </div>

    </div>
  `,

  controller: [ 'Messages', function(Messages) {

    let ctrl = this;
    ctrl.editEvent = editEvent;
    ctrl.openForm = openForm;

    function editEvent(event) {
      Messages.broadcast('events:edit:request', {
       day: ctrl.day,
       event: event
      });
    }

    function openForm() {
      Messages.broadcast('events:create:request', {
        day: ctrl.day
      });
    }

  }]

});

})();(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppFormContact', {
  bindings: {
    eventContact: '<'
  },
  template: `
    <ng-form name="eventFormContact">
    <label class="label" for="eventContactTf">Ansprechpartner</label>
    <p class="control has-icon has-icon-right"
       style="margin-bottom: 10px;">
      <input
            type="text"
            name="eventContactTf"
            class="input"
            ng-class="!eventFormContact.eventContactTf.$touched ? '' :
                      (eventFormContact.eventContactTf.$valid ? 'is-success' : 'is-danger')"
            style="max-width: 100%;"
            id="eventContactTf"
            ng-model="$ctrl.eventContact"
            ng-model-options="{updateOn: 'blur'}"
            ng-change="$ctrl.update()"
            placeholder="Ansprechpartner"
            ng-required="true"
            required>
      <i class="fa fa-warning"
         ng-if="eventFormContact.eventContactTf.$touched
                && !eventFormContact.eventContactTf.$valid">
      </i>
      <span class="help is-danger is-pulled-right"
            ng-if="eventFormContact.eventContactTf.$touched
                   && !eventFormContact.eventContactTf.$valid">
        Ansprechpartner ist erforderlich.
      </span>
    </p>
    </ng-form>
  `,
  controller: ['Messages', function(Messages) {
    var ctrl = this;

    ctrl.$onChanges = changes => {
      //console.log(ctrl.eventConatct);
    };

    ctrl.update = () => {
      Messages.broadcast('events:form:contact:update', {
        eventContact: ctrl.eventContact
      });
    };
  }]
});

})();/* bof */
(function() {

angular.module('fa.admin.files', [
  'fa.shared.constants'
]);

angular
.module('fa.admin.files')
.config(Routes);;

angular
.module('fa.admin.files')
.controller('FilesController', FilesController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

FilesController.$inject = [
  '$window',
  'API_PREFIX',
  'FileUploader',
  'Files'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('files', {
    url: '/files',
    templateUrl: ADMIN_APP_PREFIX + 'pages/files/files.tpl.html',
    controller: 'FilesController as vm'
  });
}

function FilesController($window, API_PREFIX, FileUploader, Files) {
  var vm = this;
  vm.fileApi = API_PREFIX + '/files/';
  vm.files = [];
  vm.uploader = new FileUploader({
    url: API_PREFIX + '/files'
  });
  vm.removeFile = removeFile;

  activate();

  function activate() {
    getFiles();
  }

  function getFiles() {
    Files.getAll(function(files) {
      vm.files = files;
    });
  }

  function removeFile(index, file) {
    if($window.confirm('Diese Datei wirklich löschen?')) {
      Files.remove(file.id, function() {
        getFiles();
      });
    }
  }

  vm.uploader.onSuccessItem = function() {
    vm.uploader.clearQueue();
    getFiles();
  };
  vm.uploader.onErrorItem = function() {
    //console.info('onErrorItem');
    //console.log(arguments);
  };
  vm.uploader.onCancelItem = function() {
    //console.info('onCancelItem');
  };
  vm.uploader.onCompleteItem = function() {
    //console.info('onCompleteItem');
    // set success message
  };
  vm.uploader.onAfterAddingFile = function(item) {
    item.uploadedAt = Date.now();
    //console.log(item);
  };
}

})();
/* eof */
/* bof */
(function() {
'use strict';

angular
.module('fa.admin.overview', [
  'fa.shared.constants'
]);

/* Routes */
angular
.module('fa.admin.overview')
.config(Routes);

Routes.$inject = [
  '$stateProvider',
  'ROUTE_PREFIX',
  'ADMIN_APP_PREFIX'
];

function Routes($stateProvider, ROUTE_PREFIX, ADMIN_APP_PREFIX) {
  $stateProvider
    .state('overview', {
      url: '/overview',
      templateUrl: ADMIN_APP_PREFIX + 'pages/overview/overview.tpl.html',
      controller: 'OverviewController as vm'
    })
  ;
}

/* Controller */
angular
.module('fa.admin.overview')
.controller('OverviewController', OverviewController);

OverviewController.$inject = [
  'OverviewService'
];

function OverviewController(OverviewService) {

}

angular
.module('fa.admin.overview')
.factory('OverviewService', OverviewService);

OverviewService.$inject = [

];

function OverviewService() {
  return {};
}

})();
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.rooms';
angular.module(moduleName, [
  'fa.shared.constants'
]);

angular
.module(moduleName)
.config(Routes);

angular
.module(moduleName)
.controller('RoomsController', RoomsController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

RoomsController.$inject = [
  '$scope',
  '$window',
  'API_PREFIX',
  'FileUploader',
  'RoomService'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('rooms', {
    url: '/rooms',
    templateUrl: ADMIN_APP_PREFIX + 'pages/rooms/rooms.tpl.html',
    controller: 'RoomsController as vm'
  });
}

function RoomsController($scope,
                         $window,
                         API_PREFIX,
                         FileUploader,
                         RoomService) {
  var vm = this;
  vm.uploader = new FileUploader({
    url: API_PREFIX + '/files'
  });
  vm.editable = false;
  vm.saving = false;
  vm.updating = false;
  vm.imagesVisible = false;
  vm.imageApi = API_PREFIX + '/images/';
  vm.selectedImage = {};

  vm.rooms = [];
  vm.newroom = {};
  vm.message = '';
  vm.toggleEditable = toggleEditable;
  vm.toggleImages = toggleImages;
  vm.saveRoom = saveRoom;
  vm.removeRoom = removeRoom;
  vm.editRoom = editRoom;
  vm.updateRoom = updateRoom;
  vm.cancel = cancel;

  vm.removeImage = removeImage;

  activate();

  function activate() {
    resetRoom();
    getAllRooms();
  }

  function toggleEditable() {
    vm.editable = !vm.editable;
    vm.saving = true;
    vm.updating = false;
  }

  function toggleImages() {
    vm.imagesVisible = !vm.imagesVisible;
  }

  function getAllRooms() {
    RoomService.getAll(function(result) {
      vm.rooms = result;
    });
  }

  function saveRoom() {
    if (validateForm()) {
      vm.newroom.image = vm.selectedImage.id;
      RoomService.save(vm.newroom, function() {
        resetRoom();
        vm.message = '';
      });
    } else {
      vm.message = 'Bitte Eingaben überprüfen';
    }
  }

  function resetRoom() {
    vm.newroom = {
      name: '',
      nr: '',
      building: '',
      equipment: '',
      image: ''
    };
  }

  function removeRoom(index, room) {
    if($window.confirm('Diesen Raum wirklich löschen?')) {
      RoomService.remove(index, room, function(rooms) {
        //$scope.$digest();
      });
    }
  }

  function editRoom(index, room) {
    vm.newroom = vm.rooms[index];
    vm.editable = true;
    vm.saving = false;
    vm.updating = true;

  }

  function updateRoom() {
    vm.newroom.image = vm.selectedImage.id;
    RoomService.update(vm.newroom, function(res) {
      vm.editable = false;
      vm.updating = false;
      resetRoom();
    });
  }

  function cancel() {
    vm.updating = false;
    vm.saving = false;
    vm.editable = false;
    resetRoom();
  }

  function validateForm() {
    // TODO: do more validation, e.g. with $sce service
    if (vm.newroom.name !== '' &&
        vm.newroom.nr !== '' &&
        vm.newroom.building !== '') {
      return true;
    }
    return false;
  }

  function removeImage() {
    vm.newroom.image = '';
  }

  vm.uploader.onSuccessItem = function(item, response, status) {
    vm.uploader.clearQueue();
    console.log(response);
    vm.newroom.image = response.id;
  };
  vm.uploader.onErrorItem = function() {
    //console.info('onErrorItem');
    //console.log(arguments);
  };
  vm.uploader.onCancelItem = function() {
    //console.info('onCancelItem');
  };
  vm.uploader.onCompleteItem = function() {
    //console.info('onCompleteItem');
    // set success message
  };
  vm.uploader.onAfterAddingFile = function(item) {
    item.uploadedAt = Date.now();
    //console.log(item);
  };
}

})(); 
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.users';
angular.module(moduleName, [
  'fa.shared.constants'
]);

angular.module(moduleName).config(Routes);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('users', {
    url: '/users',
    template: '<users-app></users-app>'
  });
}

})();
/* eof */
(function() {
var moduleName = 'fa.admin.users';

angular
.module(moduleName)
.factory('Users', UsersService);

UsersService.$inject = [
  '$http',
  'API_PREFIX',
  'Messages'
];


function UsersService($http, API_PREFIX, Messages) {
  var vm = this;

  return {
    getAll: getAll,
    getById: getById,
    create: create,
    remove: remove,
    update: update
  };

  function getAll(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/users'
    };
    return $http(req);
  }

  function getById(id, cb) {
    cb(vm.users[id]);
  }

  function create(user, callback) {
    var paramString = createParamString(user);
    var req = {
      method: 'POST',
      url: API_PREFIX + '/users',
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return $http(req);
  }

  function remove(user) {
    var id = user.id;
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/users/' + id
    };
    return $http(req);
  }

  function update(user) {
    var id = user.id;
    var paramString = createParamString(user);
    var req = {
      method: 'PUT',
      url: API_PREFIX + '/users/' + id,
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return $http(req);
  }

  function createParamString(user) {
    var params = 'name=' + user.name
      + '&password=' + user.password
      + '&apiKey=' + user.apiKey;
    return params;
  }
}

})();
(function() {
var moduleName = 'fa.admin.users';

angular.module(moduleName).component('usersApp', {

  bindings: {}

//   template:[
//     '<users-app-heading title="Benutzer"></users-app-heading>'
//   , '<div class="columns">'
//   , '<div class="column is-one-half">'
//   , '<users-app-table users="$ctrl.users"></users-app-table>'
//   , '<users-app-form status="$ctrl.formStatus" user="$ctrl.currentUser"></users-app-form>'
//   , '</div>'
//   , '<div class="column">'
//   , '</div>'
//   , '</div>'
//   ].join(''),

  , template: `
    <users-app-heading title="Benutzer"></users-app-heading>
    <div class="columns">
      <div class="column is-one-half">
        <users-app-table users="$ctrl.users"></users-app-table>
        <users-app-form status="$ctrl.formStatus"
                        user="$ctrl.currentUser"
                        message="$ctrl.statusMessage">
        </users-app-form>
      </div>
      <div class="column"></div>
    </div>
  `

  , controller: [
    '$window', 'Messages', 'Users', function($window, Messages, Users) {
    var vm = this;
    vm.formStatus = 'creating';
    vm.statusMessage = {text: ''};
    vm.users = [];

    Messages.listen('users:fetch:request', messageData => {
      getAllUsers();
    });

    Messages.listen('users:create:request', user => {
      Users
      .create(user)
      .then(response => {
        getAllUsers();
      })
      .catch(handleErrorResponse);
    });

    Messages.listen('users:edit:request', user => {
      vm.formStatus = 'updating';
      vm.currentUser = user;
    });

    Messages.listen('users:update:request', user => {
      Users.update(user)
      .then(response => {
        if (response.data.res === 1) {
          vm.statusMessage = {
            type: 'is-success',
            text: 'Ändern erfolgreich.'
          }
        }
        getAllUsers();
      })
      .catch(handleErrorResponse);
    });

    Messages.listen('users:delete:request', user => {
      if($window.confirm('Diesen Benutzer wirklich löschen?')) {
        Users.remove(user)
        .then(response => {
          getAllUsers();
        })
        .catch(handleErrorResponse);
      }
    });

    Messages.listen('users:form:reset', () => {
      vm.formStatus = 'creating';
    });

    function getAllUsers() {
      Users.getAll()
      .then(users => {
        vm.users = users.data;
      })
      .catch(handleErrorResponse);
    }

    function handleErrorResponse(error) {
      console.error(error);
    } 
  }]

});

})();(function() {
var moduleName = 'fa.admin.users';

angular.module(moduleName).component('usersAppHeading', {

  bindings: {
    title: '@'
  },
  template: `
    <div class="columns">
      <div class="column">
        <div class="heading">
          <h1 class="title">{{$ctrl.title}}</h1>
        </div>
      </div>
    </div>
  `
});

})(); 
(function() { 
var moduleName = 'fa.admin.users';

angular.module(moduleName).component('usersAppTable', {

  bindings: {
    users: '<'
  },
  template: `
    <h2 class="subtitle">Vorhandene Benutzer</h2>
    <table class="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Api Key</th>
          <th>Aktionen</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="user in $ctrl.users">
          <td>{{user.id}}</td>
          <td>{{user.name}}</td>
          <td>{{user.apiKey}}</td>
          <td>
            <div class="btn-group" role="group" aria-label="aktionen">
              <button type="button"
                      class="button is-small"
                      ng-click="$ctrl.edit(user)">
                <i class="fa fa-pencil" aria-hidden="true"></i>
              </button>
              <button type="button"
                      class="button is-small is-danger"
                      ng-click="$ctrl.remove(user);">
                <i class="fa fa-times" aria-hidden="true"></i>
              </button>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  `,
  controller: ['Messages', function(Messages) {
    this.edit = edit;
    this.remove = remove;

    function edit(user) {
      Messages.broadcast('users:edit:request', user);
    }

    function remove(user) {
      Messages.broadcast('users:delete:request', user);
    }

    Messages.broadcast('users:fetch:request', {});
  }]

});

})(); 
(function() {
var moduleName = 'fa.admin.users';

angular.module(moduleName).component('usersAppForm', {

  bindings: {
    status: '<',
    user: '<'
  },

  template: `
    <h2 class="subtitle">Benutzer anlegen / ändern</h2>
    <form name="form" novalidate>
      <div class="columns">
        <div class="column is-one-half">

          <label class="label" for="userNameTf">Benutzername</label>
          <p class="control has-icon has-icon-right">
            <input type="text"
               class="input"
               ng-class="!form.userNameTf.$touched ? '' : (form.userNameTf.$valid ? 'is-success' : 'is-danger')"
               name="userNameTf"
               ng-model="$ctrl.user.name"
               placeholder="Benutzername"
               ng-required="true">

            <i class="fa fa-warning"
               ng-if="form.userNameTf.$touched && !form.userNameTf.$valid">
            </i>
            <span class="help is-danger is-pulled-right"
                  ng-if="form.userNameTf.$touched && !form.userNameTf.$valid">
              Name ist erforderlich.
            </span>
          </p>

          <label class="label" for="userPassTf">Passwort</label>
          <p class="control has-icon has-icon-right">
            <input type="text"
               class="input"
               name="userPassTf"
               ng-class="!form.userPassTf.$touched ? '' : (form.userPassTf.$valid ? 'is-success' : 'is-danger')"
               ng-model="$ctrl.user.password"
               placeholder="Passwort"
               ng-required="true">
            <i class="fa fa-warning"
               ng-if="form.userPassTf.$touched && !form.userPassTf.$valid">
            </i>
            <span class="help is-danger is-pulled-right"
                  ng-if="form.userPassTf.$touched && !form.userPassTf.$valid">
              Passwort ist erforderlich.
            </span>
          </p>

          <label class="label" for="userKeyTf">API Key</label>
          <p class="control has-icon has-icon-right">
            <input type="text"
                 class="input"
                 name="userKeyTf"
                 ng-class="!form.userKeyTf.$touched ? '' : (form.userKeyTf.$valid ? 'is-success' : 'is-danger')"
                 ng-model="$ctrl.user.apiKey"
                 placeholder="API Key"
                 ng-required="true">
            <i class="fa fa-warning"
               ng-if="form.userKeyTf.$touched && !form.userKeyTf.$valid">
            </i>
            <span class="help is-danger is-pulled-right"
                  ng-if="form.userKeyTf.$touched && !form.userKeyTf.$valid">
              Passwort ist erforderlich.
            </span>
          </p>
        </div>
        <!-- column right -->
        <div class="column"></div>
      </div>


      <div class="columns">
        <!-- column left -->
        <div class="column">
          <button type="button"
                  ng-if="$ctrl.status == 'creating'"
                  ng-disabled="!form.$valid"
                  class="button is-success"
                  ng-click="$ctrl.create()">Speichern</button>
          <button type="button"
                  ng-if="$ctrl.status == 'updating'"
                  ng-disabled="!form.$valid"
                  class="button is-warning"
                  ng-click="$ctrl.update()">Ändern</button>
          <button type="reset"
                  ng-click="$ctrl.reset()"
                  class="button is-default">Abbrechen</button>
        </div>
        <!-- column right -->
        <div class="column"></div>
      </div>

    </form>
  `,

  controller: ['$scope', 'Messages', function($scope, Messages) {
    var ctrl = this;
    //vm.user = {};
    ctrl.create = create;
    ctrl.update = update;
    ctrl.reset = reset;

    function create() {
      Messages.broadcast('users:create:request', ctrl.user);
      reset();
    }

    function update() {
      Messages.broadcast('users:update:request', ctrl.user);
      reset();
    }

    function reset() {
      ctrl.user = {
        name: '',
        password: '',
        apiKey: ''
      };
      $scope.form.$setUntouched();
      $scope.form.$setPristine();
      $scope.form.$rollbackViewValue();
      Messages.broadcast('users:form:reset', {});
    }
  }]

});

})(); 
/* bof */
(function() {
var moduleName = 'fa.admin.images';
angular.module(moduleName, [
  'fa.shared.constants'
]);

angular
.module(moduleName)
.config(Routes);;

angular
.module(moduleName)
.controller('ImagesController', ImagesController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

ImagesController.$inject = [
  '$window',
  'API_PREFIX',
  'FileUploader',
  'Images'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('images', {
    url: '/images',
    templateUrl: ADMIN_APP_PREFIX + 'pages/images/images.tpl.html',
    controller: 'ImagesController as vm'
  });
}

function ImagesController($window, API_PREFIX, FileUploader, Images) {
  var vm = this;
  vm.imageApi = API_PREFIX + '/images/';
  vm.images = [];
  vm.uploader = new FileUploader({
    url: API_PREFIX + '/images'
  });
  vm.remove = remove;

  activate();

  function activate() {
    getImages();
  }

  function getImages() {
    Images.getAll(function(images) {
      vm.images = images;
    });
  }

  function remove(index, image) {
    if($window.confirm('Diese Datei wirklich löschen?')) {
      Images.remove(image, function() {
        getImages();
      });
    }
  }

  vm.uploader.onSuccessItem = function() {
    vm.uploader.clearQueue();
    getImages();
  };
  vm.uploader.onErrorItem = function() {
    //console.info('onErrorItem');
    //console.log(arguments);
  };
  vm.uploader.onCancelItem = function() {
    //console.info('onCancelItem');
  };
  vm.uploader.onCompleteItem = function() {
    //console.info('onCompleteItem');
    // set success message
  };
  vm.uploader.onAfterAddingFile = function(item) {
    item.uploadedAt = Date.now();
    //console.log(item);
  };
}

})();
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.departments';
angular.module(moduleName, [
  'fa.shared.services',
  'fa.shared.constants',
  'fa.admin.images'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('DepartmentsController', DepartmentsController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

DepartmentsController.$inject = [
  '$window',
  'Departments',
  'Images',
  'API_PREFIX'
];


function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('departments', {
    url: '/departments',
    templateUrl: ADMIN_APP_PREFIX + 'pages/departments/departments.tpl.html',
    controller: 'DepartmentsController as vm'
  })
  ;
}

function DepartmentsController($window,
                               Departments,
                               Images,
                               API_PREFIX) {
  var vm = this;
  vm.editable = false;
  vm.saving = true;
  vm.updating = false;

  vm.department = {
    id: '',
    numeral: '',
    name: '',
    imageId: ''
  };
  vm.departments = [];

  vm.pictures = [];
  vm.selectedImage = {};
  vm.imagesVisible = false;
  vm.imageApi = API_PREFIX + '/images/';

  vm.toggleEditable = toggleEditable;
  vm.openEditor = openEditor;
  vm.saveDepartment = saveDepartment;
  vm.removeDepartment = removeDepartment;
  vm.editDepartment = editDepartment;
  vm.updateDepartment = updateDepartment;
  vm.cancel = cancel;
  vm.reset = reset;

  vm.toggleImages = toggleImages;
  vm.chooseImage = chooseImage;
  vm.unselectImage = unselectImage;

  activate();

  function activate() {
    Departments.getAll(function(departments) {
      vm.departments = departments;
    });
    Images.getAll(function(images) {
      vm.pictures = images;
    });
  }

  function toggleEditable() {
    vm.editable = !vm.editable;
    vm.updating = false;
    vm.saving = true;
  }

  function openEditor() {
    vm.reset();
    vm.editable = true;
    vm.updating = false;
    vm.saving = true;
  }

  function editDepartment(index, department) {
    console.log(department);
    Images.getById(department.imageId, function(image) {
      vm.department = department;
      vm.selectedImage = image;
      vm.editable = true;
      vm.updating = true;
      vm.saving = false;
    });
  }

  function saveDepartment() {
    vm.department.imageId = vm.selectedImage.id;
    Departments.create(vm.department, function() {
      vm.cancel();
    });
  }

  function removeDepartment(index, department) {
    if($window.confirm('Dieses Sachgebiet wirklich löschen?')) {
      Departments.remove(department, function() {});
    }
  }

  function updateDepartment() {
    vm.department.imageId = vm.selectedImage.id;
    Departments.update(vm.department, function() {
      vm.editable = false;
      vm.updating = false;
      vm.reset();
    });
  }

  function cancel() {
    vm.saving = true;
    vm.editable = false;
    vm.reset();
  }

  function reset() {
    vm.department = {
      id: '',
      numeral: '',
      name: '',
      imageId: ''
    };
    vm.imagesVisible = false;
    vm.selectedImage = null;
  }

  function toggleImages() {
    vm.imagesVisible = !vm.imagesVisible;
  }

  function chooseImage(event, pic) {
    vm.selectedImage = pic;
    vm.department.imageId = pic.id;
  }

  function unselectImage() {
    vm.selectedImage = null;
    vm.department.imageId = '';
  }
}

})();
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.staffCouncil';
angular.module(moduleName, [
  'fa.shared.services',
  'fa.shared.constants',
  'fa.admin.images'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('StaffCouncilController', StaffCouncilController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

StaffCouncilController.$inject = [
  '$window',
  'Images',
  'API_PREFIX'
];


function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('staffCouncil', {
    url: '/staff-council',
    templateUrl: ADMIN_APP_PREFIX + 'pages/staff-council/staff-council.tpl.html',
    controller: 'StaffCouncilController as vm'
  })
  ;
}

function StaffCouncilController($window,
                                Images,
                                API_PREFIX) {
  var vm = this;
  
}

})();
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.aspirants';
angular.module(moduleName, [
  'fa.shared.services',
  'fa.shared.constants',
  'fa.admin.images'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('AspirantsController', AspirantsController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

AspirantsController.$inject = [
  '$window',
  'Aspirants',
  'Images',
  'API_PREFIX'
];


function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('aspirants', {
    url: '/aspirants',
    templateUrl: ADMIN_APP_PREFIX + 'pages/aspirants/aspirants.tpl.html',
    controller: 'AspirantsController as vm'
  })
  ;
}

function AspirantsController($window,
                             Aspirants,
                             Images,
                             API_PREFIX) {
  var vm = this;
  vm.aspirants = [1,2,3,4];
  vm.aspirant = {
    name: '',
    year: '2016',
    career: 'StAnw',
    imageId: '0'
  };
  vm.formVisible = false;
  vm.imageApi = API_PREFIX + '/images/';
  vm.updating = false;

  vm.cancel = cancel;
  vm.create = create;
  vm.edit = edit;
  vm.remove = remove;
  vm.toggleForm = toggleForm;
  vm.update = update;

  activate();

  function activate() {
    getAll();
  }

  function cancel() {
    reset();
  }

  function create() {
    Aspirants.create(vm.aspirant, function(aspirant) {
      vm.aspirants.push(aspirant);
      reset();
    });
  }

  function edit(aspirant) {
    vm.aspirant = aspirant;
    vm.formVisible = true;
    vm.updating = true;
  }

  function getAll() {
    Aspirants.getAll(function(aspirants) {
      vm.aspirants = aspirants;
    });
  }

  function remove(index, aspirant) {
    if($window.confirm('Diesen Anwärter wirklich löschen?')) {
      Aspirants.remove(aspirant, function() {
        getAll();
      });
    }
  }

  function reset() {
    vm.aspirant = {
      name: '',
      year: '2016',
      career: 'StAnw',
      imageId: '1'
    };
    vm.formVisible = false;
    vm.updating = false;
  }

  function toggleForm() {
    vm.formVisible = !vm.formVisible;
  }

  function update() {
    Aspirants.update(vm.aspirant, function() {
      getAll();
      reset();
    });
  }

}

})();
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.links';
angular.module(moduleName, [
  'fa.shared.constants'
]);

angular
.module(moduleName)
.config(Routes);

angular
.module(moduleName)
.controller('LinksController', LinksController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

LinksController.$inject = [
  '$scope',
  '$window',
  'API_PREFIX',
  'Links'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('links', {
    url: '/links',
    templateUrl: ADMIN_APP_PREFIX + 'pages/links/links.tpl.html',
    controller: 'LinksController as vm'
  });
}

function LinksController($scope,
                         $window,
                         API_PREFIX,
                         Links
                        ) {
  var vm = this;
  vm.isUpdating = false;
  vm.currentLink = {};
  vm.links = [];
  vm.create = create;
  vm.edit = edit;
  vm.update = update;
  vm.remove = remove;
  vm.reset = reset;

  activate();

  function activate() {
    getAll();
  }

  function getAll() {
    Links.getAll(function(links) {
      vm.links = links;
    });
  }

  function create() {
    Links.create(vm.currentLink, function(link) {
      getAll();
      reset();
    });
  }

  function edit(index, link) {
    vm.currentLink = link;
    vm.isUpdating = true;
  }

  function remove(index, link) {
    if($window.confirm('Diesen Link wirklich löschen?')) {
      Links.remove(link, function() {
        getAll();
      });
    }
  }

  function update() {
    Links.update(vm.currentLink, function(result) {
      // console.log(result);
      reset();
    });
  }

  function reset() {
    vm.isUpdating = false;
    vm.currentLink = {
      name: '',
      url: ''
    };
    $scope.form.$setUntouched();
    $scope.form.$setPristine();
    $scope.form.$rollbackViewValue();
  }
}

})();
/* eof */
/* bof */
(function() {
'use strict';

var moduleName = 'fa.admin.protocols';
angular.module(moduleName, [
  'ngSanitize',
  'ui.tinymce',
  'fa.shared.constants'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('ProtocolsController', ProtocolsController);

ProtocolsController.$inject = [
  '$scope',
  '$window',
  'API_PREFIX',
  'Articles',
  'ArticleLinks',
  'Departments',
  'Links',
  'Messages'
];

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider

  .state('protocols', {
    url: '/protocols',
    templateUrl: ADMIN_APP_PREFIX +
                 'pages/protocols/protocols.tpl.html',
    controller: 'ProtocolsController as vm'
  })
  ;
}

/******************************************************************************
 * ProtocolsController
 ******************************************************************************/
function ProtocolsController($scope,
                             $window,
                             API_PREFIX,
                             Articles,
                             ArticleLinks,
                             Departments,
                             Links,
                             Messages) {
  var vm = this;
  vm.today = '';
  vm.titlePlaceholder = '';
  vm.shortTextPlaceholder = '';
  vm.departments = [];
  vm.protocols = [];
  vm.protocol = {
    title: '',
    type: 'protocol',
    shortText: '',
    longText: '',
    createdAt: '',
    updatedAt: '',
    department: {},
    links: []
  };
  vm.updating = false;
  vm.filesVisible = false;
  vm.followLink = followLink;
  vm.newLink = {};
  vm.previewVisible = false;
  vm.removeLink = removeLink;
  vm.togglePreview = togglePreview;

  vm.create = create;
  vm.cancel = cancel;
  vm.update = update;
  vm.remove = remove;
  vm.edit = edit;

  vm.updateForm = updateForm;
  vm.insertText = insertText;

  activate();

  function activate() {
    Departments.getAll(function(departments) {
      vm.departments = departments;
      vm.protocol.department = vm.departments[0];
      vm.today = moment().format('L');
      vm.titlePlaceholder = 
        'Dienstbesprechung SG ' +
        vm.protocol.department.numeral;
      vm.shortTextPlaceholder =
        'Protokoll der Dienstbesprechung vom ' +
        vm.today + '.';
      vm.protocol.createdAt = Date.now();
      vm.protocol.updatedAt = Date.now();
    });
    Articles.getProtocols(function(protocols) {
      vm.protocols = protocols;
    });

    Messages.listen('article:edit', function(data) {
      edit(data.article);
    });

    Messages.listen('article:remove', function onSuccess(data) {
      remove(data.index, data.article);
    });
  }

  function followLink(link) {
    $window.open(link.url, '_blank');
  }

  function updateForm() {
    vm.titlePlaceholder = 
      'Dienstbesprechung SG ' +
      vm.protocol.department.numeral;
  }

  function removeLink(index, link) {
    Links.remove(link, function() {
      vm.protocol.links.splice(index, 1);
    });
  }

  function create() {
    Articles.create(vm.protocol, function(protocol) {
      ArticleLinks.appendLinks(
        protocol.id,
        vm.protocol.links,
        function(links) {
          protocol.links = links;
          //vm.protocols.unshift(protocol);
          vm.protocols.push(protocol);
          reset();
        }
      );
    });
  }

  function remove(index, article) {
    if($window.confirm('Dieses Protokoll wirklich löschen?')) {
      Articles.remove(article.id, function() {
        vm.protocols.splice(index - 1, 1);
        ArticleLinks.removeArticle(article.id, function() {});
      });
    }
  }

  function edit(protocol) {
    ArticleLinks.getLinksForArticle(protocol.id, function(links) {
      protocol.links = links;
      vm.updating = true;
      vm.protocol = protocol;
      vm.protocol.department = findDepartment(protocol.departmentId);
    });
  }

  function update() {
    // update article data
    Articles.update(vm.protocol, function(touchedRows) {
      // remove old associated files
      ArticleLinks.removeArticle(vm.protocol.id, function(res) {
        // append new associated files
        ArticleLinks.appendLinks(
          vm.protocol.id,
          vm.protocol.links,
          function(result) {
            cancel();
          }
        );
      });
    });
  }

  function cancel() {
    vm.updating = false;
    reset();
  }

  function reset() {
    vm.protocol = {
      title: '',
      type: 'protocol',
      shortText: '',
      longText: '',
      createdAt: '',
      updatedAt: '',
      department: vm.departments[0],
      files: []
    };
    vm.filesVisible = false;
    $scope.articleForm.$setUntouched();
    $scope.articleForm.$setPristine();
    $scope.articleForm.$rollbackViewValue();
  }

  function insertText() {
    vm.protocol.title = vm.titlePlaceholder;
    vm.protocol.shortText = vm.shortTextPlaceholder;
    vm.protocol.createdAt = Date.now();
    vm.protocol.updatedAt = Date.now();
  }

  function findDepartment(id) {
    var result = {};
    vm.departments.forEach(function(entry) {
      if (entry.id === id) {
        result = entry;
      }
    });
    return result;
  }

  function togglePreview() {
    vm.previewVisible = !vm.previewVisible;
  }

}

})();
/* eof */
/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.filechooser';

angular.module(moduleName, [
  'angular-loading-bar'
]);
angular.module(moduleName).directive('faFileChooser', FileChooser);

FileChooser.$inject = [
  '$window',
  'ADMIN_APP_PREFIX',
  'API_PREFIX',
  'cfpLoadingBar',
  'Files'
];

function FileChooser($window,
                     ADMIN_APP_PREFIX,
                     API_PREFIX,
                     cfpLoadingBar,
                     Files) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX + 'components/filechooser/fa.filechooser.tpl.html',
    scope: {
      articleFiles: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.fileApi = API_PREFIX + '/files/';
    scope.files = [];
    scope.addFile = addFile;

    activate();

    function activate() {
      cfpLoadingBar.start();
      Files.getAll(function(files) {
        scope.files = files;
        cfpLoadingBar.complete();
      });
    }

    function addFile(file) {
//       if (!scope.articleLinks) {
//         scope.articleLinks = [];
//       }
//       scope.articleLinks.push(link);
     copyToClipboard(scope.fileApi + file.id);
    }

    function copyToClipboard(text) {
      $window.prompt("In Zwischenablage kopieren: STRG + C, Enter", text);
    }
  }
}

})();
/* eof */
/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.imagechooser';

angular.module(moduleName, [
  'angular-loading-bar'
]);
angular.module(moduleName).directive('faImageChooser', ImageChooser);

ImageChooser.$inject = [
  'ADMIN_APP_PREFIX',
  'API_PREFIX',
  'cfpLoadingBar',
  'Images'
];

function ImageChooser(ADMIN_APP_PREFIX,
                      API_PREFIX,
                      cfpLoadingBar,
                      Images) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX +
      'components/imagechooser/fa.imagechooser.tpl.html',
    scope: {
      myImage: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.imageApi = API_PREFIX + '/images/';
    scope.images = [];
    scope.addImage = addImage;

    activate();

    function activate() {
      cfpLoadingBar.start();
      Images.getAll(function(images) {
        scope.images = images;
        cfpLoadingBar.complete();
      });
    }

    function addImage(image) {
      //scope.myImage = image.id;
      scope.myImage = image;
    }
  }
}

})();
/* eof */
/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.linkchooser';

angular.module(moduleName, [
  'angular-loading-bar'
]);
angular.module(moduleName).directive('faLinkChooser', LinkChooser);

LinkChooser.$inject = [
  '$window',
  'ADMIN_APP_PREFIX',
  'API_PREFIX',
  'cfpLoadingBar',
  'Links'
];

function LinkChooser($window,
                     ADMIN_APP_PREFIX,
                     API_PREFIX,
                     cfpLoadingBar,
                     Links) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX +
      'components/linkchooser/fa.linkchooser.tpl.html',
    scope: {
      articleLinks: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.linkApi = API_PREFIX + '/links/';
    scope.links = [];
    scope.addLink = addLink;
    scope.followLink = followLink;

    activate();

    function activate() {
      cfpLoadingBar.start();
      Links.getAll(function(links) {
        scope.links = links;
        cfpLoadingBar.complete();
      });
    }

    function addLink(link) {
//       if (!scope.articleLinks) {
//         scope.articleLinks = [];
//       }
//       scope.articleLinks.push(link);
      copyToClipboard(link.url);
    }

    function copyToClipboard(text) {
      $window.prompt("In Zwischenablage kopieren: STRG + C, Enter", text);
    }

    function followLink(link) {
      $window.open(link.url, '_blank');
    }
  }
}

})();
/* eof */
/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.linkcreator';

angular.module(moduleName, [
  'angular-loading-bar'
]);
angular.module(moduleName).directive('faLinkCreator', LinkCreator);

LinkCreator.$inject = [
  '$window',
  'ADMIN_APP_PREFIX',
  'API_PREFIX',
  'cfpLoadingBar',
  'Links'
];

function LinkCreator($window,
                     ADMIN_APP_PREFIX,
                     API_PREFIX,
                     cfpLoadingBar,
                     Links) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX +
      'components/linkcreator/fa.linkcreator.tpl.html',
    scope: {
      articleLinks: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.linkApi = API_PREFIX + '/links/';
    scope.newLink = {
      name: '',
      url: ''
    };
    scope.saveLink = saveLink;
    scope.resetLink = resetLink;
    scope.toggleFiles = toggleFiles;

    function saveLink() {
      //console.log(scope.newLink);
      if (scope.newLink.name != '' && scope.newLink.url != '') {
        Links.create(scope.newLink, function(link) {
          console.log(link);
          scope.articleLinks.push(link);
          resetLink();
        });
      }
    }

    function resetLink() {
      scope.newLink = {
        name: '',
        url: ''
      };
      scope.linkform.$setUntouched();
      scope.linkform.$setPristine();
      scope.linkform.$rollbackViewValue();
    }

    function toggleFiles() {
      scope.filesVisible = !scope.filesVisible;
    }
  }
}

})();
/* eof */
/* bof */
(function() {
'use strict';

var moduleName = 'fa.admin.articles';
angular.module(moduleName, [
  'ngSanitize',
  'ui.tinymce',
  'fa.shared.constants'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('ArticleController', ArticleController);


ArticleController.$inject = [
  '$sanitize',
  '$scope',
  '$window',
  'API_PREFIX',
  'Articles',
  'ArticleLinks',
  'ArticleTemplates',
  'Files',
  'Links',
  'Messages'
];

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('articles', {
    url: '/articles',
    templateUrl: ADMIN_APP_PREFIX + 'pages/articles/articles.tpl.html',
    controller: 'ArticleController as vm'
  });
}

/******************************************************************************
 * NewsController
 ******************************************************************************/
function ArticleController($sanitize,
                           $scope,
                           $window,
                           API_PREFIX,
                           Articles,
                           ArticleLinks,
                           ArticleTemplates,
                           Files,
                           Links,
                           Messages) {
  var vm = this;
  vm.article = {};
  vm.articles = [];
  vm.articleTypes = [];
  vm.cancel = cancel;
  vm.create = create;
  vm.edit = edit;
  vm.filesVisible = false;
  vm.followLink = followLink;
  //vm.links = [];
  vm.linksVisible = false;
  vm.newLink = {};
  vm.previewVisible = false;
  vm.remove = remove;
  vm.removeLink = removeLink;
  vm.reset = reset;
  vm.selectedType = {};
  vm.template = {};
  vm.today = '';
  vm.toggleFiles = toggleFiles;
  vm.togglePreview = togglePreview;
  vm.update = update;
  vm.updateForm = updateForm;
  vm.updating = false;

  activate();

  function activate() {
//     tinymce.init({
//       selector: '#mytextarea',
//       entity_encoding : 'raw'
//     });
    ArticleTemplates.getTypes(function(types) {
      vm.articleTypes = types;
    });
    Articles.getNews(function(news) {
      vm.articles = news;
      vm.today = moment().format('L');
    });
    vm.article.createdAt = Date.now();
    vm.article.updatedAt = Date.now();

    Messages.listen('article:edit', function(data) {
      edit(data.article);
    });

    Messages.listen('article:remove', function onSuccess(data) {
      remove(data.index, data.article);
    });
  }

  function cancel() {
    vm.updating = false;
    // clean links
    if (!vm.updating && vm.article.links.length > 0) {
      var tmp = vm.article.links;
      tmp.forEach(function(link, index) {
        //removeLink(index, link);
      });
    }
    reset();
  }

  function create() {
    vm.article.type = 'news';
    vm.article.department = {id: 0};
    console.log(vm.article);
    Articles.create(vm.article, function(article) {
      ArticleLinks.appendLinks(
        article.id,
        vm.article.links,
        function(result) {
          article.links = result;
          vm.articles.push(article);
          reset();
        }
      );
    });
  }

  function sanitizeArticle() {
    vm.article.shortText = $sanitize(vm.article.shortText);
  }

  function edit(article) {
    ArticleLinks.getLinksForArticle(article.id, function(links) {
      article.links = links;
      vm.article = article;
      vm.updating = true;
    });
  }

  function followLink(link) {
    $window.open(link.url, '_blank');
  }

  function remove(index, article) {
    if($window.confirm('Diesen Artikel wirklich löschen?')) {
      Articles.remove(article.id, function() {
        vm.articles.splice(index - 1, 1);
        ArticleLinks.removeArticle(article.id, function() {});
      });
    }
  }

  function removeLink(index, link) {
    Links.remove(link, function() {
      vm.article.links.splice(index, 1);
    });
  }

  function reset() {
    vm.article = {
      title: '',
      type: '',
      shortText: '',
      longText: '',
      createdAt: '',
      updatedAt: '',
      department: {id:0},
      links: []
    };
    vm.filesVisible = false;
    vm.previewVisible = false;
    vm.article.links = [];
    $scope.form.$setUntouched();
    $scope.form.$setPristine();
    $scope.form.$rollbackViewValue();
  }

  function toggleFiles() {
    vm.filesVisible = !vm.filesVisible;
  }

  function togglePreview() {
    vm.previewVisible = !vm.previewVisible;
  }

  function update() {
    vm.article.type = 'news';
    vm.article.department = {id: 0};
    console.log(vm.article);
     // update article data
    Articles.update(vm.article, function(touchedRows) {
      // remove old associated files
      ArticleLinks.removeArticle(vm.article.id, function(res) {
        // append new associated files
        ArticleLinks.appendLinks(
          vm.article.id,
          vm.article.links,
          function(result) {
            reset();
            vm.updating = false;
          }
        );
      });
    });
  }

  function updateForm() {
    //console.log(vm.selectedType);
    ArticleTemplates.getTemplate(
      vm.selectedType.name,
      function(template) {
        //console.log(template);
        vm.article = template;
        vm.article.createdAt = Date.now();
        vm.article.updatedAt = Date.now();
        vm.article.links = [];
      }
    );
  }
}

})();
/* eof */
/* bof */
(function() {
'use strict';

var moduleName = 'fa.admin.articles';

angular.module(moduleName).factory('ArticleTemplates', ArticleTemplates);

ArticleTemplates.$inject = [];

function ArticleTemplates() {

  return {
    getTypes: getTypes,
    getTemplate: getTemplate
  };

  function getTypes(cb) {
    cb([
      {
        name: 'market',
        text: 'Markt'
      }, {
        name: 'personal',
        text: 'Personal'
      }, {
        name: 'other',
        text: 'Sonstige'
      }
    ]);
  }

  function getTemplate(tplType, cb) {
    var tpl = {};
    switch (tplType) {
      case 'market': {
        tpl = getMarketTemplate();
        break;
      }
      case 'personal': {
        tpl = getPersonalTemplate();
        break;
      }
      case 'other': {
        tpl = getOtherTemplate();
        break;
      }
      default: {
        tpl = getOtherTemplate();
      }
    }
    cb(tpl);
  }

  function getAbstractTemplate() {
    return {
      title: '',
      type: '',
      shortText: '',
      longText: '',
      createdAt: '',
      updatedAt: '',
      department: {id: 0},
      files: []
    };
  }

  function getMarketTemplate() {
    var tpl = getAbstractTemplate();
    tpl.title = 'Verkaufe Gegenstand';
    tpl.shortText = 'Verkaufe folgende Gegenstände: (Gegenstände).<br/> Tel: (Durchwahl)';
    tpl.type = 'market';
    return tpl;
  }

  function getPersonalTemplate() {
    var tpl = getAbstractTemplate();
    tpl.title = 'Personalinformationen';
    tpl.type = 'personal';
    tpl.shortText = 'Dokumentation der Personaländerungen.';
    return tpl;
  }

  function getOtherTemplate() {
    var tpl = getAbstractTemplate();
    tpl.title = 'Sonstiges';
    tpl.type = 'other';
    return tpl;
  }

}

})();
/* eof */
/* bof */
(function() {

angular.module('fa.admin.events', [
  'fa.shared.services',
  'fa.shared.constants'
]);

angular
.module('fa.admin.events')
.config(Routes);

angular
.module('fa.admin.events')
.controller('EventsController', EventsController);

angular
.module('fa.admin.events')
.directive('faCreateEvent', FACreateEvent);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

EventsController.$inject = [
  '$window',
  'CalendarService',
  'EventService',
  'RoomService'
];

FACreateEvent.$inject = [
  'ADMIN_APP_PREFIX',
  'EventService',
  'RoomService'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('events', {
    url: '/events',
    templateUrl: ADMIN_APP_PREFIX + 'pages/events/events.tpl.html',
    controller: 'EventsController as vm'
  });
}

function EventsController($window, 
                          CalendarService,
                          EventService,
                          RoomService) {
  var vm = this;
  vm.editable = false;
  vm.saving = false;
  vm.updating = false;
  vm.editEvent = false,
  vm.events = [];
  vm.rooms = [];

  vm.currentEvent = {};
  vm.message = '';
  vm.years = [];

  vm.monthsInYear = [];
  vm.currentMonth = '1';
  vm.currentYear = '2016';

  vm.refreshList = refreshList;
  vm.getAllEvents = getAllEvents;
  vm.toggleEditable = toggleEditable;
  vm.removeEvent = removeEvent;
  vm.editEvent = editEvent;
  vm.cancel = cancel;

  activate();

  function activate() {
    initYears();
    initMonths();
    resetEvent();
    getAllEvents();
    getRooms();
  }

  function initYears() {
    var curr = moment().year();
    vm.years = [curr, curr + 1, curr + 2, curr + 3, curr + 4];
    vm.currentYear = curr;
    vm.currentMonth = moment().month();
  }

  function initMonths(cb) {
    CalendarService.getMonthsInYear(function(months) {
      vm.monthsInYear = months;
      // set selected month to current month using moment.js
      vm.currentMonth = moment().month();
      if (cb) { cb(); }
    });
  }

  function refreshList() {
    EventService.getByMonth(
      parseInt(vm.currentMonth),
      parseInt(vm.currentYear),
      function(result) {
        vm.events = result;
      }
    );
  }

  function toggleEditable() {
    vm.editable = !vm.editable;
    vm.saving = true;
    vm.updating = false;
  }

  function getAllEvents() {
    EventService.getAll(function(result) {
      vm.events = result;
    });
  }

  function getRooms() {
    RoomService.getAll(function(rooms) {
      vm.rooms = rooms;
    });
  }

  function resetEvent() {
    vm.currentEvent = {
      name: '',
      contact: '',
      date: '',
      timeBegin: '',
      timeEnd: '',
      roomId: ''
    };
  }

  function removeEvent(index, event) {
    if($window.confirm('Diese Veranstaltung wirklich löschen?')) {
      EventService.remove(index, event, function(events) {
        vm.getAllEvents();
      });
    }
  }

  function editEvent(index, event) {
    vm.editable = true;
    vm.saving = false;
    vm.updating = true;
    var timeBegin = getTimeString(moment(parseInt(event.timeBegin)));
    var timeEnd = getTimeString(moment(parseInt(event.timeEnd)));
    var dateStart = moment(parseInt(event.date));
    var dateString = getDateString(dateStart);
    vm.currentEvent = {
      id: event.id,
      name: event.name,
      contact: event.contact,
      day: parseInt(event.day),
      month: parseInt(event.month),
      year: parseInt(event.year),
      timeBegin: timeBegin,
      timeEnd: timeEnd,
      room: findRoomById(event.roomId),
      date: {
        start: dateString
      }
    };
  }

  function getTimeString(time) {
    var timeString = (time.hour() < 10 ? '0' : '');
    timeString += time.hour() + ':';
    timeString += (time.minute() < 10 ? '0' : '');
    timeString += time.minute();
    return timeString;
  }

  function getDateString(date) {
    var month = (date.month() < 10 ? '0' : '');
    month += (date.month() + 1);
    var dateString = 
      date.date() + '.'+
      month + '.' +
      date.year();
    return dateString;
  }

  function cancel() {
    vm.updating = false;
    vm.saving = false;
    vm.editable = false;
    resetEvent();
  }

  function findRoomById(roomId) {
    var result = null;
    for (var i = 0; i< vm.rooms.length; i++) {
      if (vm.rooms[i].id == roomId) {
        result = vm.rooms[i];
        break;
      }
    }
    return result;
  }
}

/* DIRECTIVE fa-create-event */
function FACreateEvent(ADMIN_APP_PREFIX, EventService, RoomService) {
  return {
    templateUrl: ADMIN_APP_PREFIX + 'pages/events/create-event.tpl.html',
    scope: {
      myEvent: '=',
      rooms: '=',
      creating: '=',
      updating: '=',
      refresh: '='
    },
    restrict: 'E',
    link: link
  };

  function link(scope, element, attrs) {
    scope.saveEvent = saveEvent;
    scope.updateEvent = updateEvent;
    scope.cancel = cancel;
    scope.multiDay = false;

    //scope.myEvent.date = {};

    function saveEvent() {
      if (scope.multiDay) {
        saveMultiDayEvent();
      } else {
        saveSingleDayEvent();
      }

    }

    function processFormContent() {
      scope.myEvent.date = createEventDate(scope.myEvent.date.start);
      scope.myEvent.timeBegin = createEventBegin(scope.myEvent.timeBegin);
      scope.myEvent.timeEnd = createEventEnd(scope.myEvent.timeEnd);
      scope.myEvent.roomId = scope.myEvent.room.id;
    }

    function saveSingleDayEvent() {
      console.log('saving single day event...');
      processFormContent();

      EventService.save(scope.myEvent, function() {
        resetForm();
      });
    }

    function saveMultiDayEvent() {
      console.log('saving multi day event...');
      console.log(scope.myEvent);
    }

    function updateEvent() {
      console.log('updating event...');
      processFormContent();
      EventService.update(scope.myEvent, function() {
        resetForm();
        scope.refresh();
      });
    }

    function cancel() {
      resetForm();
    }

    /**
     * Creates a date object from the given date string.
     * @param dateString - a date string in format dd.mm.yyyy
     */
    function createEventDate(dateString) {
      var parts = dateString.split('.'); // [day, month, year]
      scope.myEvent.day = parts[0];
      scope.myEvent.month = parts[1];
      scope.myEvent.year = parts[2];

      return new Date(
        scope.myEvent.year,
        scope.myEvent.month - 1,
        scope.myEvent.day,
        0,0,0,0);
    }

    /**
     * Creates a date object from the given time string.
     * @param timeString - a time string in format HH:MM
     */
    function createEventBegin(timeString) {
      var parts = timeString.split(':'); // [hours, minutes]

      if (!scope.myEvent.begin) {
        scope.myEvent.begin = {};
      }

      scope.myEvent.begin.hour = parts[0];
      scope.myEvent.begin.minute = parts[1];

      return new Date(
        scope.myEvent.year,
        scope.myEvent.month -1,
        scope.myEvent.day,
        scope.myEvent.begin.hour,
        scope.myEvent.begin.minute,
        0,0);
    }

    /**
     * Creates a date object from the given time string.
     * @param timeString - a time string in format HH:MM
     */
    function createEventEnd(timeString) {
      var parts = timeString.split(':'); // [hours, minutes]

      if (!scope.myEvent.end) {
        scope.myEvent.end = {};
      }

      scope.myEvent.end.hour = parts[0];
      scope.myEvent.end.minute = parts[1];

      return new Date(
        scope.myEvent.year,
        scope.myEvent.month - 1,
        scope.myEvent.day,
        scope.myEvent.end.hour,
        scope.myEvent.end.minute,
        0,0);
    }

    function resetForm() {
      scope.myEvent = {
        id: '',
        name: '',
        contact: '',
        begin: {},
        end:{},
        day: '',
        month: '',
        year: '',
        room: {}
      };
      scope.form.$setUntouched();
      scope.form.$setPristine();
      scope.form.$rollbackViewValue();
    }
  }
}

})();
/* eof */
/* bof */
(function() {

angular.module('fa.admin.files', [
  'fa.shared.constants'
]);

angular
.module('fa.admin.files')
.config(Routes);;

angular
.module('fa.admin.files')
.controller('FilesController', FilesController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

FilesController.$inject = [
  '$window',
  'API_PREFIX',
  'FileUploader',
  'Files'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('files', {
    url: '/files',
    templateUrl: ADMIN_APP_PREFIX + 'pages/files/files.tpl.html',
    controller: 'FilesController as vm'
  });
}

function FilesController($window, API_PREFIX, FileUploader, Files) {
  var vm = this;
  vm.files = [];
  vm.uploader = new FileUploader({
    url: API_PREFIX + '/files'
  });
  vm.removeFile = removeFile;

  activate();

  function activate() {
    getFiles();
  }

  function getFiles() {
    Files.getAll(function(files) {
      vm.files = files;
    });
  }

  function removeFile(index, file) {
    if($window.confirm('Diese Datei wirklich löschen?')) {
      Files.remove(file.id, function() {
        vm.files.splice(index, 1);
      });
    }
  }

  vm.uploader.onSuccessItem = function() {
    vm.uploader.clearQueue();
    getFiles();
  };
  vm.uploader.onErrorItem = function() {
    //console.info('onErrorItem');
    //console.log(arguments);
  };
  vm.uploader.onCancelItem = function() {
    //console.info('onCancelItem');
  };
  vm.uploader.onCompleteItem = function() {
    //console.info('onCompleteItem');
    // set success message
  };
  vm.uploader.onAfterAddingFile = function(item) {
    item.uploadedAt = Date.now();
    //console.log(item);
  };
}

})();
/* eof */
/* bof */
(function() {
'use strict';

angular
.module('fa.admin.overview', [
  'fa.shared.constants'
]);

/* Routes */
angular
.module('fa.admin.overview')
.config(Routes);

Routes.$inject = [
  '$stateProvider',
  'ROUTE_PREFIX',
  'ADMIN_APP_PREFIX'
];

function Routes($stateProvider, ROUTE_PREFIX, ADMIN_APP_PREFIX) {
  $stateProvider
    .state('overview', {
      url: '/overview',
      templateUrl: ADMIN_APP_PREFIX + 'pages/overview/overview.tpl.html',
      controller: 'OverviewController as vm'
    })
  ;
}

/* Controller */
angular
.module('fa.admin.overview')
.controller('OverviewController', OverviewController);

OverviewController.$inject = [
  'OverviewService'
];

function OverviewController(OverviewService) {

}

angular
.module('fa.admin.overview')
.factory('OverviewService', OverviewService);

OverviewService.$inject = [

];

function OverviewService() {
  return {};
}

})();
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.rooms';
angular.module(moduleName, [
  'fa.shared.constants'
]);

angular
.module(moduleName)
.config(Routes);

angular
.module(moduleName)
.controller('RoomsController', RoomsController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

RoomsController.$inject = [
  '$scope',
  '$window',
  'API_PREFIX',
  'FileUploader',
  'RoomService'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('rooms', {
    url: '/rooms',
    templateUrl: ADMIN_APP_PREFIX + 'pages/rooms/rooms.tpl.html',
    controller: 'RoomsController as vm'
  });
}

function RoomsController($scope,
                         $window,
                         API_PREFIX,
                         FileUploader,
                         RoomService) {
  var vm = this;
  vm.uploader = new FileUploader({
    url: API_PREFIX + '/files'
  });
  vm.editable = false;
  vm.saving = false;
  vm.updating = false;
  vm.imagesVisible = false;
  vm.imageApi = API_PREFIX + '/images/';
  vm.selectedImage = {};

  vm.rooms = [];
  vm.newroom = {};
  vm.message = '';
  vm.toggleEditable = toggleEditable;
  vm.toggleImages = toggleImages;
  vm.saveRoom = saveRoom;
  vm.removeRoom = removeRoom;
  vm.editRoom = editRoom;
  vm.updateRoom = updateRoom;
  vm.cancel = cancel;

  vm.removeImage = removeImage;

  activate();

  function activate() {
    resetRoom();
    getAllRooms();
  }

  function toggleEditable() {
    vm.editable = !vm.editable;
    vm.saving = true;
    vm.updating = false;
  }

  function toggleImages() {
    vm.imagesVisible = !vm.imagesVisible;
  }

  function getAllRooms() {
    RoomService.getAll(function(result) {
      vm.rooms = result;
    });
  }

  function saveRoom() {
    if (validateForm()) {
      vm.newroom.image = vm.selectedImage.id;
      RoomService.save(vm.newroom, function() {
        resetRoom();
        vm.message = '';
      });
    } else {
      vm.message = 'Bitte Eingaben überprüfen';
    }
  }

  function resetRoom() {
    vm.newroom = {
      name: '',
      nr: '',
      building: '',
      equipment: '',
      image: ''
    };
  }

  function removeRoom(index, room) {
    if($window.confirm('Diesen Raum wirklich löschen?')) {
      RoomService.remove(index, room, function(rooms) {
        //$scope.$digest();
      });
    }
  }

  function editRoom(index, room) {
    vm.newroom = vm.rooms[index];
    vm.editable = true;
    vm.saving = false;
    vm.updating = true;

  }

  function updateRoom() {
    vm.newroom.image = vm.selectedImage.id;
    RoomService.update(vm.newroom, function(res) {
      vm.editable = false;
      vm.updating = false;
      resetRoom();
    });
  }

  function cancel() {
    vm.updating = false;
    vm.saving = false;
    vm.editable = false;
    resetRoom();
  }

  function validateForm() {
    // TODO: do more validation, e.g. with $sce service
    if (vm.newroom.name !== '' &&
        vm.newroom.nr !== '' &&
        vm.newroom.building !== '') {
      return true;
    }
    return false;
  }

  function removeImage() {
    vm.newroom.image = '';
  }

  vm.uploader.onSuccessItem = function(item, response, status) {
    vm.uploader.clearQueue();
    console.log(response);
    vm.newroom.image = response.id;
  };
  vm.uploader.onErrorItem = function() {
    //console.info('onErrorItem');
    //console.log(arguments);
  };
  vm.uploader.onCancelItem = function() {
    //console.info('onCancelItem');
  };
  vm.uploader.onCompleteItem = function() {
    //console.info('onCompleteItem');
    // set success message
  };
  vm.uploader.onAfterAddingFile = function(item) {
    item.uploadedAt = Date.now();
    //console.log(item);
  };
}

})(); 
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.users';
angular.module(moduleName, [
  'fa.shared.constants'
]);

angular
.module(moduleName)
.config(Routes);;

angular
.module(moduleName)
.controller('UsersController', UsersController);

angular
.module(moduleName)
.factory('Users', UsersService);

angular
.module(moduleName)
.directive('faUserForm', FAUserForm);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

UsersController.$inject = [
  '$window',
  'Users'
];

UsersService.$inject = [
  '$http',
  'API_PREFIX'
];

FAUserForm.$inject = [
  'ADMIN_APP_PREFIX',
  'Users'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('users', {
    url: '/users',
    templateUrl: ADMIN_APP_PREFIX + 'pages/users/users.tpl.html',
    controller: 'UsersController as vm'
  });
}

function UsersController($window, Users) {
  var vm = this;
  vm.users = [];
  vm.isVisible = false;
  vm.creating = false;
  vm.updating = false;
  vm.selectedUser = {
    name: '',
    password: '',
    apiKey: ''
  };

  vm.getAll = getAll;
  vm.openCreationForm = openCreationForm;
  vm.removeUser = removeUser;

  activate();

  function activate() {
    getAll();
  }

  function getAll() {
    Users.getAll(function(users) {
      vm.users = users; 
    });
  }

  function openCreationForm(action, user) {
    if (action === 'updating') {
      vm.creating = false;
      vm.updating = true;
      vm.selectedUser = user;
    } else {
      vm.creating = true;
      vm.updating = false;
      vm.selectedUser = {
        name: '',
        password: '',
        apiKey: ''
      };
    }
    vm.isVisible = !vm.isVisible;
  }

  function removeUser(index, user) {
    if($window.confirm('Diesen Benutzer wirklich löschen?')) {
      Users.remove(index, user);
    }
  }
}

function UsersService($http, API_PREFIX) {
  var vm = this;
  vm.users = [];

  return {
    getAll: getAll,
    getById: getById,
    create: create,
    remove: remove,
    update: update
  };

  function getAll(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/users'
    };
    $http(req).then(
      function onSuccess(response) {
        vm.users = response.data;
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
      });
  }

  function getById(id, cb) {
    cb(vm.users[id]);
  }

  function create(user, callback) {
    var paramString = createParamString(user);
    var req = {
      method: 'POST',
      url: API_PREFIX + '/users',
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    $http(req).then(
      function onSuccess(response) {
        user.id = response.data.id;
        vm.users.push(user);
        if (callback) callback();
      },
      function onError(response) {
        console.log(response);
        if (callback) callback();
      });
  }

  function remove(index, user, callback) {
    var id = user.ID || user.id;
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/users/' + id
    };
    $http(req).then(
      function onSuccess(response) {
        vm.users.splice(index, 1);
        if (callback) callback();
      },
      function onError(response) {
        console.log(response);
        if (callback) callback();
      });
  }

  function update(user, callback) {
    var id = user.id || user.ID;
    var paramString = createParamString(user);
    var req = {
      method: 'PUT',
      url: API_PREFIX + '/users/' + id,
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    $http(req).then(
      function onSuccess(response) {
        callback(response.data); // equals the number of rows changed, i.e. 1
      },
      function onError(response) {
        callback(response.data) // equals -1
      });
  }

  function createParamString(user) {
    var params = 'name=' + user.name 
      + '&password=' + user.password
      + '&apiKey=' + user.apiKey;
    return params;
  }
}

function FAUserForm(ADMIN_APP_PREFIX, UserService) {
  return {
    templateUrl: ADMIN_APP_PREFIX + 'pages/users/user-form.tpl.html',
    scope: {
      visible: '=',
      creating: '=',
      updating: '=',
      user: '='
    },
    restrict: 'E',
    link: link
  };

  function link(scope, element, attrs) {
    scope.create = create;
    scope.update = update;
    scope.reset = reset;

    function create() {
      if (validateForm()) {
        UserService.create(scope.user, function() {
          scope.user = {
            id: '',
            name: '',
            password: '',
            apiKey: ''
          };
        });

      }
    }

    function update() {
      UserService.update(scope.user, function() {
        scope.user = {
          id: '',
          name: '',
          password: '',
          apiKey: ''
        };
        scope.reset();
      });
    }

    function reset() {
      scope.visible = false;
    }

    function validateForm() {
      // TODO: do more validation, e.g. with $sce service
      if (scope.user.name !== '' &&
          scope.user.password !== '' &&
          scope.user.apiKey !== '') {
        return true;
      }
      return false;
    }
  }
}

})();
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.images';
angular.module(moduleName, [
  'fa.shared.constants'
]);

angular
.module(moduleName)
.config(Routes);;

angular
.module(moduleName)
.controller('ImagesController', ImagesController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

ImagesController.$inject = [
  '$window',
  'API_PREFIX',
  'FileUploader',
  'Images'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('images', {
    url: '/images',
    templateUrl: ADMIN_APP_PREFIX + 'pages/images/images.tpl.html',
    controller: 'ImagesController as vm'
  });
}

function ImagesController($window, API_PREFIX, FileUploader, Images) {
  var vm = this;
  vm.images = [];
  vm.uploader = new FileUploader({
    url: API_PREFIX + '/images'
  });
  vm.removeFile = removeFile;

  activate();

  function activate() {
    getFiles();
  }

  function getFiles() {
    Images.getAll(function(files) {
      vm.images = files;
    });
  }

  function removeFile(index, file) {
    if($window.confirm('Diese Datei wirklich löschen?')) {
      Images.remove(file.id, function(files) {
        //vm.images = files;
        vm.images.splice(index, 1);
      });
    }
  }

  vm.uploader.onSuccessItem = function() {
    vm.uploader.clearQueue();
    getFiles();
  };
  vm.uploader.onErrorItem = function() {
    //console.info('onErrorItem');
    //console.log(arguments);
  };
  vm.uploader.onCancelItem = function() {
    //console.info('onCancelItem');
  };
  vm.uploader.onCompleteItem = function() {
    //console.info('onCompleteItem');
    // set success message
  };
  vm.uploader.onAfterAddingFile = function(item) {
    item.uploadedAt = Date.now();
    //console.log(item);
  };
}

/*function ImagesService($http, API_PREFIX) {
  var vm = this;
  vm.files = [];


  return {
    getAll: getAll,
    remove: remove
  };

  function getAll(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/images'
    };
    $http(req).then(
      function onSuccess(response) {
        vm.files = response.data;
        cb(vm.files);
      }, function onError(error) {
        console.log(error);
      }
    );
  }

  function remove(id, cb) {
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/images/' + id
    };
    $http(req).then(
      function onSuccess(response) {
        vm.files.splice(id, 1);
        getAll(cb);
      }, function onError(error) {
        console.log(error);
      }
    );
  }
}*/

})();
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.departments';
angular.module(moduleName, [
  'fa.shared.services',
  'fa.shared.constants',
  'fa.admin.images'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('DepartmentsController', DepartmentsController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

DepartmentsController.$inject = [
  '$window',
  'Departments',
  'Images',
  'API_PREFIX'
];


function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('departments', {
    url: '/departments',
    templateUrl: ADMIN_APP_PREFIX + 'pages/departments/departments.tpl.html',
    controller: 'DepartmentsController as vm'
  })
  ;
}

function DepartmentsController($window,
                               Departments,
                               Images,
                               API_PREFIX) {
  var vm = this;
  vm.editable = false;
  vm.saving = true;
  vm.updating = false;

  vm.department = {
    id: '',
    numeral: '',
    name: '',
    imageId: ''
  };
  vm.departments = [];

  vm.pictures = [];
  vm.selectedImage = {};
  vm.imagesVisible = false;
  vm.imageApi = API_PREFIX + '/images/';

  vm.toggleEditable = toggleEditable;
  vm.openEditor = openEditor;
  vm.saveDepartment = saveDepartment;
  vm.removeDepartment = removeDepartment;
  vm.editDepartment = editDepartment;
  vm.updateDepartment = updateDepartment;
  vm.cancel = cancel;
  vm.reset = reset;

  vm.toggleImages = toggleImages;
  vm.chooseImage = chooseImage;
  vm.unselectImage = unselectImage;

  activate();

  function activate() {
    Departments.getAll(function(departments) {
      vm.departments = departments;
    });
    Images.getAll(function(images) {
      vm.pictures = images;
    });
  }

  function toggleEditable() {
    vm.editable = !vm.editable;
    vm.updating = false;
    vm.saving = true;
  }

  function openEditor() {
    vm.reset();
    vm.editable = true;
    vm.updating = false;
    vm.saving = true;
  }

  function editDepartment(index, department) {
    console.log(department);
    Images.getById(department.imageId, function(image) {
      vm.department = department;
      vm.selectedImage = image;
      vm.editable = true;
      vm.updating = true;
      vm.saving = false;
    });
  }

  function saveDepartment() {
    vm.department.imageId = vm.selectedImage.id;
    Departments.create(vm.department, function() {
      vm.cancel();
    });
  }

  function removeDepartment(index, department) {
    if($window.confirm('Dieses Sachgebiet wirklich löschen?')) {
      Departments.remove(index, department, function() {});
    }
  }

  function updateDepartment() {
    vm.department.imageId = vm.selectedImage.id;
    Departments.update(vm.department, function() {
      vm.editable = false;
      vm.updating = false;
      vm.reset();
    });
  }

  function cancel() {
    vm.saving = true;
    vm.editable = false;
    vm.reset();
  }

  function reset() {
    vm.department = {
      id: '',
      numeral: '',
      name: '',
      imageId: ''
    };
    vm.imagesVisible = false;
    vm.selectedImage = null;
  }

  function toggleImages() {
    vm.imagesVisible = !vm.imagesVisible;
  }

  function chooseImage(event, pic) {
    vm.selectedImage = pic;
    vm.department.imageId = pic.id;
  }

  function unselectImage() {
    vm.selectedImage = null;
    vm.department.imageId = '';
  }
}

})();
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.staffCouncil';
angular.module(moduleName, [
  'fa.shared.services',
  'fa.shared.constants',
  'fa.admin.images'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('StaffCouncilController', StaffCouncilController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

StaffCouncilController.$inject = [
  '$window',
  'Images',
  'API_PREFIX'
];


function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('staffCouncil', {
    url: '/staff-council',
    templateUrl: ADMIN_APP_PREFIX + 'pages/staff-council/staff-council.tpl.html',
    controller: 'StaffCouncilController as vm'
  })
  ;
}

function StaffCouncilController($window,
                                Images,
                                API_PREFIX) {
  var vm = this;
  
}

})();
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.aspirants';
angular.module(moduleName, [
  'fa.shared.services',
  'fa.shared.constants',
  'fa.admin.images'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('AspirantsController', AspirantsController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

AspirantsController.$inject = [
  '$window',
  'Aspirants',
  'Images',
  'API_PREFIX'
];


function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('aspirants', {
    url: '/aspirants',
    templateUrl: ADMIN_APP_PREFIX + 'pages/aspirants/aspirants.tpl.html',
    controller: 'AspirantsController as vm'
  })
  ;
}

function AspirantsController($window,
                             Aspirants,
                             Images,
                             API_PREFIX) {
  var vm = this;
  vm.aspirants = [];

  activate();

  function activate() {
    Aspirants.getAll(function(aspirants) {
      vm.aspirants = aspirants;
      console.log(aspirants);
    });
  }

}

})();
/* eof */
/* bof */
(function() {
var moduleName = 'fa.admin.links';
angular.module(moduleName, [
  'fa.shared.constants'
]);

angular
.module(moduleName)
.config(Routes);

angular
.module(moduleName)
.controller('LinksController', LinksController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

LinksController.$inject = [
  '$scope',
  '$window',
  'API_PREFIX',
  'Links'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('links', {
    url: '/links',
    templateUrl: ADMIN_APP_PREFIX + 'pages/links/links.tpl.html',
    controller: 'LinksController as vm'
  });
}

function LinksController($scope,
                         $window,
                         API_PREFIX,
                         Links
                        ) {
  var vm = this;
  vm.isUpdating = false;
  vm.currentLink = {};
  vm.links = [];
  vm.create = create;
  vm.edit = edit;
  vm.update = update;
  vm.remove = remove;
  vm.reset = reset;

  activate();

  function activate() {
    getAll();
  }

  function getAll() {
    Links.getAll(function(links) {
      vm.links = links;
    });
  }

  function create() {
    Links.create(vm.currentLink, function(link) {
      getAll();
      reset();
    });
  }

  function edit(index, link) {
    vm.currentLink = link;
    vm.isUpdating = true;
  }

  function remove(index, link) {
    if($window.confirm('Diesen Link wirklich löschen?')) {
      Links.remove(index, link, function(events) {
        getAll();
      });
    }
  }

  function update() {
    Links.update(vm.currentLink, function(result) {
      // console.log(result);
      reset();
    });
  }

  function reset() {
    vm.isUpdating = false;
    vm.currentLink = {
      name: '',
      url: ''
    };
    $scope.form.$setUntouched();
    $scope.form.$setPristine();
    $scope.form.$rollbackViewValue();
  }
}

})();
/* eof */
/* bof */
(function() {
'use strict';

var moduleName = 'fa.admin.protocols';
angular.module(moduleName, [
  'ngSanitize',
  'ui.tinymce',
  'fa.shared.constants'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('ProtocolsController', ProtocolsController);

ProtocolsController.$inject = [
  '$scope',
  '$window',
  'API_PREFIX',
  'Articles',
  'ArticleFiles',
  'Departments',
  'Links'
];

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider

  .state('protocols', {
    url: '/protocols',
    templateUrl: ADMIN_APP_PREFIX +
                 'pages/protocols/protocols.tpl.html',
    controller: 'ProtocolsController as vm'
  })
  ;
}

/******************************************************************************
 * ProtocolsController
 ******************************************************************************/
function ProtocolsController($scope,
                             $window,
                             API_PREFIX,
                             Articles,
                             ArticleLinks,
                             Departments,
                             Links) {
  var vm = this;
  vm.today = '';
  vm.titlePlaceholder = '';
  vm.shortTextPlaceholder = '';
  //vm.fileApi = API_PREFIX + '/files/';

  vm.departments = [];
  vm.protocols = [];
  vm.protocol = {
    title: '',
    type: 'protocol',
    shortText: '',
    longText: '',
    createdAt: '',
    updatedAt: '',
    department: {},
    links: []
  };
  vm.updating = false;
  vm.filesVisible = false;
  vm.newLink = {};

  vm.toggleFiles = toggleFiles;
  vm.removeLink = removeLink;
  vm.resetLink = resetLink;
  vm.saveLink = saveLink;

  vm.create = create;
  vm.cancel = cancel;
  vm.update = update;
  vm.remove = remove;
  vm.edit = edit;

  vm.updateForm = updateForm;
  vm.insertText = insertText;

  activate();

  function activate() {
    Departments.getAll(function(departments) {
      vm.departments = departments;
      vm.protocol.department = vm.departments[0];
      vm.today = moment().format('L');
      vm.titlePlaceholder = 
        'Dienstbesprechung SG ' +
        vm.protocol.department.numeral;
      vm.shortTextPlaceholder =
        'Protokoll der Dienstbesprechung vom ' +
        vm.today + '.';
    });
    Articles.getProtocols(function(protocols) {
      vm.protocols = protocols;
    });
  }

  function updateForm() {
    vm.titlePlaceholder = 
      'Dienstbesprechung SG ' +
      vm.protocol.department.numeral;
  }

  function toggleFiles() {
    vm.filesVisible = !vm.filesVisible;
  }

  function removeLink(index, link) {
    Links.remove(link, function() {
      vm.protocol.links.splice(index, 1);
    });
  }

  function create() {
    Articles.create(vm.protocol, function(protocol) {
      ArticleLinks.appendLinks(
        protocol.id,
        vm.protocol.links,
        function(result) {
          protocol.links = links;
          //vm.protocols.unshift(protocol);
          vm.protocols.push(protocol);
          reset();
        }
      );
    });
  }

  function remove(index, article) {
    if($window.confirm('Dieses Protokoll wirklich löschen?')) {
      Articles.remove(article.id, function() {
        vm.protocols.splice(index - 1, 1);
        ArticleLinks.removeArticle(article.id, function() {});
      });
    }
  }

  function edit(protocol) {
    vm.updating = true;
    vm.protocol = protocol;
    vm.protocol.department = findDepartment(protocol.departmentId);
    ArticleLinks.getLinksForArticle(protocol.id, function(links) {
      vm.protocol.links = links;
    });
  }

  function update() {
    // update article data
    Articles.update(vm.protocol, function(touchedRows) {
      // remove old associated files
      ArticleLinks.removeArticle(vm.protocol.id, function(res) {
        // append new associated files
        ArticleLinks.appendLinks(
          vm.protocol.id,
          vm.protocol.links,
          function(result) {
            console.log(result);
            cancel();
          }
        );
      });
    });
  }

  function cancel() {
    vm.updating = false;
    reset();
  }

  function reset() {
    vm.protocol = {
      title: '',
      type: 'protocol',
      shortText: '',
      longText: '',
      createdAt: '',
      updatedAt: '',
      department: vm.departments[0],
      files: []
    };
    vm.filesVisible = false;
    $scope.form.$setUntouched();
    $scope.form.$setPristine();
    $scope.form.$rollbackViewValue();
  }

  function resetLink() {
    vm.newLink = {
      name: '',
      url: ''
    };
  }

  function insertText() {
    vm.protocol.title = vm.titlePlaceholder;
    vm.protocol.shortText = vm.shortTextPlaceholder;
  }

  function findDepartment(id) {
    var result = {};
    vm.departments.forEach(function(entry) {
      if (entry.id === id) {
        result = entry;
      }
    });
    return result;
  }

}

})();
/* eof */
(function() {
'use strict';

angular.module('fa-admin', [
  'ui.router',
  'angularFileUpload',
  'angular-loading-bar',
  'fa.shared.services',
  'fa.components.articlebox',
  'fa.components.filechooser',
  'fa.admin.overview',
  'fa.admin.articles',
  'fa.admin.departments',
  'fa.admin.events',
  'fa.admin.files',
  'fa.admin.images',
  'fa.admin.rooms',
  'fa.admin.users'
]);
angular.module('fa-admin').config(Routes);
angular.module('fa-admin').run(RunBlock);

Routes.$inject = [
  '$urlRouterProvider'
];

RunBlock.$inject = [
  '$window'
];

function Routes($urlRouterProvider) {
  $urlRouterProvider.otherwise('/overview');
}

function RunBlock($window) {
  moment.locale('de');
  var windowElement = angular.element($window);
  windowElement.on('beforeunload', function (event) {
    // the following line of code will prevent reload or navigating away.
    // console.log(event);
    //event.preventDefault();
  });
}

})();(function() {
'use strict';

angular.module('fa-editorial', [
  'ui.router',
  'angularFileUpload',
  'angular-loading-bar',
  'fa.shared.constants',
  'fa.shared.services',
  //'ui.tinymce',
  'fa.components.filechooser',
  //'fa.admin.overview',
  'fa.admin.files',
  'fa.admin.images',
  'fa.admin.articles'
]);
angular.module('fa-editorial').config(Routes);
angular.module('fa-editorial').run(RunBlock);

Routes.$inject = [
  '$urlRouterProvider'
];

RunBlock.$inject = ['$window'];

function Routes($urlRouterProvider) {
  $urlRouterProvider.otherwise('/articles');
}

function RunBlock($window) {
  moment.locale('de');
  var windowElement = angular.element($window);
  windowElement.on('beforeunload', function (event) {
    // the following line of code will prevent reload or navigating away.
    // console.log(event);
    //event.preventDefault();
  });
}

})();(function() {
'use strict';

angular.module('fa-reservations', [
  'ui.router',
  'angularFileUpload',
  'angular-loading-bar',
  'fa.shared.services',
  'fa.shared.constants',
  'fa.components.filechooser',
  'fa.admin.overview',
  'fa.admin.rooms',
  'fa.admin.events'
]);
angular.module('fa-reservations').config(Routes);
angular.module('fa-reservations').run(RunBlock);

Routes.$inject = [
  '$urlRouterProvider'
];

RunBlock.$inject = ['$window'];

function Routes($urlRouterProvider) {
  $urlRouterProvider.otherwise('/events');
}

function RunBlock($window) {
  moment.locale('de');
  var windowElement = angular.element($window);
  windowElement.on('beforeunload', function (event) {
    // the following line of code will prevent reload or navigating away.
    // console.log(event);
    //event.preventDefault();
  });
}

})();