(function() {
'use strict';

angular.module('fa-admin', [
  'ui.router',
  'angularFileUpload',
  'angular-loading-bar',

  'fa.shared.components',
  'fa.shared.services',

  'fa.components.articlepanel',
  'fa.components.filechooser',
  'fa.components.imagechooser',
  'fa.components.linkcreator',

  // 'fa.admin.overview',
  'fa.admin.articles',
  'fa.admin.aspirants',
  'fa.admin.departments',
  'fa.admin.events',
  'fa.admin.files',
  'fa.admin.images',
  'fa.admin.links',
  // 'fa.admin.protocols',
  'fa.admin.rooms',
  // 'fa.admin.staffCouncil',
  'fa.admin.users'
]);
angular.module('fa-admin').config(Routes);
angular.module('fa-admin').run(RunBlock);

Routes.$inject = [
  '$urlRouterProvider'
];

RunBlock.$inject = [
  '$window'
];

function Routes($urlRouterProvider) {
  $urlRouterProvider.otherwise('/overview');
}

function RunBlock($window) {
  moment.locale('de');
  var windowElement = angular.element($window);
  windowElement.on('beforeunload', function (event) {
    // the following line of code will prevent reload or navigating away.
    // console.log(event);
    //event.preventDefault();
  });
}

})();