#!/bin/bash

DIRECTORY="/usr/vdv2/fa/www/DEV/client/admin/app/"
OUTPUTMIN="$DIRECTORY"admin.bundle.js
ADMINJS="$DIRECTORY"admin.js
EDITORIALJS="$DIRECTORY"editorial.js
RESERVATIONSJS="$DIRECTORY"reservations.js

function cleanBundleFile {
  if [ -f $OUTPUTMIN ]; then
    rm "$OUTPUTMIN"
  fi
}

function collectFileNames {
  files=$(find $DIRECTORY -name '*.js')
}

function concatFiles {
  for f in $files
  do
    if [ $f == $ADMINJS ]; then
      continue
    fi
    if [ $f == $EDITORIALJS ]; then
      continue
    fi
    if [ $f == $RESERVATIONSJS ]; then
      continue
    fi
    cat "$f" >> $OUTPUTMIN
  done
}

function printLineCount {
  lines=`wc -l "$OUTPUTMIN" | cut -f1 -d' '`
  echo "Wrote $lines lines to $OUTPUTMIN"
}

function changeAccessRights {
  chmod 644 "$OUTPUTMIN"
}

cleanBundleFile
collectFileNames
concatFiles
changeAccessRights
printLineCount
