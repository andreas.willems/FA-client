/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.articlepanel';

angular.module(moduleName, []);
angular.module(moduleName).directive('faArticlePanel', ArticlePanel);

ArticlePanel.$inject = [
  'ADMIN_APP_PREFIX',
  'Messages'
];

function ArticlePanel(ADMIN_APP_PREFIX, Messages) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX + 'components/articlepanel/fa.articlepanel.tpl.html',
    scope: {
      articles: '=',
      heading: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.edit = function(article) {
      Messages.broadcast('article:edit', {
        article: article
      });
    }

    scope.remove = function(index, article) {
      Messages.broadcast('article:remove', {
        article: article,
        index: index,
      });
    }
  }
}

})();
/* eof */
 
