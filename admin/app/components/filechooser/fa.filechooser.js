/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.filechooser';

angular.module(moduleName, [
  'angular-loading-bar'
]);
angular.module(moduleName).directive('faFileChooser', FileChooser);

FileChooser.$inject = [
  '$window',
  'ADMIN_APP_PREFIX',
  'API_PREFIX',
  'cfpLoadingBar',
  'Files'
];

function FileChooser($window,
                     ADMIN_APP_PREFIX,
                     API_PREFIX,
                     cfpLoadingBar,
                     Files) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX + 'components/filechooser/fa.filechooser.tpl.html',
    scope: {
      articleFiles: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.fileApi = API_PREFIX + '/files/';
    scope.files = [];
    scope.addFile = addFile;

    activate();

    function activate() {
      cfpLoadingBar.start();
      Files.getAll(function(files) {
        scope.files = files;
        cfpLoadingBar.complete();
      });
    }

    function addFile(file) {
//       if (!scope.articleLinks) {
//         scope.articleLinks = [];
//       }
//       scope.articleLinks.push(link);
     copyToClipboard(scope.fileApi + file.id);
    }

    function copyToClipboard(text) {
      $window.prompt("In Zwischenablage kopieren: STRG + C, Enter", text);
    }
  }
}

})();
/* eof */
