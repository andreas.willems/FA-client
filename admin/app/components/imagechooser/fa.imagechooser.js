/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.imagechooser';

angular.module(moduleName, [
  'angular-loading-bar'
]);
angular.module(moduleName).directive('faImageChooser', ImageChooser);

ImageChooser.$inject = [
  'ADMIN_APP_PREFIX',
  'API_PREFIX',
  'cfpLoadingBar',
  'Images'
];

function ImageChooser(ADMIN_APP_PREFIX,
                      API_PREFIX,
                      cfpLoadingBar,
                      Images) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX +
      'components/imagechooser/fa.imagechooser.tpl.html',
    scope: {
      myImage: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.imageApi = API_PREFIX + '/images/';
    scope.images = [];
    scope.addImage = addImage;

    activate();

    function activate() {
      cfpLoadingBar.start();
      Images.getAll(function(images) {
        scope.images = images;
        cfpLoadingBar.complete();
      });
    }

    function addImage(image) {
      //scope.myImage = image.id;
      scope.myImage = image;
    }
  }
}

})();
/* eof */
