/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.linkchooser';

angular.module(moduleName, [
  'angular-loading-bar'
]);
angular.module(moduleName).directive('faLinkChooser', LinkChooser);

LinkChooser.$inject = [
  '$window',
  'ADMIN_APP_PREFIX',
  'API_PREFIX',
  'cfpLoadingBar',
  'Links'
];

function LinkChooser($window,
                     ADMIN_APP_PREFIX,
                     API_PREFIX,
                     cfpLoadingBar,
                     Links) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX +
      'components/linkchooser/fa.linkchooser.tpl.html',
    scope: {
      articleLinks: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.linkApi = API_PREFIX + '/links/';
    scope.links = [];
    scope.addLink = addLink;
    scope.followLink = followLink;

    activate();

    function activate() {
      cfpLoadingBar.start();
      Links.getAll(function(links) {
        scope.links = links;
        cfpLoadingBar.complete();
      });
    }

    function addLink(link) {
//       if (!scope.articleLinks) {
//         scope.articleLinks = [];
//       }
//       scope.articleLinks.push(link);
      copyToClipboard(link.url);
    }

    function copyToClipboard(text) {
      $window.prompt("In Zwischenablage kopieren: STRG + C, Enter", text);
    }

    function followLink(link) {
      $window.open(link.url, '_blank');
    }
  }
}

})();
/* eof */
