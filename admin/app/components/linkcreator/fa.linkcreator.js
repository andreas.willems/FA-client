/* bof */
(function() {
'use strict';

var moduleName = 'fa.components.linkcreator';

angular.module(moduleName, [
  'angular-loading-bar'
]);
angular.module(moduleName).directive('faLinkCreator', LinkCreator);

LinkCreator.$inject = [
  '$window',
  'ADMIN_APP_PREFIX',
  'API_PREFIX',
  'cfpLoadingBar',
  'Links'
];

function LinkCreator($window,
                     ADMIN_APP_PREFIX,
                     API_PREFIX,
                     cfpLoadingBar,
                     Links) {
  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX +
      'components/linkcreator/fa.linkcreator.tpl.html',
    scope: {
      articleLinks: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.linkApi = API_PREFIX + '/links/';
    scope.newLink = {
      name: '',
      url: ''
    };
    scope.saveLink = saveLink;
    scope.resetLink = resetLink;
    scope.toggleFiles = toggleFiles;

    function saveLink() {
      //console.log(scope.newLink);
      if (scope.newLink.name != '' && scope.newLink.url != '') {
//         Links.create(scope.newLink, function(link) {
//           if (!scope.articleLinks) {
//             scope.articleLinks = [];
//           }
//           scope.articleLinks.push(link);
//           resetLink();
//         });
        if (!scope.articleLinks) {
          scope.articleLinks = [];
        }
        scope.articleLinks.push(scope.newLink);
        resetLink();
      }
    }

    function resetLink() {
      scope.newLink = {
        name: '',
        url: ''
      };
      scope.linkform.$setUntouched();
      scope.linkform.$setPristine();
      scope.linkform.$rollbackViewValue();
    }

    function toggleFiles() {
      scope.filesVisible = !scope.filesVisible;
    }
  }
}

})();
/* eof */
