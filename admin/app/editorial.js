(function() {
'use strict';

angular.module('fa-editorial', [
  'ui.router',
  'angularFileUpload',
  'angular-loading-bar',

  'fa.shared.components',
  'fa.shared.services',

  'fa.components.filechooser',
  'fa.components.imagechooser',
  'fa.components.linkcreator',

  'fa.admin.articles',
  'fa.admin.aspirants',
  'fa.admin.departments',
  'fa.admin.files',
  'fa.admin.images',
  'fa.admin.links',
  // 'fa.admin.protocols',
  // 'fa.admin.staffCouncil',
]);
angular.module('fa-editorial').config(Routes);
angular.module('fa-editorial').run(RunBlock);

Routes.$inject = [
  '$urlRouterProvider'
];

RunBlock.$inject = ['$window'];

function Routes($urlRouterProvider) {
  $urlRouterProvider.otherwise('/articles');
}

function RunBlock($window) {
  moment.locale('de');
  var windowElement = angular.element($window);
  windowElement.on('beforeunload', function (event) {
    // the following line of code will prevent reload or navigating away.
    // console.log(event);
    //event.preventDefault();
  });
}

})();