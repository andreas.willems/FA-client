/* bof */
(function() {
'use strict';

var moduleName = 'fa.admin.articles';

angular.module(moduleName).factory('ArticleTemplates', ArticleTemplates);

ArticleTemplates.$inject = [];

function ArticleTemplates() {

  return {
    getTypes: getTypes,
    getTemplate: getTemplate
  };

  function getTypes(cb) {
    cb([
      {
        name: 'market',
        text: 'Markt'
      }, {
        name: 'personal',
        text: 'Personal'
      }, {
        name: 'protocol',
        text: 'Protokoll'
      }, {
        name: 'other',
        text: 'Sonstige'
      }
    ]);
  }

  function getTemplate(tplType, cb) {
    var tpl = {};
    switch (tplType) {
      case 'market': {
        tpl = getMarketTemplate();
        break;
      }
      case 'personal': {
        tpl = getPersonalTemplate();
        break;
      }
      case 'protocol': {
        tpl = getProtocolTemplate();
        break;
      }
      case 'other': {
        tpl = getOtherTemplate();
        break;
      }
      default: {
        tpl = getOtherTemplate();
      }
    }
    cb(tpl);
  }

  function getAbstractTemplate() {
    return {
      title: '',
      type: '',
      shortText: '',
      longText: '',
      createdAt: '',
      updatedAt: '',
      department: {id: 0},
      files: []
    };
  }

  function getMarketTemplate() {
    var tpl = getAbstractTemplate();
    tpl.title = 'Verkaufe Gegenstand';
    tpl.shortText = 'Verkaufe folgende Gegenstände: (Gegenstände).<br/> Tel: (Durchwahl)';
    tpl.type = 'market';
    return tpl;
  }

  function getPersonalTemplate() {
    var tpl = getAbstractTemplate();
    tpl.title = 'Personalinformationen';
    tpl.type = 'personal';
    tpl.shortText = 'Dokumentation der Personaländerungen.';
    return tpl;
  }

  function getProtocolTemplate() {
    var today = moment().format('L');
    var tpl = getAbstractTemplate();
    tpl.title = 'Dienstbesprechung SG ...';
    tpl.shortText = 'Protokoll der Dienstbesprechung vom ' + today;
    tpl.type = 'protocol';
    tpl.department = null;
    return tpl;
  }

  function getOtherTemplate() {
    var tpl = getAbstractTemplate();
    tpl.title = 'Sonstiges';
    tpl.type = 'other';
    return tpl;
  }

}

})();
/* eof */
