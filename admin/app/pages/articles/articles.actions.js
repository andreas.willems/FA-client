(function() {
'use strict';

angular
.module('fa.admin.articles')
.factory('ArticlesActions', ArticlesActions);

ArticlesActions.$inject = [];

function ArticlesActions() {
  return {
    sayHello: sayHello
  };

  function sayHello() {
    //console.log('Hello from ArticlesActions');
  }

  function create() {

  }

  function read() {

  }

  function update() {

  }

  function remove() {

  }
}


})();
