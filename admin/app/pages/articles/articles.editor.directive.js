(function() {
'use strict';

angular
.module('fa.admin.articles')
.directive('faArticlesPageEditor', ArticlesPageEditor);

ArticlesPageEditor.$inject = ['ADMIN_APP_PREFIX'];

function ArticlesPageEditor(ADMIN_APP_PREFIX) {

  ArticlePageEditorController.$inject = [
    '$sanitize',
    '$scope',
    '$state',
    '$stateParams',
    '$window',
    'Articles',
    'ArticlesActions',
    'ArticleLinks',
    'ArticleTemplates',
    'Departments',
    'Links'
  ];

 /******************************************************************************
  * ArticlePageEditorController
  *****************************************************************************/
  function ArticlePageEditorController($sanitize,
                                       $scope,
                                       $state,
                                       $stateParams,
                                       $window,
                                       Articles,
                                       ArticlesActions,
                                       ArticleLinks,
                                       ArticleTemplates,
                                       Departments,
                                       Links) {

    var vm = this;
    vm.article = {};
    vm.articleTypes = [];
    vm.departments = [];
    vm.selectedType = {};
    vm.updating = false;
    vm.resultMessage = {
      type: '',
      text: ''
    }

    vm.cancel = cancel;
    vm.create = create;
    vm.followLink = followLink;
    vm.hideMessage = hideMessage;
    vm.update = update;
    vm.updateDepartment = updateDepartment;
    vm.updateForm = updateForm;
    vm.removeLink = removeLink;
    vm.showMessage = showMessage;

    activate();

    function activate() {
      if ($stateParams.data) {
        vm.article = $stateParams.data.article;
        vm.updating = true;
      }
      ArticleTemplates.getTypes(function(types) {
        vm.articleTypes = types;
      });
      Departments.getAll(function(departments) {
        vm.departments = departments;
      });
    }

    function cancel() {
      vm.updating = false;
      reset();
    }

    function create() {
      var sanitizedArticle = sanitizeArticle(vm.article);
      console.log(sanitizedArticle);
      Articles
      .create(sanitizedArticle)
      .then((article) => {
        showMessage('is-success', 'Speichern erfolgreich.');
        reset();
      })
      .catch(function(err) {
        console.error(err);
        showMessage(
          'is-danger',
          'Ein Fehler ist aufgetreten. Speichern nicht erfolgreich.'
        );
      });
    }

    function sanitizeArticle(article) {
      var sanitizedProps = {
        title: encodeURIComponent(article.title),
        shortText : encodeURIComponent(article.shortText)
      };
      var sanitizedArticle = {};
      angular.merge(sanitizedArticle, article, sanitizedProps);
      return sanitizedArticle;
    }

    function update() {
      vm.article.updatedAt = Date.now();
      var sanitizedArticle = sanitizeArticle(vm.article);
      Articles
      .update(sanitizedArticle)
      .then((article) => {
        showMessage('is-success', 'Speichern erfolgreich.');
        vm.updating = false;
        reset();
      })
      .catch((err) => {
        console.error(err);
        showMessage(
          'is-danger',
          'Ein Fehler ist aufgetreten. Änderung nicht erfolgreich.'
        );
      });
    }

    function createLinksFromArticle(response) {
      var newArticle = response.data;
      var newLinks = [];
      // create links
      vm.article.links.forEach(function(item) {
        Links.create(item, function(createdLink) {
          newLinks.push(createdLink);
        });
      });
      newArticle.links = newLinks;
      return newArticle;
    }

    function associateArticleWithLinks(article) {
      return ArticleLinks.appendLinks(article.id, article.links);
    }

    function edit(article) {
      ArticleLinks.getLinksForArticle(article.id, function(links) {
        article.links = links;
        vm.article = article;
        vm.updating = true;
      });
    }

    function followLink(link) {
      $window.open(link.url, '_blank');
    }

    function hideMessage() {
      vm.resultMessage = {type: '', text: ''}
    }

    function removeLink(index, link) {
      vm.article.links.splice(index, 1);
    }

    function reset() {
      vm.article = {
        title: '',
        type: '',
        shortText: '',
        longText: '',
        createdAt: '',
        updatedAt: '',
        department: {id:0},
        links: []
      };
      vm.article.links = [];
      vm.filesVisible = false;
      vm.selectedType = {};
      $scope.form.$setUntouched();
      $scope.form.$setPristine();
      $scope.form.$rollbackViewValue();
    }

    function toggleFiles() {
      vm.filesVisible = !vm.filesVisible;
    }

    function updateDepartment(department) {
      vm.article.title =
       'Dienstbesprechung des SG ' +
       vm.article.department.numeral;
    }

    function updateForm() {
      //console.log(vm.selectedType);
      ArticleTemplates.getTemplate(
        vm.selectedType.name,
        function(template) {
          //console.log(template);
          vm.article = template;
          vm.article.createdAt = Date.now();
          vm.article.updatedAt = Date.now();
          vm.article.links = [];
        }
      );
    }

    function showMessage(type, msg) {
      vm.resultMessage = {type: type, text: msg};
    }
  }

  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX + 'pages/articles/articles.editor.tpl.html',
    scope: {},
    controller: ArticlePageEditorController,
    controllerAs: 'vm'
  };
}

})();