(function() {
'use strict';

angular
.module('fa.admin.articles')
.directive('faArticlesPageList', ArticlesPageList);

ArticlesPageList.$inject = ['ADMIN_APP_PREFIX', 'Articles'];

function ArticlesPageList(ADMIN_APP_PREFIX, Articles) {

  ArticlePageListController.$inject = ['Articles', 'Messages'];

  function ArticlePageListController(Articles, Messages) {
    var vm = this;
    vm.articles = [];
    vm.edit = edit;
    vm.heading = 'Vorhandene Artikel';
    vm.remove = remove;
    vm.today = '';

    activate();

    function activate() {
      Messages.listen('articles:updated', function() {
        getAll();
      });
      getAll();
    }

    function edit(article) {
      Messages.broadcast('article:edit', {
        article: article
      });
    }

    function getAll() {
      Articles.getAll(articles => {
        vm.articles = articles;
      });
    }

    function remove(article, index) {
      Messages.broadcast('article:remove', {
        article: article,
        index: index,
      });
    }
  }

  return {
    restrict: 'E',
    templateUrl: ADMIN_APP_PREFIX + 'pages/articles/articles.list.tpl.html',
    scope: {},
    controller: ArticlePageListController,
    controllerAs: 'vm'
  };

}

})();