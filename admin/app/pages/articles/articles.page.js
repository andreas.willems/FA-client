/* bof */
(function() {
'use strict';

var moduleName = 'fa.admin.articles';
angular.module(moduleName, [
  'ngSanitize',
  'ui.tinymce',
  'fa.shared.constants'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('ArticleController', ArticleController);


ArticleController.$inject = [
  '$state',
  '$window',
  'Articles',
  'ArticleLinks',
  'Messages'
];

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('articles', {
    url: '/articles',
    templateUrl: ADMIN_APP_PREFIX + 'pages/articles/articles.tpl.html',
    controller: 'ArticleController as vm'
  })
  .state('articles.editor', {
    url: '/editor',
    template: '<fa-articles-page-editor></fa-articles-page-editor>',
    params: {data: null}
  })
  .state('articles.list', {
    url: '/list',
    template: '<fa-articles-page-list></fa-articles-page-list>',
  })
  ;
}

/******************************************************************************
 * ArticleController
 ******************************************************************************/
function ArticleController($state,
                           $window,
                           Articles,
                           ArticleLinks,
                           Messages) {
  var vm = this;
  vm.remove = remove;

  activate();

  function activate() {

    Messages.listen('article:edit', function(articleData) {
      $state.go('articles.editor', {data: articleData}, {});
    });

    Messages.listen('article:remove', function onSuccess(data) {
      remove(data.index, data.article);
      //$state.go('articles.editor');
    });
  }

  function remove(index, article) {
    if($window.confirm('Diesen Artikel wirklich löschen?')) {
      Articles.remove(article.id, function() {
        ArticleLinks.removeArticle(article.id, function() {
          Messages.broadcast('articles:updated', {});
        });
      });
    }
  }
}

})();
/* eof */
