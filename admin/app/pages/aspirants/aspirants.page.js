/* bof */
(function() {
var moduleName = 'fa.admin.aspirants';
angular.module(moduleName, [
  'fa.shared.services',
  'fa.shared.constants',
  'fa.admin.images'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('AspirantsController', AspirantsController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

AspirantsController.$inject = [
  '$window',
  'Aspirants',
  'Images',
  'API_PREFIX'
];


function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('aspirants', {
    url: '/aspirants',
    templateUrl: ADMIN_APP_PREFIX + 'pages/aspirants/aspirants.tpl.html',
    controller: 'AspirantsController as vm'
  })
  ;
}

function AspirantsController($window,
                             Aspirants,
                             Images,
                             API_PREFIX) {
  var vm = this;
  vm.aspirants = [1,2,3,4];
  vm.aspirant = {
    name: '',
    year: '2016',
    career: 'StAnw',
    imageId: '0'
  };
  vm.formVisible = false;
  vm.imageApi = API_PREFIX + '/images/';
  vm.updating = false;

  vm.cancel = cancel;
  vm.create = create;
  vm.edit = edit;
  vm.remove = remove;
  vm.toggleForm = toggleForm;
  vm.update = update;

  activate();

  function activate() {
    getAll();
  }

  function cancel() {
    reset();
  }

  function create() {
    Aspirants.create(vm.aspirant, function(aspirant) {
      vm.aspirants.push(aspirant);
      reset();
    });
  }

  function edit(aspirant) {
    vm.aspirant = aspirant;
    vm.formVisible = true;
    vm.updating = true;
  }

  function getAll() {
    Aspirants.getAll(function(aspirants) {
      vm.aspirants = aspirants;
    });
  }

  function remove(index, aspirant) {
    if($window.confirm('Diesen Anwärter wirklich löschen?')) {
      Aspirants.remove(aspirant, function() {
        getAll();
      });
    }
  }

  function reset() {
    vm.aspirant = {
      name: '',
      year: '2016',
      career: 'StAnw',
      imageId: '1'
    };
    vm.formVisible = false;
    vm.updating = false;
  }

  function toggleForm() {
    vm.formVisible = !vm.formVisible;
  }

  function update() {
    Aspirants.update(vm.aspirant, function() {
      getAll();
      reset();
    });
  }

}

})();
/* eof */
