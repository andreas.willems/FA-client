/* bof */
(function() {
var moduleName = 'fa.admin.departments';
angular.module(moduleName, [
  'fa.shared.services',
  'fa.shared.constants',
  'fa.admin.images'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('DepartmentsController', DepartmentsController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

DepartmentsController.$inject = [
  '$window',
  'Departments',
  'Images',
  'API_PREFIX'
];


function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('departments', {
    url: '/departments',
    templateUrl: ADMIN_APP_PREFIX + 'pages/departments/departments.tpl.html',
    controller: 'DepartmentsController as vm'
  })
  ;
}

function DepartmentsController($window,
                               Departments,
                               Images,
                               API_PREFIX) {
  var vm = this;
  vm.editable = false;
  vm.saving = true;
  vm.updating = false;

  vm.department = {
    id: '',
    numeral: '',
    name: '',
    imageId: ''
  };
  vm.departments = [];

  vm.pictures = [];
  vm.selectedImage = {};
  vm.imagesVisible = false;
  vm.imageApi = API_PREFIX + '/images/';

  vm.toggleEditable = toggleEditable;
  vm.openEditor = openEditor;
  vm.saveDepartment = saveDepartment;
  vm.removeDepartment = removeDepartment;
  vm.editDepartment = editDepartment;
  vm.updateDepartment = updateDepartment;
  vm.cancel = cancel;
  vm.reset = reset;

  vm.toggleImages = toggleImages;
  vm.chooseImage = chooseImage;
  vm.unselectImage = unselectImage;

  activate();

  function activate() {
    Departments.getAll(function(departments) {
      vm.departments = departments;
    });
    Images.getAll(function(images) {
      vm.pictures = images;
    });
  }

  function toggleEditable() {
    vm.editable = !vm.editable;
    vm.updating = false;
    vm.saving = true;
  }

  function openEditor() {
    vm.reset();
    vm.editable = true;
    vm.updating = false;
    vm.saving = true;
  }

  function editDepartment(index, department) {
    console.log(department);
    Images.getById(department.imageId, function(image) {
      vm.department = department;
      vm.selectedImage = image;
      vm.editable = true;
      vm.updating = true;
      vm.saving = false;
    });
  }

  function saveDepartment() {
    vm.department.imageId = vm.selectedImage.id;
    Departments.create(vm.department, function() {
      vm.cancel();
    });
  }

  function removeDepartment(index, department) {
    if($window.confirm('Dieses Sachgebiet wirklich löschen?')) {
      Departments.remove(department, function() {});
    }
  }

  function updateDepartment() {
    vm.department.imageId = vm.selectedImage.id;
    Departments.update(vm.department, function() {
      vm.editable = false;
      vm.updating = false;
      vm.reset();
    });
  }

  function cancel() {
    vm.saving = true;
    vm.editable = false;
    vm.reset();
  }

  function reset() {
    vm.department = {
      id: '',
      numeral: '',
      name: '',
      imageId: ''
    };
    vm.imagesVisible = false;
    vm.selectedImage = null;
  }

  function toggleImages() {
    vm.imagesVisible = !vm.imagesVisible;
  }

  function chooseImage(event, pic) {
    vm.selectedImage = pic;
    vm.department.imageId = pic.id;
  }

  function unselectImage() {
    vm.selectedImage = null;
    vm.department.imageId = '';
  }
}

})();
/* eof */
