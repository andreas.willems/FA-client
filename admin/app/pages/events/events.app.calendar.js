(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppCalendar', {

  bindings: {
    month: '<'
  , events: '<'
  }

  , template: `
    <h2 class="title has-text-centered">{{$ctrl.month.format('MMMM YYYY')}}</h2>

    <div class="is-fullwidth">
      <ul class="weekdays">
        <li ng-repeat="weekday in $ctrl.weekdays">{{weekday}}</li>
      </ul>
    </div>

    <div class="is-fullwidth">

      <div class="columns is-multiline is-gapless"
           id="calendar">

        <div class="column" ng-repeat="day in $ctrl.daysInMonth">

          <events-app-day day="day"></events-app-day>

        </div><!-- .column -->

      </div><!-- .columns -->

    </div><!-- .is-fullwidth -->
  `

  , controller: [ 'Messages', function(Messages) {

    var ctrl = this;
    ctrl.weekdays = [];
    ctrl.daysInMonth = [];

    ctrl.$onInit = () => {
      ctrl.weekdays = [
        'Montag'
      , 'Dienstag'
      , 'Mittwoch'
      , 'Donnerstag'
      , 'Freitag'
      , 'Samstag'
      , 'Sonntag'
      ];

      //console.log(ctrl.events);
    };

    ctrl.$onChanges = (changes) => {
      if (changes.month) {
        ctrl.daysInMonth =
          createDaysArray(changes.month.currentValue);
      }
      if (changes.events) {
        let arr = combineDaysAndEvents(
          ctrl.daysInMonth,
          changes.events.currentValue
        );
        ctrl.daysInMonth = arr;
      }
    }

  }]

});

function createDaysArray(month) {
  let result = [];

  result = createWeekdayPadding(result, month);
  result = createDays(result, month);

  return result;
}

function createWeekdayPadding(array, month) {
  let res = [].concat(array);
  let weekdayOfFirstInMonth = month.date(1).weekday();
  if (weekdayOfFirstInMonth != 0) {
    for (let i = 0; i < weekdayOfFirstInMonth; i++) {
      res.push({nr: 0});
    }
  }
  return res;
}

function createDays(array, month) {
  let res = [].concat(array);
  let daysInMonth = month.daysInMonth();
  for(let i = 1; i <= daysInMonth; i++) {
    res.push({nr: i});
  }
  return res;
}

function combineDaysAndEvents(days, events) {
  let result = days.map(day => {
    let newDay = angular.copy(day);
    newDay.events = [];

    events.forEach(event => {
      if (parseInt(event.day) === day.nr) {
        newDay.events.push(event);
      }
    });
    return newDay;
  });
  return result;
}

})();