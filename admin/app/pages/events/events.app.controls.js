(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppControls', {

  bindings: {}

  , template: `
    <div class="tabs is-centered is-fullwidth">
      <ul>
        <li>
          <a ng-click="$ctrl.lastMonth()">
            <span class="icon is-small">
              <i class="fa fa-chevron-left"></i>
            </span>
            <span>zurück</span>
          </a>
        </li>
        <li>
          <a ng-click="$ctrl.today()">
            <span class="icon is-small">
              <i class="fa fa-chevron-down"></i>
            </span>
            <span>Heute</span>
          </a>
        </li>
        <li>
          <a ng-click="$ctrl.nextMonth()">
            <span class="icon is-small">
              <i class="fa fa-chevron-right"></i>
            </span>
            <span>vor</span>
          </a>
        </li>
      </ul>
    </div>
  `

  , controller: [ 'Messages', function(Messages) {

    this.lastMonth = lastMonth;
    this.nextMonth = nextMonth;
    this.today = today;

    function lastMonth() {
      Messages.broadcast('events:lastMonth:request', {});
    }

    function nextMonth() {
      Messages.broadcast('events:nextMonth:request', {});
    }

    function today() {
      Messages.broadcast('events:today:request', {});
    }

  }]

});

})();