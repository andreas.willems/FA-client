(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppDay', {

  bindings: {
    day: '<'
  },

  template: `
    <div ng-hide="$ctrl.day.nr == 0"
         class="calendar-day">

      <div class="calendar-day-heading">
        {{$ctrl.day.nr}}
      </div>

      <div class="calendar-day-body">
        <p ng-repeat="event in $ctrl.day.events">
          <a ng-click="$ctrl.editEvent(event)"
             style="min-width:100%;max-width: 100%;">
            {{event.timeBegin | date: 'shortTime' }} <br/> {{event.name}}
          </a>
        </p>
      </div>

      <div class="overlay">
        <div class="">
          <a class="button is-primary is-outlined is-fullwidth"
             style="line-height: 2em"
             ng-click="$ctrl.openForm()">
            Termin hinzufügen
          </a>
        </div>
      </div>

    </div>
  `,

  controller: [ 'Messages', function(Messages) {

    let ctrl = this;
    ctrl.editEvent = editEvent;
    ctrl.openForm = openForm;

    function editEvent(event) {
      Messages.broadcast('events:edit:request', {
       day: ctrl.day,
       event: event
      });
    }

    function openForm() {
      Messages.broadcast('events:create:request', {
        day: ctrl.day
      });
    }

  }]

});

})();