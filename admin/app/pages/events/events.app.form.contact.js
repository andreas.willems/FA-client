(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppFormContact', {
  bindings: {
    eventContact: '<'
  },
  template: `
    <ng-form name="eventFormContact">
    <label class="label" for="eventContactTf">Ansprechpartner</label>
    <p class="control has-icon has-icon-right"
       style="margin-bottom: 10px;">
      <input
            type="text"
            name="eventContactTf"
            class="input"
            ng-class="!eventFormContact.eventContactTf.$touched ? '' :
                      (eventFormContact.eventContactTf.$valid ? 'is-success' : 'is-danger')"
            style="max-width: 100%;"
            id="eventContactTf"
            ng-model="$ctrl.eventContact"
            ng-model-options="{updateOn: 'blur'}"
            ng-change="$ctrl.update()"
            placeholder="Ansprechpartner"
            ng-required="true"
            required>
      <i class="fa fa-warning"
         ng-if="eventFormContact.eventContactTf.$touched
                && !eventFormContact.eventContactTf.$valid">
      </i>
      <span class="help is-danger is-pulled-right"
            ng-if="eventFormContact.eventContactTf.$touched
                   && !eventFormContact.eventContactTf.$valid">
        Ansprechpartner ist erforderlich.
      </span>
    </p>
    </ng-form>
  `,
  controller: ['Messages', function(Messages) {
    var ctrl = this;

    ctrl.$onChanges = changes => {
      //console.log(ctrl.eventConatct);
    };

    ctrl.update = () => {
      Messages.broadcast('events:form:contact:update', {
        eventContact: ctrl.eventContact
      });
    };
  }]
});

})();