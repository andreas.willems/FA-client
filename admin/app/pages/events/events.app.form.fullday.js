(function() {

var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppFormFullDay', {
  bindings: {
    isFullDay: '<'
  },

  template: `
    <ng-form name="eventFormFullDay">
      <label class="label">Uhrzeit</label>
      <p class="control"
         style="margin-bottom: 10px;">
        <label class="checkbox">
          <input type="checkbox"
                 ng-model="$ctrl.isFullDay"
                 ng-change="$ctrl.update()">
          Ganztägig
        </label>
      </p>
    </ng-form>
  `,

  controller: ['Messages', function(Messages) {
    var ctrl = this;

    ctrl.$onChanges = changes => {
      //console.log(changes);
    };

    ctrl.update = () => {
      Messages.broadcast('events:form:isFullDay:update', {
        isFullDay: ctrl.isFullDay
      });
    };
  }]
});

})();