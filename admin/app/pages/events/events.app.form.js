(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppForm', {

  bindings: {
    event: '<'
  },

  template: `
    <div class="heading">
      <h1 class="title">
      Termin {{$ctrl.message}} am
      <span ng-bind="::$ctrl.event.day.format('DD.MM.YYYY')"></span>
      </h1>
    </div>

    <br/>

    <form class="event-form"
          name="form"
          novalidate>

      <div class="columns">
        <div class="column">

          <events-app-form-name event-name="$ctrl.event.name">
          </events-app-form-name>

          <events-app-form-contact event-contact="$ctrl.event.contact">
          </events-app-form-contact>

          <events-app-form-full-day is-full-day="$ctrl.event.isFullDay">
          </events-app-form-full-day>

          <div class="columns">

            <div class="column">
              <events-app-form-time
                field="timeBegin"
                is-full-day="$ctrl.event.isFullDay"
                label="Beginn"
                time-string="$ctrl.event.timeBegin">
              </events-app-form-time>
            </div>

            <div class="column">
              <events-app-form-time
                field="timeEnd"
                is-full-day="$ctrl.event.isFullDay"
                label="Ende"
                time-string="$ctrl.event.timeEnd">
              </events-app-form-time>
            </div>
          </div>


        </div>
      </div>
    </form>
  `,

  controller: [ 'Messages', function(Messages) {

    let ctrl = this;
    ctrl.message = 'anlegen';

    ctrl.$onChanges = changes => {
      if (changes.event.currentValue) {
        if (changes.event.currentValue.id) {
          ctrl.message = 'ändern';
        } else {
          ctrl.message = 'anlegen';
        }
      }
    };

  }]

});

})();