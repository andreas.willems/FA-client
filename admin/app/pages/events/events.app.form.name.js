(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppFormName', {
  bindings: {
    eventName: '<'
  },
  template: `
    <ng-form name="eventFormName">
    <label class="label" for="eventNameTf">Bezeichnung</label>
    <p class="control has-icon has-icon-right"
       style="margin-bottom: 10px;">
      <input type="text"
             name="eventNameTF"
             class="input"
             ng-class="!eventFormName.eventNameTF.$touched ? '' : (eventFormName.eventNameTF.$valid ? 'is-success' : 'is-danger')"
             style="max-width: 100%;"
             id="eventNameTf"
             ng-model="$ctrl.eventName"
             ng-model-options="{updateOn: 'blur'}"
             ng-change="$ctrl.update()"
             placeholder="Bezeichnung"
             ng-required="true"
             required autofocus>
      <i class="fa fa-warning"
         ng-if="eventFormName.eventNameTF.$touched && !eventFormName.eventNameTF.$valid">
      </i>
      <span class="help is-danger is-pulled-right"
            ng-if="eventFormName.eventNameTF.$touched && !eventFormName.eventNameTF.$valid">
        Bezeichnung ist erforderlich.
      </span>
    </p>
    </ng-form>
  `,
  controller: ['Messages', function(Messages) {
    var ctrl = this;

    ctrl.$onChanges = changes => {
      ctrl.eventName = changes.eventName.currentValue;
    };

    ctrl.update = () => {
      Messages.broadcast('events:form:name:update', {
        eventName: ctrl.eventName
      });
    };
  }]
});

})();