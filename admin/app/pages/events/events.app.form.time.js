(function() {
  var moduleName = 'fa.admin.events';

  angular.module(moduleName).component('eventsAppFormTime', {
    bindings: {
      field: '@',
      isFullDay: '<',
      label: '@',
      timeString: '<'
    },

    template: `
      <ng-form name="timeForm">
        <label
          class="label"
          ng-bind="::$ctrl.label">
        </label>
        <p class="control has-icon has-icon-right"
           style="margin-bottom: 10px;">
          <input
            type="text"
            name="timeTF"
            class="input"
            placeholder="Zeit (Format: HH:MM)"
            required
            ng-disabled="$ctrl.isFullDay"
            ng-class="!timeForm.timeTF.$touched ? ''
                      : (timeForm.timeTF.$valid ? 'is-success' : 'is-danger')"
            ng-required="true"
            ng-model="$ctrl.timeString"
            ng-model-options="{updateOn: 'blur'}"
            ng-change="$ctrl.update()"
            ng-pattern="/^(0[1-9]|1[0-9]|2[0-3]):([0-5][0-9])$/">
          <i
            class="fa fa-warning"
            ng-if="timeForm.timeTF.$touched && !timeForm.timeTF.$valid">
          </i>
          <span
            class="help is-danger is-pulled-right"
            ng-if="timeForm.timeTF.$touched && !timeForm.timeTF.$valid">
            Zeit fehlt oder hat falsches Format.
          </span>
        </p>
      </ng-form>
    `,

    controller: ['$scope', 'Messages', function($scope, Messages) {
      var ctrl = this;
      ctrl.field = 'time';
      ctrl.isFullDay = false;
      ctrl.label = 'Zeit';
      ctrl.timeString = '08:00';
      ctrl.hours = '08';
      ctrl.minutes = '00';
      ctrl.update = update;

      ctrl.$onChanges = changes => {
        //console.log('TimeForm says:');
        //console.log(changes);
        if (changes.timeString) {
          if (changes.timeString.currentValue) {
            let newMoment = moment(changes.timeString.currentValue);
            ctrl.hours = getPaddedValue(newMoment.hours());
            ctrl.minutes = getPaddedValue(newMoment.minutes());
            ctrl.timeString = ctrl.hours + ':' + ctrl.minutes;
          }
        }
      };

      function update() {
        if (ctrl.timeString) {
          ctrl.hours = ctrl.timeString.split(':')[0];
          ctrl.minutes = ctrl.timeString.split(':')[1];
        }
        if ($scope.timeForm.$valid) {
          Messages.broadcast('events:form:time:update', {
            field: ctrl.field,
            hours: ctrl.hours,
            minutes: ctrl.minutes
          });
        }
      };

      function getPaddedValue(number) {
        return (number < 10) ? '0' + number : number;
      }
    }]
  });

})();
