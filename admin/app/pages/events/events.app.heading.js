(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppHeading', {

  bindings: {
    title: '@'
  },
  template: `
    <div class="columns">
      <div class="column">
        <div class="heading">
          <h1 class="title">{{$ctrl.title}}</h1>
        </div>
      </div>
    </div>
  `
});

})();