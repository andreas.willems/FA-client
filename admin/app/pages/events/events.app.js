(function() {
const moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsApp', {

  bindings: {},

  template: `
    <events-app-heading title="Veranstaltungen"></events-app-heading>
    <div class="columns">
      <div class="column">
        <events-app-controls></events-app-controls>
      </div>
    </div>
    <div class="columns">
      <div class="column">
        <events-app-calendar month="$ctrl.month"
                             events="$ctrl.events">
        </events-app-calendar>
        <events-app-modal modal-status="$ctrl.modalStatus"
                          event="$ctrl.event">
        </events-app-modal>
      </div>
    </div>
  `,

  controller: [
    '$window', 'Events', 'Messages',
    function($window, Events, Messages) {

      let ctrl = this;
      ctrl.events = [];
      ctrl.modalStatus = '';
      ctrl.month = {};

      ctrl.getEvents = getEvents;

      ctrl.$onInit = () => {
        ctrl.month = moment();
        //ctrl.day = ctrl.month;
        getEvents();
      };

      function getEvents() {
        Events.get(ctrl.month.month() + 1, ctrl.month.year())
        .then(response => {
          ctrl.events = response.data;
        })
        .catch(handleErrors);
      }

      function handleErrors(error) {
        console.error(error);
      }

      function update(obj, newData) {
//         console.log('##### update state #####');
//         console.log('old state');
//         console.log(obj);
//         console.log('extension');
//         console.log(newData);
        let dst = {};
        angular.merge(dst, obj, newData);
//         console.log('new state');
//         console.log(dst);
        return dst;
      }

      // register event listeners
      Messages.listen('events:lastMonth:request', messageData => {
        ctrl.month = moment(ctrl.month).subtract(1, 'month');
        getEvents();
      });

      Messages.listen('events:nextMonth:request', messageData => {
        ctrl.month = moment(ctrl.month).add(1, 'month');
        getEvents();
      });

      Messages.listen('events:today:request', messageData => {
        ctrl.month = moment();
        getEvents();
      });

      Messages.listen('events:create:request', data => {
        ctrl.modalStatus = 'is-active';
        // reset event object on create request to clear form
        ctrl.event = {};
        ctrl.event.day = ctrl.month.date(data.day.nr);
      });

      Messages.listen('events:edit:request', data => {
        console.log(data);
        ctrl.modalStatus = 'is-active';
        ctrl.event = data.event;
        ctrl.event.day = ctrl.month.date(data.day.nr);
      });

      Messages.listen('events:form:hide:request', data => {
        ctrl.modalStatus = 'is-inactive';
        ctrl.event = {};
      });

      Messages.listen('events:form:name:update', data => {
        //console.log(data);
        ctrl.event = update(ctrl.event, data);
      });

      Messages.listen('events:form:contact:update', data => {
        ctrl.event = update(ctrl.event, data);
      });

      Messages.listen('events:form:isFullDay:update', data => {
        //console.log('##### reacting to fullday update #####')
        let timeData = {};
        if (data.isFullDay) {
          let timeBeginMoment = moment(ctrl.event.day).hours(8).minutes(0);
          let timeEndMoment = moment(ctrl.event.day).hours(16).minutes(0);
          let time = {
            timeBegin: timeBeginMoment,
            timeEnd: timeEndMoment
          }
          timeData = update(data, time);
        } else {
          timeData = update(data, {});
        }
        ctrl.event = update(ctrl.event, timeData);
      });

      Messages.listen('events:form:time:update', data => {
        console.log(data);
      });
    }
  ]

});

})();