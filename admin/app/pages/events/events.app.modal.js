(function() {
var moduleName = 'fa.admin.events';

angular.module(moduleName).component('eventsAppModal', {

  bindings: {
    modalStatus: '<',
    event: '<'
  },

  template: `
    <div class="modal"
         ng-class="$ctrl.modalStatus"
         id="event-dialog">
      <div class="modal-background"></div>
      <div class="modal-card">
        <header class="modal-card-head">
          <p class="modal-card-title">Veranstaltung</p>
          <button class="delete" ng-click="$ctrl.hideForm()"></button>
        </header>
        <div class="modal-card-body">
          <events-app-form event="$ctrl.event"></events-app-form>
        </div>
      </div> <!-- .modal-card -->
      <button class="modal-close" ng-click="$ctrl.hideForm()"></button>
    </div><!-- .modal -->
  `,

  controller: [ 'Messages', function(Messages) {

    let ctrl = this;
    ctrl.hideForm = hideForm;

    function hideForm() {
      Messages.broadcast('events:form:hide:request', {});
    }

  }]

});

})();