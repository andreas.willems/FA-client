/* bof */
(function() {

angular.module('fa.admin.events', [
  'fa.shared.services',
  'fa.shared.constants'
]);

angular
.module('fa.admin.events')
.config(Routes);

angular
.module('fa.admin.events')
.controller('EventsController', EventsController);

angular
.module('fa.admin.events')
.directive('faCreateEvent', FACreateEvent);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

EventsController.$inject = [
  '$window',
  'CalendarService',
  'EventService',
  'RoomService'
];

FACreateEvent.$inject = [
  'ADMIN_APP_PREFIX',
  'EventService',
  'RoomService'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('events', {
    url: '/events',
    templateUrl: ADMIN_APP_PREFIX + 'pages/events/events.tpl.html',
    controller: 'EventsController as vm'
    //template: '<events-app></events-app>'
  });
}

function EventsController($window,
                          CalendarService,
                          EventService,
                          RoomService) {
  var vm = this;
  vm.editable = false;
  vm.saving = false;
  vm.updating = false;
  vm.editEvent = false,
  vm.events = [];
  vm.rooms = [];

  vm.currentEvent = {};
  vm.message = '';
  vm.years = [];

  vm.monthsInYear = [];
  vm.currentMonth = '1';
  vm.currentYear = '2016';

  vm.refreshList = refreshList;
  vm.getAllEvents = getAllEvents;
  vm.toggleEditable = toggleEditable;
  vm.removeEvent = removeEvent;
  vm.editEvent = editEvent;
  vm.cancel = cancel;

  activate();

  function activate() {
    initYears();
    initMonths();
    resetEvent();
    getAllEvents();
    getRooms();
  }

  function initYears() {
    var curr = moment().year();
    vm.years = [curr, curr + 1, curr + 2, curr + 3, curr + 4];
    vm.currentYear = curr;
    vm.currentMonth = moment().month();
  }

  function initMonths(cb) {
    CalendarService.getMonthsInYear(function(months) {
      vm.monthsInYear = months;
      // set selected month to current month using moment.js
      vm.currentMonth = moment().month();
      if (cb) { cb(); }
    });
  }

  function refreshList() {
    EventService.getByMonth(
      parseInt(vm.currentMonth),
      parseInt(vm.currentYear),
      function(result) {
        vm.events = result;
      }
    );
  }

  function toggleEditable() {
    vm.editable = !vm.editable;
    vm.saving = true;
    vm.updating = false;
  }

  function getAllEvents() {
    EventService.getAll(function(result) {
      vm.events = result;
    });
  }

  function getRooms() {
    RoomService.getAll(function(rooms) {
      vm.rooms = rooms;
    });
  }

  function resetEvent() {
    vm.currentEvent = {
      name: '',
      contact: '',
      date: '',
      timeBegin: '',
      timeEnd: '',
      roomId: 1
    };
  }

  function removeEvent(index, event) {
    if($window.confirm('Diese Veranstaltung wirklich löschen?')) {
      EventService.remove(index, event, function(events) {
        vm.getAllEvents();
      });
    }
  }

  function editEvent(index, event) {
    vm.editable = true;
    vm.saving = false;
    vm.updating = true;
    var timeBegin = getTimeString(moment(parseInt(event.timeBegin)));
    var timeEnd = getTimeString(moment(parseInt(event.timeEnd)));
    var dateStart = moment(parseInt(event.date));
    var dateString = getDateString(dateStart);
    vm.currentEvent = {
      id: event.id,
      name: event.name,
      contact: event.contact,
      day: parseInt(event.day),
      month: parseInt(event.month),
      year: parseInt(event.year),
      timeBegin: timeBegin,
      timeEnd: timeEnd,
      room: findRoomById(event.roomId),
      date: {
        start: dateString
      }
    };
  }

  function getTimeString(time) {
    var timeString = (time.hour() < 10 ? '0' : '');
    timeString += time.hour() + ':';
    timeString += (time.minute() < 10 ? '0' : '');
    timeString += time.minute();
    return timeString;
  }

  function getDateString(date) {
    var month = (date.month() < 10 ? '0' : '');
    month += (date.month() + 1);
    var dateString =
      date.date() + '.'+
      month + '.' +
      date.year();
    return dateString;
  }

  function cancel() {
    vm.updating = false;
    vm.saving = false;
    vm.editable = false;
    resetEvent();
  }

  function findRoomById(roomId) {
    var result = null;
    for (var i = 0; i< vm.rooms.length; i++) {
      if (vm.rooms[i].id == roomId) {
        result = vm.rooms[i];
        break;
      }
    }
    return result;
  }
}

/* DIRECTIVE fa-create-event */
function FACreateEvent(ADMIN_APP_PREFIX, EventService, RoomService) {
  return {
    templateUrl: ADMIN_APP_PREFIX + 'pages/events/create-event.tpl.html',
    scope: {
      myEvent: '=',
      rooms: '=',
      creating: '=',
      updating: '=',
      refresh: '='
    },
    restrict: 'E',
    link: link
  };

  function link(scope, element, attrs) {
    scope.saveEvent = saveEvent;
    scope.updateEvent = updateEvent;
    scope.cancel = cancel;
    scope.multiDay = false;

    //scope.myEvent.date = {};

    function saveEvent() {
      if (scope.multiDay) {
        saveMultiDayEvent();
      } else {
        saveSingleDayEvent();
      }

    }

    function processFormContent() {
      scope.myEvent.date = createEventDate(scope.myEvent.date.start);
      scope.myEvent.timeBegin = createEventBegin(scope.myEvent.timeBegin);
      scope.myEvent.timeEnd = createEventEnd(scope.myEvent.timeEnd);
      scope.myEvent.roomId = scope.myEvent.room.id;
    }

    function saveSingleDayEvent() {
      console.log('saving single day event...');
      processFormContent();

      EventService.save(scope.myEvent, function() {
        resetForm();
        scope.refresh();
      });
    }

    function saveMultiDayEvent() {
      console.log('saving multi day event...');
      console.log(scope.myEvent);
    }

    function updateEvent() {
      console.log('updating event...');
      processFormContent();
      EventService.update(scope.myEvent, function() {
        resetForm();
        scope.refresh();
      });
    }

    function cancel() {
      resetForm();
    }

    /**
     * Creates a date object from the given date string.
     * @param dateString - a date string in format dd.mm.yyyy
     */
    function createEventDate(dateString) {
      var parts = dateString.split('.'); // [day, month, year]
      scope.myEvent.day = parts[0];
      scope.myEvent.month = parts[1];
      scope.myEvent.year = parts[2];

      return new Date(
        scope.myEvent.year,
        scope.myEvent.month - 1,
        scope.myEvent.day,
        0,0,0,0);
    }

    /**
     * Creates a date object from the given time string.
     * @param timeString - a time string in format HH:MM
     */
    function createEventBegin(timeString) {
      var parts = timeString.split(':'); // [hours, minutes]

      if (!scope.myEvent.begin) {
        scope.myEvent.begin = {};
      }

      scope.myEvent.begin.hour = parts[0];
      scope.myEvent.begin.minute = parts[1];

      return new Date(
        scope.myEvent.year,
        scope.myEvent.month -1,
        scope.myEvent.day,
        scope.myEvent.begin.hour,
        scope.myEvent.begin.minute,
        0,0);
    }

    /**
     * Creates a date object from the given time string.
     * @param timeString - a time string in format HH:MM
     */
    function createEventEnd(timeString) {
      var parts = timeString.split(':'); // [hours, minutes]

      if (!scope.myEvent.end) {
        scope.myEvent.end = {};
      }

      scope.myEvent.end.hour = parts[0];
      scope.myEvent.end.minute = parts[1];

      return new Date(
        scope.myEvent.year,
        scope.myEvent.month - 1,
        scope.myEvent.day,
        scope.myEvent.end.hour,
        scope.myEvent.end.minute,
        0,0);
    }

    function resetForm() {
      scope.myEvent = {
        id: '',
        name: '',
        contact: '',
        begin: {},
        end:{},
        day: '',
        month: '',
        year: '',
        room: {id:1}
      };
      scope.form.$setUntouched();
      scope.form.$setPristine();
      scope.form.$rollbackViewValue();
    }
  }
}

})();
/* eof */
