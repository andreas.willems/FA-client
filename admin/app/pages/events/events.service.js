(function() {
const moduleName = 'fa.admin.events';

angular.module(moduleName).factory('Events', Events);

Events.$inject = ['$http', 'API_PREFIX'];

function Events($http, API_PREFIX) {
  return {
    get : get,
    getAll : getAll
  };

  function get(month, year) {
    let req = {
      method: 'GET',
      url: API_PREFIX + '/events?month=' + month + '&year=' + year
    };
    return $http(req);
  }

  function getAll() {
    let req = {
      method: 'GET',
      url: API_PREFIX + '/events'
    };
    return $http(req);
  }
}

})();