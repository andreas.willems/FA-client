/* bof */
(function() {

angular.module('fa.admin.files', [
  'fa.shared.constants'
]);

angular
.module('fa.admin.files')
.config(Routes);;

angular
.module('fa.admin.files')
.controller('FilesController', FilesController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

FilesController.$inject = [
  '$window',
  'API_PREFIX',
  'FileUploader',
  'Files'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('files', {
    url: '/files',
    templateUrl: ADMIN_APP_PREFIX + 'pages/files/files.tpl.html',
    controller: 'FilesController as vm'
  });
}

function FilesController($window, API_PREFIX, FileUploader, Files) {
  var vm = this;
  vm.fileApi = API_PREFIX + '/files/';
  vm.files = [];
  vm.uploader = new FileUploader({
    url: API_PREFIX + '/files'
  });
  vm.removeFile = removeFile;

  activate();

  function activate() {
    getFiles();
  }

  function getFiles() {
    Files.getAll(function(files) {
      vm.files = files;
    });
  }

  function removeFile(index, file) {
    if($window.confirm('Diese Datei wirklich löschen?')) {
      Files.remove(file.id, function() {
        getFiles();
      });
    }
  }

  vm.uploader.onSuccessItem = function() {
    vm.uploader.clearQueue();
    getFiles();
  };
  vm.uploader.onErrorItem = function() {
    //console.info('onErrorItem');
    //console.log(arguments);
  };
  vm.uploader.onCancelItem = function() {
    //console.info('onCancelItem');
  };
  vm.uploader.onCompleteItem = function() {
    //console.info('onCompleteItem');
    // set success message
  };
  vm.uploader.onAfterAddingFile = function(item) {
    item.uploadedAt = Date.now();
    //console.log(item);
  };
}

})();
/* eof */
