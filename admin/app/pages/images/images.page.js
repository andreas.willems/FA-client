/* bof */
(function() {
var moduleName = 'fa.admin.images';
angular.module(moduleName, [
  'fa.shared.constants'
]);

angular
.module(moduleName)
.config(Routes);;

angular
.module(moduleName)
.controller('ImagesController', ImagesController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

ImagesController.$inject = [
  '$window',
  'API_PREFIX',
  'FileUploader',
  'Images'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('images', {
    url: '/images',
    templateUrl: ADMIN_APP_PREFIX + 'pages/images/images.tpl.html',
    controller: 'ImagesController as vm'
  });
}

function ImagesController($window, API_PREFIX, FileUploader, Images) {
  var vm = this;
  vm.imageApi = API_PREFIX + '/images/';
  vm.images = [];
  vm.uploader = new FileUploader({
    url: API_PREFIX + '/images'
  });
  vm.remove = remove;

  activate();

  function activate() {
    getImages();
  }

  function getImages() {
    Images.getAll(function(images) {
      vm.images = images;
    });
  }

  function remove(index, image) {
    if($window.confirm('Diese Datei wirklich löschen?')) {
      Images.remove(image, function() {
        getImages();
      });
    }
  }

  vm.uploader.onSuccessItem = function() {
    vm.uploader.clearQueue();
    getImages();
  };
  vm.uploader.onErrorItem = function() {
    //console.info('onErrorItem');
    //console.log(arguments);
  };
  vm.uploader.onCancelItem = function() {
    //console.info('onCancelItem');
  };
  vm.uploader.onCompleteItem = function() {
    //console.info('onCompleteItem');
    // set success message
  };
  vm.uploader.onAfterAddingFile = function(item) {
    item.uploadedAt = Date.now();
    //console.log(item);
  };
}

})();
/* eof */
