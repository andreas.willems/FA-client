/* bof */
(function() {
var moduleName = 'fa.admin.links';
angular.module(moduleName, [
  'fa.shared.constants'
]);

angular
.module(moduleName)
.config(Routes);

angular
.module(moduleName)
.controller('LinksController', LinksController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

LinksController.$inject = [
  '$scope',
  '$window',
  'API_PREFIX',
  'Links'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('links', {
    url: '/links',
    templateUrl: ADMIN_APP_PREFIX + 'pages/links/links.tpl.html',
    controller: 'LinksController as vm'
  });
}

function LinksController($scope,
                         $window,
                         API_PREFIX,
                         Links
                        ) {
  var vm = this;
  vm.isUpdating = false;
  vm.currentLink = {};
  vm.links = [];
  vm.create = create;
  vm.edit = edit;
  vm.update = update;
  vm.remove = remove;
  vm.reset = reset;

  activate();

  function activate() {
    getAll();
  }

  function getAll() {
    Links.getAll(function(links) {
      vm.links = links;
    });
  }

  function create() {
    Links.create(vm.currentLink, function(link) {
      getAll();
      reset();
    });
  }

  function edit(index, link) {
    vm.currentLink = link;
    vm.isUpdating = true;
  }

  function remove(index, link) {
    if($window.confirm('Diesen Link wirklich löschen?')) {
      Links.remove(link, function() {
        getAll();
      });
    }
  }

  function update() {
    Links.update(vm.currentLink, function(result) {
      // console.log(result);
      reset();
    });
  }

  function reset() {
    vm.isUpdating = false;
    vm.currentLink = {
      name: '',
      url: ''
    };
    $scope.form.$setUntouched();
    $scope.form.$setPristine();
    $scope.form.$rollbackViewValue();
  }
}

})();
/* eof */
