/* bof */
(function() {
'use strict';

angular
.module('fa.admin.overview', [
  'fa.shared.constants'
]);

/* Routes */
angular
.module('fa.admin.overview')
.config(Routes);

Routes.$inject = [
  '$stateProvider',
  'ROUTE_PREFIX',
  'ADMIN_APP_PREFIX'
];

function Routes($stateProvider, ROUTE_PREFIX, ADMIN_APP_PREFIX) {
  $stateProvider
    .state('overview', {
      url: '/overview',
      templateUrl: ADMIN_APP_PREFIX + 'pages/overview/overview.tpl.html',
      controller: 'OverviewController as vm'
    })
  ;
}

/* Controller */
angular
.module('fa.admin.overview')
.controller('OverviewController', OverviewController);

OverviewController.$inject = [
  'OverviewService'
];

function OverviewController(OverviewService) {

}

angular
.module('fa.admin.overview')
.factory('OverviewService', OverviewService);

OverviewService.$inject = [

];

function OverviewService() {
  return {};
}

})();
/* eof */
