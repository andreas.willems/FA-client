/* bof */
(function() {
'use strict';

var moduleName = 'fa.admin.protocols';
angular.module(moduleName, [
  'ngSanitize',
  'fa.shared.constants'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('ProtocolsController', ProtocolsController);

ProtocolsController.$inject = [
  '$scope',
  '$window',
  'API_PREFIX',
  'Articles',
  'ArticleLinks',
  'Departments',
  'Links',
  'Messages'
];

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider

  .state('protocols', {
    url: '/protocols',
    templateUrl: ADMIN_APP_PREFIX +
                 'pages/protocols/protocols.tpl.html',
    controller: 'ProtocolsController as vm'
  })
  ;
}

/******************************************************************************
 * ProtocolsController
 ******************************************************************************/
function ProtocolsController($scope,
                             $window,
                             API_PREFIX,
                             Articles,
                             ArticleLinks,
                             Departments,
                             Links,
                             Messages) {
  var vm = this;
  vm.today = '';
  vm.titlePlaceholder = '';
  vm.shortTextPlaceholder = '';
  vm.departments = [];
  vm.protocols = [];
  vm.protocol = {
    title: '',
    type: 'protocol',
    shortText: '',
    longText: '',
    createdAt: '',
    updatedAt: '',
    department: {},
    links: []
  };
  vm.updating = false;
  vm.filesVisible = false;
  vm.followLink = followLink;
  vm.newLink = {};
  vm.previewVisible = false;
  vm.removeLink = removeLink;
  vm.togglePreview = togglePreview;

  vm.create = create;
  vm.cancel = cancel;
  vm.update = update;
  vm.remove = remove;
  vm.edit = edit;

  vm.updateForm = updateForm;
  vm.insertText = insertText;

  activate();

  function activate() {
    Departments.getAll(function(departments) {
      vm.departments = departments;
      vm.protocol.department = vm.departments[0];
      vm.today = moment().format('L');
      vm.titlePlaceholder = 
        'Dienstbesprechung SG ' +
        vm.protocol.department.numeral;
      vm.shortTextPlaceholder =
        'Protokoll der Dienstbesprechung vom ' +
        vm.today + '.';
      vm.protocol.createdAt = Date.now();
      vm.protocol.updatedAt = Date.now();
    });
    Articles.getProtocols(function(protocols) {
      vm.protocols = protocols;
    });

    Messages.listen('article:edit', function(data) {
      edit(data.article);
    });

    Messages.listen('article:remove', function onSuccess(data) {
      remove(data.index, data.article);
    });
  }

  function followLink(link) {
    $window.open(link.url, '_blank');
  }

  function updateForm() {
    vm.titlePlaceholder = 
      'Dienstbesprechung SG ' +
      vm.protocol.department.numeral;
  }

  function removeLink(index, link) {
    Links.remove(link, function() {
      vm.protocol.links.splice(index, 1);
    });
  }

  function create() {
    Articles.create(vm.protocol, function(protocol) {
      ArticleLinks.appendLinks(
        protocol.id,
        vm.protocol.links,
        function(links) {
          protocol.links = links;
          //vm.protocols.unshift(protocol);
          vm.protocols.push(protocol);
          reset();
        }
      );
    });
  }

  function remove(index, article) {
    if($window.confirm('Dieses Protokoll wirklich löschen?')) {
      Articles.remove(article.id, function() {
        vm.protocols.splice(index - 1, 1);
        ArticleLinks.removeArticle(article.id, function() {});
      });
    }
  }

  function edit(protocol) {
    ArticleLinks.getLinksForArticle(protocol.id, function(links) {
      protocol.links = links;
      vm.updating = true;
      vm.protocol = protocol;
      vm.protocol.department = findDepartment(protocol.departmentId);
    });
  }

  function update() {
    // update article data
    Articles.update(vm.protocol, function(touchedRows) {
      // remove old associated files
      ArticleLinks.removeArticle(vm.protocol.id, function(res) {
        // append new associated files
        ArticleLinks.appendLinks(
          vm.protocol.id,
          vm.protocol.links,
          function(result) {
            cancel();
          }
        );
      });
    });
  }

  function cancel() {
    vm.updating = false;
    reset();
  }

  function reset() {
    vm.protocol = {
      title: '',
      type: 'protocol',
      shortText: '',
      longText: '',
      createdAt: '',
      updatedAt: '',
      department: vm.departments[0],
      files: []
    };
    vm.filesVisible = false;
    $scope.articleForm.$setUntouched();
    $scope.articleForm.$setPristine();
    $scope.articleForm.$rollbackViewValue();
  }

  function insertText() {
    vm.protocol.title = vm.titlePlaceholder;
    vm.protocol.shortText = vm.shortTextPlaceholder;
    vm.protocol.createdAt = Date.now();
    vm.protocol.updatedAt = Date.now();
  }

  function findDepartment(id) {
    var result = {};
    vm.departments.forEach(function(entry) {
      if (entry.id === id) {
        result = entry;
      }
    });
    return result;
  }

  function togglePreview() {
    vm.previewVisible = !vm.previewVisible;
  }

}

})();
/* eof */
