/* bof */
(function() {
var moduleName = 'fa.admin.rooms';
angular.module(moduleName, [
  'fa.shared.constants'
]);

angular
.module(moduleName)
.config(Routes);

angular
.module(moduleName)
.controller('RoomsController', RoomsController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

RoomsController.$inject = [
  '$scope',
  '$window',
  'API_PREFIX',
  'FileUploader',
  'RoomService'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('rooms', {
    url: '/rooms',
    templateUrl: ADMIN_APP_PREFIX + 'pages/rooms/rooms.tpl.html',
    controller: 'RoomsController as vm'
  });
}

function RoomsController($scope,
                         $window,
                         API_PREFIX,
                         FileUploader,
                         RoomService) {
  var vm = this;
  vm.uploader = new FileUploader({
    url: API_PREFIX + '/files'
  });
  vm.editable = false;
  vm.saving = false;
  vm.updating = false;
  vm.imagesVisible = false;
  vm.imageApi = API_PREFIX + '/images/';
  vm.selectedImage = {};

  vm.rooms = [];
  vm.newroom = {};
  vm.message = '';
  vm.toggleEditable = toggleEditable;
  vm.toggleImages = toggleImages;
  vm.saveRoom = saveRoom;
  vm.removeRoom = removeRoom;
  vm.editRoom = editRoom;
  vm.updateRoom = updateRoom;
  vm.cancel = cancel;

  vm.removeImage = removeImage;

  activate();

  function activate() {
    resetRoom();
    getAllRooms();
  }

  function toggleEditable() {
    vm.editable = !vm.editable;
    vm.saving = true;
    vm.updating = false;
  }

  function toggleImages() {
    vm.imagesVisible = !vm.imagesVisible;
  }

  function getAllRooms() {
    RoomService.getAll(function(result) {
      vm.rooms = result;
    });
  }

  function saveRoom() {
    if (validateForm()) {
      vm.newroom.image = vm.selectedImage.id;
      RoomService.save(vm.newroom, function() {
        resetRoom();
        vm.message = '';
      });
    } else {
      vm.message = 'Bitte Eingaben überprüfen';
    }
  }

  function resetRoom() {
    vm.newroom = {
      name: '',
      nr: '',
      building: '',
      equipment: '',
      image: ''
    };
  }

  function removeRoom(index, room) {
    if($window.confirm('Diesen Raum wirklich löschen?')) {
      RoomService.remove(index, room, function(rooms) {
        //$scope.$digest();
      });
    }
  }

  function editRoom(index, room) {
    vm.newroom = vm.rooms[index];
    vm.editable = true;
    vm.saving = false;
    vm.updating = true;

  }

  function updateRoom() {
    vm.newroom.image = vm.selectedImage.id;
    RoomService.update(vm.newroom, function(res) {
      vm.editable = false;
      vm.updating = false;
      resetRoom();
    });
  }

  function cancel() {
    vm.updating = false;
    vm.saving = false;
    vm.editable = false;
    resetRoom();
  }

  function validateForm() {
    // TODO: do more validation, e.g. with $sce service
    if (vm.newroom.name !== '' &&
        vm.newroom.nr !== '' &&
        vm.newroom.building !== '') {
      return true;
    }
    return false;
  }

  function removeImage() {
    vm.newroom.image = '';
  }

  vm.uploader.onSuccessItem = function(item, response, status) {
    vm.uploader.clearQueue();
    console.log(response);
    vm.newroom.image = response.id;
  };
  vm.uploader.onErrorItem = function() {
    //console.info('onErrorItem');
    //console.log(arguments);
  };
  vm.uploader.onCancelItem = function() {
    //console.info('onCancelItem');
  };
  vm.uploader.onCompleteItem = function() {
    //console.info('onCompleteItem');
    // set success message
  };
  vm.uploader.onAfterAddingFile = function(item) {
    item.uploadedAt = Date.now();
    //console.log(item);
  };
}

})(); 
/* eof */
