/* bof */
(function() {
var moduleName = 'fa.admin.staffCouncil';
angular.module(moduleName, [
  'fa.shared.services',
  'fa.shared.constants',
  'fa.admin.images'
]);

angular.module(moduleName).config(Routes);

angular
.module(moduleName)
.controller('StaffCouncilController', StaffCouncilController);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

StaffCouncilController.$inject = [
  '$window',
  'Images',
  'API_PREFIX'
];


function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('staffCouncil', {
    url: '/staff-council',
    templateUrl: ADMIN_APP_PREFIX + 'pages/staff-council/staff-council.tpl.html',
    controller: 'StaffCouncilController as vm'
  })
  ;
}

function StaffCouncilController($window,
                                Images,
                                API_PREFIX) {
  var vm = this;
  
}

})();
/* eof */
