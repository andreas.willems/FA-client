(function() {
var moduleName = 'fa.admin.users';

angular.module(moduleName).component('usersAppForm', {

  bindings: {
    status: '<',
    user: '<'
  },

  template: `
    <h2 class="subtitle">Benutzer anlegen / ändern</h2>
    <form name="form" novalidate>
      <div class="columns">
        <div class="column is-one-half">

          <label class="label" for="userNameTf">Benutzername</label>
          <p class="control has-icon has-icon-right">
            <input type="text"
               class="input"
               ng-class="!form.userNameTf.$touched ? '' : (form.userNameTf.$valid ? 'is-success' : 'is-danger')"
               name="userNameTf"
               ng-model="$ctrl.user.name"
               placeholder="Benutzername"
               ng-required="true">

            <i class="fa fa-warning"
               ng-if="form.userNameTf.$touched && !form.userNameTf.$valid">
            </i>
            <span class="help is-danger is-pulled-right"
                  ng-if="form.userNameTf.$touched && !form.userNameTf.$valid">
              Name ist erforderlich.
            </span>
          </p>

          <label class="label" for="userPassTf">Passwort</label>
          <p class="control has-icon has-icon-right">
            <input type="text"
               class="input"
               name="userPassTf"
               ng-class="!form.userPassTf.$touched ? '' : (form.userPassTf.$valid ? 'is-success' : 'is-danger')"
               ng-model="$ctrl.user.password"
               placeholder="Passwort"
               ng-required="true">
            <i class="fa fa-warning"
               ng-if="form.userPassTf.$touched && !form.userPassTf.$valid">
            </i>
            <span class="help is-danger is-pulled-right"
                  ng-if="form.userPassTf.$touched && !form.userPassTf.$valid">
              Passwort ist erforderlich.
            </span>
          </p>

          <label class="label" for="userKeyTf">API Key</label>
          <p class="control has-icon has-icon-right">
            <input type="text"
                 class="input"
                 name="userKeyTf"
                 ng-class="!form.userKeyTf.$touched ? '' : (form.userKeyTf.$valid ? 'is-success' : 'is-danger')"
                 ng-model="$ctrl.user.apiKey"
                 placeholder="API Key"
                 ng-required="true">
            <i class="fa fa-warning"
               ng-if="form.userKeyTf.$touched && !form.userKeyTf.$valid">
            </i>
            <span class="help is-danger is-pulled-right"
                  ng-if="form.userKeyTf.$touched && !form.userKeyTf.$valid">
              Passwort ist erforderlich.
            </span>
          </p>
        </div>
        <!-- column right -->
        <div class="column"></div>
      </div>


      <div class="columns">
        <!-- column left -->
        <div class="column">
          <button type="button"
                  ng-if="$ctrl.status == 'creating'"
                  ng-disabled="!form.$valid"
                  class="button is-success"
                  ng-click="$ctrl.create()">Speichern</button>
          <button type="button"
                  ng-if="$ctrl.status == 'updating'"
                  ng-disabled="!form.$valid"
                  class="button is-warning"
                  ng-click="$ctrl.update()">Ändern</button>
          <button type="reset"
                  ng-click="$ctrl.reset()"
                  class="button is-default">Abbrechen</button>
        </div>
        <!-- column right -->
        <div class="column"></div>
      </div>

    </form>
  `,

  controller: ['$scope', 'Messages', function($scope, Messages) {
    var ctrl = this;
    //vm.user = {};
    ctrl.create = create;
    ctrl.update = update;
    ctrl.reset = reset;

    function create() {
      Messages.broadcast('users:create:request', ctrl.user);
      reset();
    }

    function update() {
      Messages.broadcast('users:update:request', ctrl.user);
      reset();
    }

    function reset() {
      ctrl.user = {
        name: '',
        password: '',
        apiKey: ''
      };
      $scope.form.$setUntouched();
      $scope.form.$setPristine();
      $scope.form.$rollbackViewValue();
      Messages.broadcast('users:form:reset', {});
    }
  }]

});

})(); 
