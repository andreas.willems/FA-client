(function() {
var moduleName = 'fa.admin.users';

angular.module(moduleName).component('usersAppHeading', {

  bindings: {
    title: '@'
  },
  template: `
    <div class="columns">
      <div class="column">
        <div class="heading">
          <h1 class="title">{{$ctrl.title}}</h1>
        </div>
      </div>
    </div>
  `
});

})(); 
