(function() {
var moduleName = 'fa.admin.users';

angular.module(moduleName).component('usersApp', {

  bindings: {},

//   template:[
//     '<users-app-heading title="Benutzer"></users-app-heading>'
//   , '<div class="columns">'
//   , '<div class="column is-one-half">'
//   , '<users-app-table users="$ctrl.users"></users-app-table>'
//   , '<users-app-form status="$ctrl.formStatus" user="$ctrl.currentUser"></users-app-form>'
//   , '</div>'
//   , '<div class="column">'
//   , '</div>'
//   , '</div>'
//   ].join(''),

  template: `
    <users-app-heading title="Benutzer"></users-app-heading>
    <div class="columns">
      <div class="column is-one-half">
        <users-app-table users="$ctrl.users"></users-app-table>
        <users-app-form status="$ctrl.formStatus"
                        user="$ctrl.currentUser"
                        message="$ctrl.statusMessage">
        </users-app-form>
      </div>
      <div class="column"></div>
    </div>
  `,

  controller: [
    '$window', 'Messages', 'Users', function($window, Messages, Users) {
    var vm = this;
    vm.formStatus = 'creating';
    vm.statusMessage = {text: ''};
    vm.users = [];

    Messages.listen('users:fetch:request', messageData => {
      getAllUsers();
    });

    Messages.listen('users:create:request', user => {
      Users
      .create(user)
      .then(response => {
        getAllUsers();
      })
      .catch(handleErrorResponse);
    });

    Messages.listen('users:edit:request', user => {
      vm.formStatus = 'updating';
      vm.currentUser = user;
    });

    Messages.listen('users:update:request', user => {
      Users.update(user)
      .then(response => {
        if (response.data.res === 1) {
          vm.statusMessage = {
            type: 'is-success',
            text: 'Ändern erfolgreich.'
          }
        }
        getAllUsers();
      })
      .catch(handleErrorResponse);
    });

    Messages.listen('users:delete:request', user => {
      if($window.confirm('Diesen Benutzer wirklich löschen?')) {
        Users.remove(user)
        .then(response => {
          getAllUsers();
        })
        .catch(handleErrorResponse);
      }
    });

    Messages.listen('users:form:reset', () => {
      vm.formStatus = 'creating';
    });

    function getAllUsers() {
      Users.getAll()
      .then(users => {
        vm.users = users.data;
      })
      .catch(handleErrorResponse);
    }

    function handleErrorResponse(error) {
      console.error(error);
    } 
  }]

});

})();