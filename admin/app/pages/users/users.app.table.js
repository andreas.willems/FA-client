(function() { 
var moduleName = 'fa.admin.users';

angular.module(moduleName).component('usersAppTable', {

  bindings: {
    users: '<'
  },
  template: `
    <h2 class="subtitle">Vorhandene Benutzer</h2>
    <table class="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Api Key</th>
          <th>Aktionen</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="user in $ctrl.users">
          <td>{{user.id}}</td>
          <td>{{user.name}}</td>
          <td>{{user.apiKey}}</td>
          <td>
            <div class="btn-group" role="group" aria-label="aktionen">
              <button type="button"
                      class="button is-small"
                      ng-click="$ctrl.edit(user)">
                <i class="fa fa-pencil" aria-hidden="true"></i>
              </button>
              <button type="button"
                      class="button is-small is-danger"
                      ng-click="$ctrl.remove(user);">
                <i class="fa fa-times" aria-hidden="true"></i>
              </button>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  `,
  controller: ['Messages', function(Messages) {
    this.edit = edit;
    this.remove = remove;

    function edit(user) {
      Messages.broadcast('users:edit:request', user);
    }

    function remove(user) {
      Messages.broadcast('users:delete:request', user);
    }

    Messages.broadcast('users:fetch:request', {});
  }]

});

})(); 
