/* bof */
(function() {
var moduleName = 'fa.admin.users';
angular.module(moduleName, [
  'fa.shared.constants'
]);

angular.module(moduleName).config(Routes);

Routes.$inject = [
  '$stateProvider',
  'ADMIN_APP_PREFIX'
];

function Routes($stateProvider, ADMIN_APP_PREFIX) {
  $stateProvider
  .state('users', {
    url: '/users',
    template: '<users-app></users-app>'
  });
}

})();
/* eof */
