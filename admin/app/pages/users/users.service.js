(function() {
var moduleName = 'fa.admin.users';

angular
.module(moduleName)
.factory('Users', UsersService);

UsersService.$inject = [
  '$http',
  'API_PREFIX',
  'Messages'
];


function UsersService($http, API_PREFIX, Messages) {
  var vm = this;

  return {
    getAll: getAll,
    getById: getById,
    create: create,
    remove: remove,
    update: update
  };

  function getAll(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/users'
    };
    return $http(req);
  }

  function getById(id, cb) {
    cb(vm.users[id]);
  }

  function create(user, callback) {
    var paramString = createParamString(user);
    var req = {
      method: 'POST',
      url: API_PREFIX + '/users',
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return $http(req);
  }

  function remove(user) {
    var id = user.id;
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/users/' + id
    };
    return $http(req);
  }

  function update(user) {
    var id = user.id;
    var paramString = createParamString(user);
    var req = {
      method: 'PUT',
      url: API_PREFIX + '/users/' + id,
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return $http(req);
  }

  function createParamString(user) {
    var params = 'name=' + user.name
      + '&password=' + user.password
      + '&apiKey=' + user.apiKey;
    return params;
  }
}

})();
