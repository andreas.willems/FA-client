(function() {
'use strict';

angular.module('fa-reservations', [
  'ui.router',
  'angularFileUpload',
  'angular-loading-bar',
  'fa.shared.services',
  'fa.shared.constants',
  'fa.components.filechooser',
  'fa.components.imagechooser',
  'fa.admin.rooms',
  'fa.admin.events'
]);
angular.module('fa-reservations').config(Routes);
angular.module('fa-reservations').run(RunBlock);

Routes.$inject = [
  '$urlRouterProvider'
];

RunBlock.$inject = ['$window'];

function Routes($urlRouterProvider) {
  $urlRouterProvider.otherwise('/events');
}

function RunBlock($window) {
  moment.locale('de');
  var windowElement = angular.element($window);
  windowElement.on('beforeunload', function (event) {
    // the following line of code will prevent reload or navigating away.
    // console.log(event);
    //event.preventDefault();
  });
}

})();