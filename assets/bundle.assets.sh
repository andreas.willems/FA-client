#!/bin/bash
DIRECTORY="/usr/vdv2/fa/www/DEV/client/assets"
OUTPUTMIN="$DIRECTORY/assets.bundle.js"

# clear bundle files
echo '' > "$OUTPUTMIN"

# append libraries - js
cat "$DIRECTORY"/libs/polyfills/promise.min.js\
    "$DIRECTORY"/libs/polyfills/fetch.js\
    "$DIRECTORY"/libs/zepto/zepto.min.js\
    "$DIRECTORY"/libs/angularjs/angular.js\
    "$DIRECTORY"/libs/angularjs/angular-ui-router.min.js\
    "$DIRECTORY"/libs/angularjs/angular-locale_de-de.js\
    "$DIRECTORY"/libs/angularjs/angular-sanitize.min.js\
    "$DIRECTORY"/libs/angular-loading-bar/build/loading-bar.min.js\
    "$DIRECTORY"/libs/angular-file-upload/dist/angular-file-upload.min.js\
    "$DIRECTORY"/libs/momentjs/moment-with-locales.min.js\
    "$DIRECTORY"/libs/tinymce/js/tinymce/tinymce.js\
    "$DIRECTORY"/libs/ui-tinymce/src/tinymce.js\
>> "$OUTPUTMIN"
chmod 644 "$OUTPUTMIN"
lines=`wc -l "$OUTPUTMIN" | cut -f1 -d' '`
echo "Wrote $lines lines to $OUTPUTMIN"