function MyDateLib() {}

/**
 * Gets the number of days in the given month of the given year.
 */
MyDateLib.prototype.daysInMonth = function daysInMonth(month, year) {
  return new Date(year, month, 0).getDate();
};

/**
 * Gets the number of the current month (1-based).
 */
MyDateLib.prototype.getCurrentMonth = function() {
  return new Date().getMonth() + 1;
};

/**
 * Gets the number of the last month (1-based).
 */
MyDateLib.prototype.getLastMonth = function() {
  var curr = this.getCurrentMonth();
  if (curr === 1) {return 12};
  return curr - 1;
};

/**
 * Gets the number of the next month (1-based).
 */
MyDateLib.prototype.getNextMonth = function() {
  var curr = this.getCurrentMonth();
  if (curr === 12) {return 1};
  return curr + 1;
};

MyDateLib.prototype.getNameOfMonth = function(month) {
  var result = '';
  switch(month) {
    case 1: result = 'Januar'; break;
    case 2: result = 'Februar'; break;
    case 3: result = 'März'; break;
    case 4: result = 'April'; break;
    case 5: result = 'Mai'; break;
    case 6: result = 'Juni'; break;
    case 7: result = 'Juli'; break;
    case 8: result = 'August'; break;
    case 9: result = 'September'; break;
    case 10: result = 'Oktober'; break;
    case 11: result = 'November'; break;
    case 12: result = 'Dezember'; break;
  }
  return result;
}