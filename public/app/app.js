(function() {
'use strict';
angular.module('fa-ler-public', [
  'ngSanitize',
  'ui.router',
  'angular-loading-bar',
  'fa.shared.components',
  'fa.shared.constants',
  'fa.shared.services',
  'fa.public.main.header',
  'fa.public.main.banner',
  'fa.public.main.footer',
  'fa.public.weather'
]);
angular.module('fa-ler-public').config(Routes);
angular.module('fa-ler-public').run(RunBlock);

Routes.$inject = [
  '$urlRouterProvider'
];

RunBlock.$inject = [];

function Routes($urlRouterProvider) {
  // For any unmatched url, redirect to /home
  $urlRouterProvider.otherwise('/home');
}

function RunBlock() {
  moment.locale('de');
}

})();