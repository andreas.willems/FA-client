#!/bin/bash

DIRECTORY="/usr/vdv2/fa/www/DEV/client/public/app/"
OUTPUTMIN="$DIRECTORY"public.bundle.js
APPJS="$DIRECTORY"app.js

function cleanBundleFile {
  if [ -f $OUTPUTMIN ]; then
    rm "$OUTPUTMIN"
  fi
}

function collectFileNames {
  files=$(find $DIRECTORY -name '*.js')
}

function concatExtraFiles {
  cat "$APPJS" >> $OUTPUTMIN
}

function concatFiles {
  for f in $files
  do
    if [ $f == $APPJS ]; then
      continue
    fi
    cat "$f" >> $OUTPUTMIN
  done
}

function printLineCount {
  lines=`wc -l "$OUTPUTMIN" | cut -f1 -d' '`
  echo "Wrote $lines lines to $OUTPUTMIN"
}

function changeAccessRights {
  chmod 644 "$OUTPUTMIN"
}

cleanBundleFile
collectFileNames
concatExtraFiles
concatFiles
changeAccessRights
printLineCount
