(function() {

angular
.module('fa-ler-public')
.directive('faCalendar', FACalendar);

angular
.module('fa-ler-public')
.filter('cut', CutFilter);

FACalendar.$inject = [
  '$document',
  'API_PREFIX',
  'CalendarService',
  'RoomService'
];

function FACalendar($document, API_PREFIX, CalendarService, RoomService) {
  return {
    templateUrl: 'public/app/components/calendar/calendar.tpl.html',
    scope: {},
    restrict: 'E',
    link: link
  };

  function link(scope, element, attrs) {
    scope.rooms = [];
    scope.years = [];
    scope.monthsInYear = [];
    scope.daysInMonth = [];
    scope.weekdays = [
      'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag',
      'Freitag', 'Samstag', 'Sonntag'
    ];
    scope.year = '2016';
    scope.month = '1';
    scope.selectedEvent = {};
    scope.selectedRoom = {};

    scope.imageApi = API_PREFIX + '/images/';
    scope.largeImageUrl = 'http://placehold.it/400x225';
    scope.placeholderImage = 'http://placehold.it/400x225';

    scope.selectRoom = selectRoom;
    scope.updateMonth = updateMonth;
    scope.getDaysInMonth = getDaysInMonth;
    scope.showEvent = showEvent;
    scope.hideEvent = hideEvent;
    scope.showImage = showImage;
    scope.hideImage = hideImage;

    activate();

    function activate() {
      initYearsAvailable();
      initRooms(function() {
        getMonthsInYear(function() {
          getDaysInMonth();
        });
      });
    }

    function initRooms(cb) {
      RoomService.getAll(function(rooms) {
        scope.rooms = rooms;
        scope.selectedRoom = rooms[0];
        if (cb) { cb(); }
      });
    }

    function initYearsAvailable(cb) {
      var start = moment().year();
      var end = start + 4;
      scope.year = start;
      for (start; start <= end; start++) {
        scope.years.push(start);
      }
      if (cb) { cb(); }
    }

    function getMonthsInYear(cb) {
      CalendarService.getMonthsInYear(function(months) {
        scope.monthsInYear = months;
        // set selected month to current month using moment.js
        scope.month = moment().month();
        if (cb) { cb(); }
      });
    }

    function getDaysInMonth(cb) {
      scope.month = parseInt(scope.month);
      CalendarService.getDaysInMonth(
        scope.month,
        scope.year,
        scope.selectedRoom.id,
        function(days) {
          scope.daysInMonth = days;
          if (cb) { cb(); }
        }
      );
    }

    function showEvent(event) {
      scope.selectedEvent = event;
      //angular.element('#event-dialog').addClass('is-active');
      $('#event-dialog').addClass('is-active');
    }

    function hideEvent() {
      scope.selectedEvent = {};
      //angular.element('#event-dialog').removeClass('is-active');
      $('#event-dialog').removeClass('is-active');
    }

    function showImage(room) {
      scope.largeImageUrl = scope.imageApi + room.image;
      //angular.element('#image-dialog').addClass('is-active');
      $('#image-dialog').addClass('is-active');
    }

    function hideImage() {
      //scope.selectedRoom = {};
      //angular.element('#image-dialog').removeClass('is-active');
      $('#image-dialog').removeClass('is-active');
    }

    function selectRoom(event, room) {
      angular
        .element(event.target.parentNode.children)
        .removeClass('active');
      //angular.element(event.target).addClass('active');
      $(event.target).addClass('active');
      scope.selectedRoom = room;
      getDaysInMonth();
    }

    /*
     * Sets the current month to the previous or next month,
     * depending on the value of direction.
     */
    function updateMonth(direction) {
      var mom = moment({year: scope.year, month: scope.month});
      if (momentIsInRange(mom, direction)) {
        if (direction < 0) {
          mom.subtract(1, 'month');
        } else {
          mom.add(1, 'month');
        }
        scope.month = parseInt(mom.month());
        scope.year = parseInt(mom.year());;
        scope.getDaysInMonth();
      } else {
        console.log('year out of range');
      }
    }

    function momentIsInRange(mom, direction) {
      var result = true;
      if (direction < 0) {
        if (mom.year() === scope.years[0] &&
          mom.month() === 0) result = false;
      } else {
        if (mom.year() === scope.years[4] &&
          mom.month() === 11) result = false;
      }
      return result;
    }
  }
}

function CutFilter() {
  return function (value, wordwise, max, tail) {
    if (!value) return '';

    max = parseInt(max, 10);
    if (!max) return value;
    if (value.length <= max) return value;

    value = value.substr(0, max);
    if (wordwise) {
      var lastspace = value.lastIndexOf(' ');
      if (lastspace != -1) {
        //Also remove . and , so its gives a cleaner result.
        if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
          lastspace = lastspace - 1;
        }
        value = value.substr(0, lastspace);
      }
    }

    return value + (tail || ' …');
  };
}


})();