/*
 * Version: 05 OCT 2016
 */
(function() {

angular
.module('fa-ler-public')
.directive('faEventList', FAEventList);

FAEventList.$inject = [
  '$document',
  'API_PREFIX',
  'CalendarService'
];

function FAEventList($document, API_PREFIX, CalendarService) {
  return {
    templateUrl: 'public/app/components/eventlist/eventlist.tpl.html',
    scope: {},
    restrict: 'E',
    link: link
  };

  function link(scope, element, attrs) {

    scope.events = [];

    activate();

    function activate() {
      CalendarService.getAllEvents(function(events) {
        var upcomingEvents = events.filter(event =>
            parseInt(event.timeBegin) >= Date.now()
        );
        scope.events = upcomingEvents;
      });
    }

  }
}


})();