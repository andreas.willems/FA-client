(function() {

angular.module('fa.public.main.banner', []);

angular.module('fa.public.main.banner').directive('faMainBanner', FaMainBanner);

FaMainBanner.$inject = [];

function FaMainBanner() {
  return {
    templateUrl: 'public/app/components/main-banner/main-banner.component.html',
    restrict: 'E',
    scope: {},
    link: link
  };

  function link(scope, element, attrs) {}
}

})();