(function() {

angular.module('fa.public.main.footer', []);

angular.module('fa.public.main.footer').directive('faMainFooter', FaMainFooter);

FaMainFooter.$inject = [];

function FaMainFooter() {
  return {
    templateUrl: 'public/app/components/main-footer/main-footer.component.html',
    restrict: 'E',
    scope: {},
    link: link
  };

  function link(scope, element, attrs) {
    scope.showImprint = showImprint;
    scope.hideImprint = hideImprint;

    function showImprint(event) {
      event.preventDefault();
      $('.modal').addClass('is-active');
    }

    function hideImprint() {
      $('.modal').removeClass('is-active');
    }
  }
}

})();