(function() {

angular.module('fa.public.main.header', []);

angular.module('fa.public.main.header').directive('faMainHeader', FaMainHeader);

FaMainHeader.$inject = ['$window'];

function FaMainHeader($window) {
  return {
    templateUrl: 'public/app/components/main-header/main-header.component.html',
    restrict: 'E',
    scope: {},
    link: link
  };

  function link(scope, element, attrs) {
    scope.itemsInQuickAccess = [
      {
        text: 'Alte Seite',
        url: '...',
        target: '_blank',
        icon: 'fa-step-backward'
      },
      {
        text: 'Eilmitteilungen',
        url:'...',
        target: '_blank',
        icon: 'fa-clock-o'
      },
      {
        text: 'Geschäftsverteilungsplan',
        url:'...',
        target: '_blank',
        icon: 'fa-file-pdf-o'
      },
      {
        text: 'Kalender',
        url:'...',
        target: '_self',
        icon: 'fa-calendar'
      },
      {
        text: 'Mitarbeitersuche',
        url: '...',
        target: '_blank',
        icon: 'fa-users'
      },
      {
        text: 'Service Desk',
        url:'...',
        target: '_blank',
        icon: 'fa-medkit'
      },
      {
        text: 'Telefonverzeichnis',
        url: '...',
        target: '_blank',
        icon: 'fa-phone'
      }
    ];
    scope.selectedItem = scope.itemsInQuickAccess[0];
    scope.quicklinksVisible = false;
    scope.toggleQuicklinks = toggleQuicklinks;
    scope.hideQuicklinksWithDelay = hideQuicklinksWithDelay;


    function toggleQuicklinks() {
      scope.quicklinksVisible = !scope.quicklinksVisible;
    };

    function hideQuicklinksWithDelay() {
      $window.setTimeout(function() {
        scope.quicklinksVisible = false;
        scope.$digest();
      }, 1000);
    }

  }
}

})();