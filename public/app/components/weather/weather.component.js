(function() {

angular.module('fa.public.weather', []);

angular.module('fa.public.weather').directive('faWeatherApp', WeatherApp);

WeatherApp.$inject = ['$http', '$window'];

function WeatherApp($http, $window) {
  return {
    templateUrl: 'public/app/components/weather/weather.component.html',
    restrict: 'E',
    scope: {
      screenWideEnough: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.weather = {};
    var minWidth = 1340;
    if ($window.innerWidth >= minWidth) {
      //angular.element('#tomorrow').show();
      $('#tomorrow').show();
    } else {
      //angular.element('#tomorrow').hide();
      $('#tomorrow').hide();
    }

    //angular.element($window).resize(function() {
    $($window).resize(function() {
      if ($window.innerWidth >= minWidth) {
        //angular.element('#tomorrow').show();
        $('#tomorrow').show();
      } else {
        //angular.element('#tomorrow').hide();
        $('#tomorrow').hide();
      }
    });

    var weatherApi = 'http://api.openweathermap.org/data/2.5/forecast/daily'
      + '?id=2879697&appid=...'
      + '&units=metric&lang=de';
    $http.get(weatherApi).then(
      function onSuccess(response) {
        // console.log(response.data.list[0]);
        scope.weather.today = response.data.list[0];
        scope.weather.tomorrow = response.data.list[1];
      },
      function onError(response) {
        console.log('Weather request failed:');
        console.log(response);
        scope.weather = {};
    });
  }
}

})();