(function() {
angular
.module('fa-ler-public')
.controller('MainController', MainController);

MainController.$inject = [];

function MainController() {
  var vm = this;
  vm.showImprint = showImprint;
  vm.hideImprint = hideImprint;

  function showImprint(event) {
    event.preventDefault();
    angular.element('.modal').addClass('is-active');
  }

  function hideImprint() {
    angular.element('.modal').removeClass('is-active');
  }
}


})();