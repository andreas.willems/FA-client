(function() {
'use strict';
angular.module('fa-ler-public').config(Routes);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'SITE_ROOT'
];

function Routes($stateProvider, $urlRouterProvider, SITE_ROOT) {
  var clientPrefix = SITE_ROOT + '/client/public/app/';

  $stateProvider
    .state('fachinfo', {
      url: '/fachinfo',
      templateUrl: clientPrefix + 'pages/fachinfo/fachinfo.tpl.html'
    })
    .state('fachinfo.literatur', {
      url: '/literatur',
      templateUrl: clientPrefix + 'pages/fachinfo/partials/literatur.tpl.html'
    })
    .state('fachinfo.lohnsteuerhilfe', {
      url: '/lohnsteuerhilfe',
      templateUrl: clientPrefix + 'pages/fachinfo/partials/lohnsteuerhilfe.tpl.html'
    })
    .state('fachinfo.portale', {
      url: '/portale',
      templateUrl: clientPrefix + 'pages/fachinfo/partials/portale.tpl.html'
    })
    .state('fachinfo.steuerberater', {
      url: '/steuerberater',
      templateUrl: clientPrefix + 'pages/fachinfo/partials/steuerberater.tpl.html'
    })
    .state('fachinfo.veranlagung', {
      url: '/veranlagungshilfen',
      templateUrl: clientPrefix + 'pages/fachinfo/partials/veranlagungshilfen.tpl.html'
    })
  ;
}

})();