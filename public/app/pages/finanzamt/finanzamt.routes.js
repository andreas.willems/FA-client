(function() {
'use strict';
angular.module('fa-ler-public').config(Routes);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'SITE_ROOT'
];

function Routes($stateProvider,
                $urlRouterProvider,
                SITE_ROOT) {
  var clientPrefix = SITE_ROOT + '/client/public/app/';

  $stateProvider
    .state('finanzamt', {
      url: '/finanzamt',
      templateUrl: clientPrefix + 'pages/finanzamt/finanzamt.tpl.html'
    })
    .state('finanzamt.anfahrt', {
      url: '/anfahrt',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/anfahrt.tpl.html'
    })
    .state('finanzamt.ansprechpartner', {
      url: '/ansprechpartner',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/ansprechpartner.tpl.html'
    })
    .state('finanzamt.verfuegungen', {
      url: '/amtsverfuegungen',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/amtsverfuegungen/amtsverfuegungen.tpl.html',
      controller: 'DecreeController as vm'
    })
    .state('finanzamt.ausbildungsplaene_anwaerter', {
      url: '/ausbildungsplaene-anwaerter',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/ausbildungsplaene_anwaerter.tpl.html'
    })
    .state('finanzamt.ausbildungsplaene_praktikanten', {
      url: '/ausbildungsplaene-praktikanten',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/ausbildungsplaene_praktikanten.tpl.html'
    })
    .state('finanzamt.edvteam', {
      url: '/edvteam',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/edvteam.tpl.html'
    })
    .state('finanzamt.geschichte', {
      url: '/geschichte',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/geschichte.tpl.html'
    })
    .state('finanzamt.gesundheit', {
      url: '/gesundheit',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/gesundheit.tpl.html'
    })
    .state('finanzamt.gleichstellung', {
      url: '/gleichstellung',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/gleichstellung.tpl.html'
    })
    .state('finanzamt.guteshaus', {
      url: '/gutesfuershaus',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/gutesfuershaus.tpl.html'
    })
    .state('finanzamt.reinigungsplan', {
      url: '/reinigungsplan',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/reinigungsplan.tpl.html'
    })
    .state('finanzamt.sport', {
      url: '/sport',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/sport.tpl.html'
    })
    .state('finanzamt.vorsteher', {
      url: '/vorsteher',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/vorsteher/vorsteher.tpl.html',
      controller: 'DeanController as vm'
    })
  ;
}

})();