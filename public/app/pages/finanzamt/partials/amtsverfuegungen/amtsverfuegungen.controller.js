
/* bof */
(function() {
  'use strict';

  angular.module('fa-ler-public').controller('DecreeController', DecreeController);

  DecreeController.$inject = ['$http'];

  function DecreeController ($http) {
    var vm = this;
    vm.fileBase = 'http://lers051.ofd-h.de/fa/intern/archiv/Amtsverfuegungen/';
    vm.decrees = [];

    activate();

    function activate() {
      // fetchDecrees();
    }

    function fetchDecrees () {
      var uri = 'http://lers051.ofd-h.de/fa/ARCHIVE/index.php/api/root/Amtsverfuegungen/2017';
      var req = {
        method: 'GET',
        url: uri
      };
      $http(req)
        .then(function(response) {
          var decreeNames = response.data;
          var extendedNames = decreeNames.map(function(item) {
            return vm.fileBase + item;
          });
          vm.decrees = extendedNames;
        })
        .catch(function(error) {
          console.error(error);
        });
    }
  }


})();

/* eof */

