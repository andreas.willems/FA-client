
/* bof */
(function() {
'use strict';

angular.module('fa-ler-public').controller('DeanController', DeanController);

DeanController.$inject = [];

function DeanController() {
  var vm = this;
  vm.imageApi = 'http://lers051/fa/AW/client/public/app/pages/finanzamt/' +
    'partials/vorsteher/';
  vm.deans = [
    {
      title: 'Geh. RR',
      name: 'Wilhelm Sperling',
      yearsActive: '1920 - 1933',
      dobirth: '16.12.1867',
      dodeath: '09.02.1933',
      imageId: 'sperling.jpg'
    }, {
      title: 'RR',
      name: 'Werner Reinsch',
      yearsActive: '1933 - 1939',
      dobirth: '18.01.1892',
      dodeath: '09.11.1955',
      imageId: 'reinsch.jpg'
    }, {
      title: 'RR',
      name: 'Heinrich Volmer',
      yearsActive: '1939 - 1944',
      dobirth: '12.04.1902',
      dodeath: '05.07.1944',
      imageId: 'volmer.jpg'
    }, {
      title: 'RR',
      name: 'Rolf Lindemann',
      yearsActive: '1944 - 1945',
      dobirth: '27.10.1910',
      dodeath: '13.09.1988',
      imageId: 'lindemann.jpg'
    }, {
      title: 'StA',
      name: 'Theodor Schlichting',
      yearsActive: '1945 - 1948',
      dobirth: '06.06.1892',
      dodeath: '19.10.1975',
      imageId: 'schlichting.jpg'
    }, {
      title: 'ORR',
      name: 'Hermann Brinkmann',
      yearsActive: '1948 - 1953',
      dobirth: '28.09.1888',
      dodeath: '24.05.1978',
      imageId: 'brinkmann.jpg'
    }, {
      title: 'RD',
      name: 'Werner Akva',
      yearsActive: '1953 - 1962',
      dobirth: '17.10.1904',
      dodeath: '30.09.1990',
      imageId: 'akva.jpg'
    }, {
      title: 'RD',
      name: 'Hans Knade',
      yearsActive: '1962 - 1973',
      dobirth: '26.09.1908',
      dodeath: '20.09.1996',
      imageId: 'knade.jpg'
    }, {
      title: 'RD',
      name: 'Wilhelm Siekmann',
      yearsActive: '1973 - 1977',
      dobirth: '16.09.1935',
      dodeath: '',
      imageId: 'siekmann.jpg'
    }, {
      title: 'RD',
      name: 'Karl Schraplau',
      yearsActive: '1977 - 2002',
      dobirth: '24.03.1940',
      dodeath: '',
      imageId: 'schraplau.jpg'
    }, {
      title: 'LRD',
      name: 'Eckehard Lamberts',
      yearsActive: 'seit 2003',
      dobirth: '18.06.1952',
      dodeath: '',
      imageId: 'lamberts.jpg'
    },
  ];

  activate();

  function activate() {
    //
  }
}


})();

/* eof */
