(function() {
angular
.module('fa-ler-public')
.controller('HomeController', HomeController);

HomeController.$inject = [
  'API_PREFIX',
  'Articles',
  'ArticleLinks',
  'Links'
];

function HomeController(API_PREFIX,
                        Articles,
                        ArticleLinks,
                        Links) {
  var vm = this;
  vm.protocols = [];
  vm.news = [];

  activate();

  function activate() {
    Articles.getLastTenProtocols(handleProtocols);
    Articles.getLastTenNews(handleNews);
  }

  function handleProtocols(protocols) {
    //console.log(protocols);
    if (protocols && protocols.length > 0) {
      vm.protocols = protocols;
      // console.log(vm.protocols);
    } else {
      vm.protocols = [];
    }
  }

  function handleNews(news) {
    if (news && news.length > 0) {
      vm.news = news;
      //vm.news.forEach(getLinksForArticle);
    } else {
      vm.news = [];
    }
  }

  function getLinksForArticle(article) {
    //console.log('fetching links for article...');
    //console.log(article);
    var newLinks = [];
    ArticleLinks.getLinksForArticle(article.id, function(links) {
      //console.log(links);
      links.forEach(function(link) {
        Links.getById(link.id, function(result) {
          newLinks.push(result);
        });
      });
      article.links = newLinks;
      //console.log(article);
    });
  }
}


})();