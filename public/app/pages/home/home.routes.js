(function() {
'use strict';
angular.module('fa-ler-public').config(Routes);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'SITE_ROOT'
];

function Routes($stateProvider, $urlRouterProvider, SITE_ROOT) {
  var clientPrefix = SITE_ROOT + '/client/public/app/';

  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: clientPrefix + 'pages/home/home.tpl.html',
      controller: 'HomeController as vm'
    })
  ;
}

})();