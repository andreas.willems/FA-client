(function() {
'use strict';
angular.module('fa-ler-public').config(Routes);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'SITE_ROOT'
];

function Routes($stateProvider, $urlRouterProvider, SITE_ROOT) {
  var clientPrefix = SITE_ROOT + '/client/public/app/';

  $stateProvider
    .state('links', {
      url: '/links',
      templateUrl: clientPrefix + 'pages/links/links.tpl.html'
    })
    .state('links.anleitungen', {
      url: '/anleitungen',
      templateUrl: clientPrefix + 'pages/links/partials/anleitungen.tpl.html'
    })
    .state('links.behoerden', {
      url: '/behoerden',
      templateUrl: clientPrefix + 'pages/links/partials/behoerden.tpl.html'
    })
    .state('links.ernie', {
      url: '/ernie',
      templateUrl: clientPrefix + 'pages/links/partials/ernie.tpl.html'
    })
    .state('links.firmensuche', {
      url: '/firmensuche',
      templateUrl: clientPrefix + 'pages/links/partials/firmensuche.tpl.html'
    })
    .state('links.formulare', {
      url: '/formulare',
      templateUrl: clientPrefix + 'pages/links/partials/formulare.tpl.html'
    })
    .state('links.kantine', {
      url: '/kantine',
      templateUrl: clientPrefix + 'pages/links/partials/kantine.tpl.html'
    })
    .state('links.markt', {
      url: '/markt',
      templateUrl: clientPrefix + 'pages/links/partials/markt.tpl.html'
    })
    .state('links.employeesearch', {
      url: '/mitarbeitersuche',
      templateUrl: clientPrefix + 'pages/links/partials/mitarbeitersuche.tpl.html'
    })
    .state('links.pmv', {
      url: '/pmv',
      templateUrl: clientPrefix + 'pages/links/partials/pmv.tpl.html'
    })
    .state('links.phone', {
      url: '/telefon',
      templateUrl: clientPrefix + 'pages/links/partials/telefon.tpl.html'
    })
    .state('links.zeus', {
      url: '/zeus',
      templateUrl: clientPrefix + 'pages/links/partials/zeus.tpl.html'
    })
  ;
}

})();