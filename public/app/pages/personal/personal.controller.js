
/* bof */
(function() {

var moduleName = 'fa-ler-public';

angular
.module(moduleName)
.controller('AspirantsController', AspirantsController);

angular
.module(moduleName)
.controller('PersonalController', PersonalController);

angular
.module(moduleName)
.controller('DepartmentController', DepartmentController);


// ######## Dependency Injection ########

AspirantsController.$inject = [
  'API_PREFIX',
  'Aspirants'
];

PersonalController.$inject = [
  'API_PREFIX',
  'Departments'
];

DepartmentController.$inject = [
  '$stateParams',
  'API_PREFIX',
  'Articles',
  'ArticleLinks',
  'Departments',
  'Links'
];

// ######## Controller definitions ########

function AspirantsController(API_PREFIX, Aspirants) {
  var vm = this;
  vm.aspirants = [];
  vm.imageApi = API_PREFIX + '/images/';

  activate();

  function activate() {
    Aspirants.getAll(handleAspirants);
  }

  function handleAspirants(aspirants) {
    vm.aspirants = aspirants;
  }

}

function PersonalController(API_PREFIX, Departments) {
  var vm = this;
  vm.departments = [];
  vm.imageApi = API_PREFIX + '/images/';

  activate();

  function activate() {
    Departments.getAll(handleDepartments);
  }

  function handleDepartments(departments) {
    vm.departments = departments;
  }

}

function DepartmentController($stateParams,
                              API_PREFIX,
                              Articles,
                              ArticleLinks,
                              Departments,
                              Links) {
  var vm = this;
  vm.imageApi = API_PREFIX + '/images/';
  //vm.fileApi = API_PREFIX + '/files/';
  vm.department = {
    id: '',
    numeral: '',
    imageId: ''
  };
  vm.protocols = [];

  activate();

  function activate() {
    Departments.getWithId($stateParams.id, handleDepartment);
  }

  function handleDepartment(department) {
    vm.department = department;
    Articles.getProtocolsForDepartment(vm.department.id, handleProtocols);
  }

  function handleProtocols(protocols) {
    vm.protocols = protocols;
  }
}

})();
/* eof */
