(function() {
'use strict';
angular.module('fa-ler-public').config(Routes);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'SITE_ROOT'
];

function Routes($stateProvider, $urlRouterProvider, SITE_ROOT) {
  var clientPrefix = SITE_ROOT + '/client/public/app/';

  $stateProvider
    .state('personal', {
      url: '/personal',
      templateUrl: clientPrefix + 'pages/personal/personal.tpl.html',
      controller: 'PersonalController as vm'
    })
    .state('personal.sachgebiet', {
      url: '/sachgebiet/:id',
      views: {
        'detail': {
          templateUrl: clientPrefix + 'pages/personal/partials/sachgebiet.html',
          controller: 'DepartmentController as vm'
        }
      }
    })
    .state('personal.anwaerter', {
      url: '/anwaerter',
      views: {
        'detail': {
          templateUrl: clientPrefix + 'pages/personal/partials/anwaerter.html',
          controller: 'AspirantsController as vm'
        }
      }
    })
  ;
}

})();