(function() {

angular
.module('fa-ler-public')
.controller('RoomsController', RoomsController);

RoomsController.$inject = [
  'RoomsPageService'
];

function RoomsController(RoomsPageService) {
  var vm = this;
  vm.events = [];

  activate();

  function activate() {
    initEventList(function() {});
  }

  function initEventList(cb) {
    RoomsPageService.getAllEvents(function(events) {
      vm.events = events;
      if (cb) { cb(); }
    });
  }
}

})();