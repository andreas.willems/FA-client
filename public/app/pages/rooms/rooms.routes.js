(function() {
'use strict';
angular.module('fa-ler-public').config(Routes);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'SITE_ROOT'
];

function Routes($stateProvider, $urlRouterProvider, SITE_ROOT) {
  var clientPrefix = SITE_ROOT + '/client/public/app/';

  $stateProvider
    .state('rooms', {
      url: '/rooms',
      abstract: true,
      templateUrl: clientPrefix + 'pages/rooms/rooms.tpl.html',
      controller: 'RoomsController as vm'
    })
    .state('rooms.calendar', {
      parent: 'rooms',
      url: '/calendar',
      templateUrl: clientPrefix + 'pages/rooms/partials/calendar.tpl.html',
      controller: 'RoomsController as vm'
    })
    .state('rooms.list', {
      parent: 'rooms',
      url: '/list',
      templateUrl: clientPrefix + 'pages/rooms/partials/list.tpl.html',
      controller: 'RoomsController as vm'
    })
  ;
}

})();