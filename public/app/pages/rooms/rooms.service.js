(function() {

angular
.module('fa-ler-public')
.factory('RoomsPageService', RoomsPageService);

RoomsPageService.$inject = [
  'EventService',
  'RoomService'
];

function RoomsPageService(EventService, RoomService) {

  var model = this;
  model.rooms = [];
  model.events = [];

  return {
    getAllEvents: getAllEvents
  };

  function getAllEvents(cb) {
    RoomService.getAll(function(rooms) {
      model.rooms = rooms;
      EventService.getAll(function(events) {
        extendedEvents = addRoomNameToEvent(rooms, events);
        model.events = extendedEvents;
        cb(extendedEvents);
      });
    });
  }

  function addRoomNameToEvent(rooms, events) {
    events.forEach(function(event) {
      var roomId = event.roomId;
      for (var i = 0; i < rooms.length; i++) {
        if (rooms[i].id === roomId) {
          event.roomName = rooms[i].name;
          break;
        }
      }
    });
    return events;
  }
}

})();