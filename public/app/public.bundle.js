(function() {
'use strict';
angular.module('fa-ler-public', [
  'ngSanitize',
  'ui.router',
  'angular-loading-bar',
  'fa.shared.components',
  'fa.shared.constants',
  'fa.shared.services',
  'fa.public.main.header',
  'fa.public.main.banner',
  'fa.public.main.footer',
  'fa.public.weather'
]);
angular.module('fa-ler-public').config(Routes);
angular.module('fa-ler-public').run(RunBlock);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider'
];

RunBlock.$inject = [];

function Routes($stateProvider, $urlRouterProvider) {
  // For any unmatched url, redirect to /home
  $urlRouterProvider.otherwise('/home');
}

function RunBlock() {
  moment.locale('de');
}

})();
(function() {
angular
.module('fa-ler-public')
.controller('HomeController', HomeController);

HomeController.$inject = [
  'API_PREFIX',
  'Articles',
  'ArticleLinks',
  'Links'
];

function HomeController(API_PREFIX,
                        Articles,
                        ArticleLinks,
                        Links) {
  var vm = this;
  vm.protocols = [];
  vm.news = [];

  activate();

  function activate() {
    Articles.getLastTenProtocols(handleProtocols);
    Articles.getLastTenNews(handleNews);
  }

  function handleProtocols(protocols) {
    //console.log(protocols);
    if (protocols && protocols.length > 0) {
      vm.protocols = protocols;
      // console.log(vm.protocols);
    } else {
      vm.protocols = [];
    }
  }

  function handleNews(news) {
    if (news && news.length > 0) {
      vm.news = news;
      //vm.news.forEach(getLinksForArticle);
    } else {
      vm.news = [];
    }
  }

  function getLinksForArticle(article) {
    //console.log('fetching links for article...');
    //console.log(article);
    var newLinks = [];
    ArticleLinks.getLinksForArticle(article.id, function(links) {
      //console.log(links);
      links.forEach(function(link) {
        Links.getById(link.id, function(result) {
          newLinks.push(result);
        });
      });
      article.links = newLinks;
      //console.log(article);
    });
  }
}


})();(function() {
'use strict';
angular.module('fa-ler-public').config(Routes);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'SITE_ROOT'
];

function Routes($stateProvider, $urlRouterProvider, SITE_ROOT) {
  var clientPrefix = SITE_ROOT + '/client/public/app/';

  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: clientPrefix + 'pages/home/home.tpl.html',
      controller: 'HomeController as vm'
    })
  ;
}

})();(function() {
'use strict';
angular.module('fa-ler-public').config(Routes);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'SITE_ROOT'
];

function Routes($stateProvider, $urlRouterProvider, SITE_ROOT) {
  var clientPrefix = SITE_ROOT + '/client/public/app/';

  $stateProvider
    .state('personal', {
      url: '/personal',
      templateUrl: clientPrefix + 'pages/personal/personal.tpl.html',
      controller: 'PersonalController as vm'
    })
    .state('personal.sachgebiet', {
      url: '/sachgebiet/:id',
      views: {
        'detail': {
          templateUrl: clientPrefix + 'pages/personal/partials/sachgebiet.html',
          controller: 'DepartmentController as vm'
        }
      }
    })
    .state('personal.anwaerter', {
      url: '/anwaerter',
      views: {
        'detail': {
          templateUrl: clientPrefix + 'pages/personal/partials/anwaerter.html',
          controller: 'AspirantsController as vm'
        }
      }
    })
  ;
}

})();
/* bof */
(function() {

var moduleName = 'fa-ler-public';

angular
.module(moduleName)
.controller('AspirantsController', AspirantsController);

angular
.module(moduleName)
.controller('PersonalController', PersonalController);

angular
.module(moduleName)
.controller('DepartmentController', DepartmentController);


// ######## Dependency Injection ########

AspirantsController.$inject = [
  'API_PREFIX',
  'Aspirants'
];

PersonalController.$inject = [
  'API_PREFIX',
  'Departments'
];

DepartmentController.$inject = [
  '$stateParams',
  'API_PREFIX',
  'Articles',
  'ArticleLinks',
  'Departments',
  'Links'
];

// ######## Controller definitions ########

function AspirantsController(API_PREFIX, Aspirants) {
  var vm = this;
  vm.aspirants = [];
  vm.imageApi = API_PREFIX + '/images/';

  activate();

  function activate() {
    Aspirants.getAll(handleAspirants);
  }

  function handleAspirants(aspirants) {
    vm.aspirants = aspirants;
  }

}

function PersonalController(API_PREFIX, Departments) {
  var vm = this;
  vm.departments = [];
  vm.imageApi = API_PREFIX + '/images/';

  activate();

  function activate() {
    Departments.getAll(handleDepartments);
  }

  function handleDepartments(departments) {
    vm.departments = departments;
  }

}

function DepartmentController($stateParams,
                              API_PREFIX,
                              Articles,
                              ArticleLinks,
                              Departments,
                              Links) {
  var vm = this;
  vm.imageApi = API_PREFIX + '/images/';
  //vm.fileApi = API_PREFIX + '/files/';
  vm.department = {
    id: '',
    numeral: '',
    imageId: ''
  };
  vm.protocols = [];

  activate();

  function activate() {
    Departments.getWithId($stateParams.id, handleDepartment);
  }

  function handleDepartment(department) {
    vm.department = department;
    Articles.getProtocolsForDepartment(vm.department.id, handleProtocols);
  }

  function handleProtocols(protocols) {
    vm.protocols = protocols;
  }
}

})();
/* eof */
(function() {

angular
.module('fa-ler-public')
.factory('RoomsPageService', RoomsPageService);

RoomsPageService.$inject = [
  'EventService',
  'RoomService'
];

function RoomsPageService(EventService, RoomService) {

  var model = this;
  model.rooms = [];
  model.events = [];

  return {
    getAllEvents: getAllEvents
  };

  function getAllEvents(cb) {
    RoomService.getAll(function(rooms) {
      model.rooms = rooms;
      EventService.getAll(function(events) {
        extendedEvents = addRoomNameToEvent(rooms, events);
        model.events = extendedEvents;
        cb(extendedEvents);
      });
    });
  }

  function addRoomNameToEvent(rooms, events) {
    events.forEach(function(event) {
      var roomId = event.roomId;
      for (var i = 0; i < rooms.length; i++) {
        if (rooms[i].id === roomId) {
          event.roomName = rooms[i].name;
          break;
        }
      }
    });
    return events;
  }
}

})();(function() {
'use strict';
angular.module('fa-ler-public').config(Routes);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'SITE_ROOT'
];

function Routes($stateProvider, $urlRouterProvider, SITE_ROOT) {
  var clientPrefix = SITE_ROOT + '/client/public/app/';

  $stateProvider
    .state('rooms', {
      url: '/rooms',
      abstract: true,
      templateUrl: clientPrefix + 'pages/rooms/rooms.tpl.html',
      controller: 'RoomsController as vm'
    })
    .state('rooms.calendar', {
      parent: 'rooms',
      url: '/calendar',
      templateUrl: clientPrefix + 'pages/rooms/partials/calendar.tpl.html',
      controller: 'RoomsController as vm'
    })
    .state('rooms.list', {
      parent: 'rooms',
      url: '/list',
      templateUrl: clientPrefix + 'pages/rooms/partials/list.tpl.html',
      controller: 'RoomsController as vm'
    })
  ;
}

})();(function() {

angular
.module('fa-ler-public')
.controller('RoomsController', RoomsController);

RoomsController.$inject = [
  'RoomsPageService'
];

function RoomsController(RoomsPageService) {
  var vm = this;
  vm.events = [];

  activate();

  function activate() {
    initEventList(function() {});
  }

  function initEventList(cb) {
    RoomsPageService.getAllEvents(function(events) {
      vm.events = events;
      if (cb) { cb(); }
    });
  }
}

})();
/* bof */
(function() {
'use strict';

angular.module('fa-ler-public').controller('DeanController', DeanController);

DeanController.$inject = [];

function DeanController() {
  var vm = this;
  vm.imageApi = 'http://lers051/fa/AW/client/public/app/pages/finanzamt/' +
    'partials/vorsteher/';
  vm.deans = [
    {
      title: 'Geh. RR',
      name: 'Wilhelm Sperling',
      yearsActive: '1920 - 1933',
      dobirth: '16.12.1867',
      dodeath: '09.02.1933',
      imageId: 'sperling.jpg'
    }, {
      title: 'RR',
      name: 'Werner Reinsch',
      yearsActive: '1933 - 1939',
      dobirth: '18.01.1892',
      dodeath: '09.11.1955',
      imageId: 'reinsch.jpg'
    }, {
      title: 'RR',
      name: 'Heinrich Volmer',
      yearsActive: '1939 - 1944',
      dobirth: '12.04.1902',
      dodeath: '05.07.1944',
      imageId: 'volmer.jpg'
    }, {
      title: 'RR',
      name: 'Rolf Lindemann',
      yearsActive: '1944 - 1945',
      dobirth: '27.10.1910',
      dodeath: '13.09.1988',
      imageId: 'lindemann.jpg'
    }, {
      title: 'StA',
      name: 'Theodor Schlichting',
      yearsActive: '1945 - 1948',
      dobirth: '06.06.1892',
      dodeath: '19.10.1975',
      imageId: 'schlichting.jpg'
    }, {
      title: 'ORR',
      name: 'Hermann Brinkmann',
      yearsActive: '1948 - 1953',
      dobirth: '28.09.1888',
      dodeath: '24.05.1978',
      imageId: 'brinkmann.jpg'
    }, {
      title: 'RD',
      name: 'Werner Akva',
      yearsActive: '1953 - 1962',
      dobirth: '17.10.1904',
      dodeath: '30.09.1990',
      imageId: 'akva.jpg'
    }, {
      title: 'RD',
      name: 'Hans Knade',
      yearsActive: '1962 - 1973',
      dobirth: '26.09.1908',
      dodeath: '20.09.1996',
      imageId: 'knade.jpg'
    }, {
      title: 'RD',
      name: 'Wilhelm Siekmann',
      yearsActive: '1973 - 1977',
      dobirth: '16.09.1935',
      dodeath: '',
      imageId: 'siekmann.jpg'
    }, {
      title: 'RD',
      name: 'Karl Schraplau',
      yearsActive: '1977 - 2002',
      dobirth: '24.03.1940',
      dodeath: '',
      imageId: 'schraplau.jpg'
    }, {
      title: 'LRD',
      name: 'Eckehard Lamberts',
      yearsActive: 'seit 2003',
      dobirth: '18.06.1952',
      dodeath: '',
      imageId: 'lamberts.jpg'
    },
  ];

  activate();

  function activate() {
    //
  }
}


})();

/* eof */

/* bof */
(function() {
  'use strict';

  angular.module('fa-ler-public').controller('DecreeController', DecreeController);

  DecreeController.$inject = ['$http'];

  function DecreeController ($http) {
    var vm = this;
    vm.fileBase = 'http://lers051.ofd-h.de/fa/intern/archiv/Amtsverfuegungen/';
    vm.decrees = [];

    activate();

    function activate() {
      // fetchDecrees();
    }

    function fetchDecrees () {
      var uri = 'http://lers051.ofd-h.de/fa/ARCHIVE/index.php/api/root/Amtsverfuegungen/2017';
      var req = {
        method: 'GET',
        url: uri
      };
      $http(req)
        .then(function(response) {
          var decreeNames = response.data;
          var extendedNames = decreeNames.map(function(item) {
            return vm.fileBase + item;
          });
          vm.decrees = extendedNames;
        })
        .catch(function(error) {
          console.error(error);
        });
    }
  }


})();

/* eof */

 
(function() {
'use strict';
angular.module('fa-ler-public').config(Routes);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'SITE_ROOT'
];

function Routes($stateProvider,
                $urlRouterProvider,
                SITE_ROOT) {
  var clientPrefix = SITE_ROOT + '/client/public/app/';

  $stateProvider
    .state('finanzamt', {
      url: '/finanzamt',
      templateUrl: clientPrefix + 'pages/finanzamt/finanzamt.tpl.html'
    })
    .state('finanzamt.anfahrt', {
      url: '/anfahrt',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/anfahrt.tpl.html'
    })
    .state('finanzamt.ansprechpartner', {
      url: '/ansprechpartner',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/ansprechpartner.tpl.html'
    })
    .state('finanzamt.verfuegungen', {
      url: '/amtsverfuegungen',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/amtsverfuegungen/amtsverfuegungen.tpl.html',
      controller: 'DecreeController as vm'
    })
    .state('finanzamt.ausbildungsplaene_anwaerter', {
      url: '/ausbildungsplaene-anwaerter',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/ausbildungsplaene_anwaerter.tpl.html'
    })
    .state('finanzamt.edvteam', {
      url: '/edvteam',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/edvteam.tpl.html'
    })
    .state('finanzamt.geschichte', {
      url: '/geschichte',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/geschichte.tpl.html'
    })
    .state('finanzamt.gesundheit', {
      url: '/gesundheit',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/gesundheit.tpl.html'
    })
    .state('finanzamt.gleichstellung', {
      url: '/gleichstellung',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/gleichstellung.tpl.html'
    })
    .state('finanzamt.guteshaus', {
      url: '/gutesfuershaus',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/gutesfuershaus.tpl.html'
    })
    .state('finanzamt.reinigungsplan', {
      url: '/reinigungsplan',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/reinigungsplan.tpl.html'
    })
    .state('finanzamt.sport', {
      url: '/sport',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/sport.tpl.html'
    })
    .state('finanzamt.vorsteher', {
      url: '/vorsteher',
      templateUrl: clientPrefix + 'pages/finanzamt/partials/vorsteher/vorsteher.tpl.html',
      controller: 'DeanController as vm'
    })
  ;
}

})();(function() {
'use strict';
angular.module('fa-ler-public').config(Routes);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'SITE_ROOT'
];

function Routes($stateProvider, $urlRouterProvider, SITE_ROOT) {
  var clientPrefix = SITE_ROOT + '/client/public/app/';

  $stateProvider
    .state('fachinfo', {
      url: '/fachinfo',
      templateUrl: clientPrefix + 'pages/fachinfo/fachinfo.tpl.html'
    })
    .state('fachinfo.literatur', {
      url: '/literatur',
      templateUrl: clientPrefix + 'pages/fachinfo/partials/literatur.tpl.html'
    })
    .state('fachinfo.lohnsteuerhilfe', {
      url: '/lohnsteuerhilfe',
      templateUrl: clientPrefix + 'pages/fachinfo/partials/lohnsteuerhilfe.tpl.html'
    })
    .state('fachinfo.portale', {
      url: '/portale',
      templateUrl: clientPrefix + 'pages/fachinfo/partials/portale.tpl.html'
    })
    .state('fachinfo.steuerberater', {
      url: '/steuerberater',
      templateUrl: clientPrefix + 'pages/fachinfo/partials/steuerberater.tpl.html'
    })
    .state('fachinfo.veranlagung', {
      url: '/veranlagungshilfen',
      templateUrl: clientPrefix + 'pages/fachinfo/partials/veranlagungshilfen.tpl.html'
    })
  ;
}

})();(function() {
'use strict';
angular.module('fa-ler-public').config(Routes);

Routes.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'SITE_ROOT'
];

function Routes($stateProvider, $urlRouterProvider, SITE_ROOT) {
  var clientPrefix = SITE_ROOT + '/client/public/app/';

  $stateProvider
    .state('links', {
      url: '/links',
      templateUrl: clientPrefix + 'pages/links/links.tpl.html'
    })
    .state('links.anleitungen', {
      url: '/anleitungen',
      templateUrl: clientPrefix + 'pages/links/partials/anleitungen.tpl.html'
    })
    .state('links.behoerden', {
      url: '/behoerden',
      templateUrl: clientPrefix + 'pages/links/partials/behoerden.tpl.html'
    })
    .state('links.ernie', {
      url: '/ernie',
      templateUrl: clientPrefix + 'pages/links/partials/ernie.tpl.html'
    })
    .state('links.firmensuche', {
      url: '/firmensuche',
      templateUrl: clientPrefix + 'pages/links/partials/firmensuche.tpl.html'
    })
    .state('links.formulare', {
      url: '/formulare',
      templateUrl: clientPrefix + 'pages/links/partials/formulare.tpl.html'
    })
    .state('links.kantine', {
      url: '/kantine',
      templateUrl: clientPrefix + 'pages/links/partials/kantine.tpl.html'
    })
    .state('links.markt', {
      url: '/markt',
      templateUrl: clientPrefix + 'pages/links/partials/markt.tpl.html'
    })
    .state('links.employeesearch', {
      url: '/mitarbeitersuche',
      templateUrl: clientPrefix + 'pages/links/partials/mitarbeitersuche.tpl.html'
    })
    .state('links.pmv', {
      url: '/pmv',
      templateUrl: clientPrefix + 'pages/links/partials/pmv.tpl.html'
    })
    .state('links.phone', {
      url: '/telefon',
      templateUrl: clientPrefix + 'pages/links/partials/telefon.tpl.html'
    })
    .state('links.zeus', {
      url: '/zeus',
      templateUrl: clientPrefix + 'pages/links/partials/zeus.tpl.html'
    })
  ;
}

})();(function() {
angular
.module('fa-ler-public')
.controller('MainController', MainController);

MainController.$inject = [];

function MainController() {
  var vm = this;
  vm.showImprint = showImprint;
  vm.hideImprint = hideImprint;

  function showImprint(event) {
    event.preventDefault();
    angular.element('.modal').addClass('is-active');
  }

  function hideImprint() {
    angular.element('.modal').removeClass('is-active');
  }
}


})();(function() {

angular
.module('fa-ler-public')
.directive('faCalendar', FACalendar);

angular
.module('fa-ler-public')
.filter('cut', CutFilter);

FACalendar.$inject = [
  '$document',
  'API_PREFIX',
  'CalendarService',
  'RoomService'
];

function FACalendar($document, API_PREFIX, CalendarService, RoomService) {
  return {
    templateUrl: 'public/app/components/calendar/calendar.tpl.html',
    scope: {},
    restrict: 'E',
    link: link
  };

  function link(scope, element, attrs) {
    scope.rooms = [];
    scope.years = [];
    scope.monthsInYear = [];
    scope.daysInMonth = [];
    scope.weekdays = [
      'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag',
      'Freitag', 'Samstag', 'Sonntag'
    ];
    scope.year = '2016';
    scope.month = '1';
    scope.selectedEvent = {};
    scope.selectedRoom = {};

    scope.imageApi = API_PREFIX + '/images/';
    scope.largeImageUrl = 'http://placehold.it/400x225';
    scope.placeholderImage = 'http://placehold.it/400x225';

    scope.selectRoom = selectRoom;
    scope.updateMonth = updateMonth;
    scope.getDaysInMonth = getDaysInMonth;
    scope.showEvent = showEvent;
    scope.hideEvent = hideEvent;
    scope.showImage = showImage;
    scope.hideImage = hideImage;

    activate();

    function activate() {
      initYearsAvailable();
      initRooms(function() {
        getMonthsInYear(function() {
          getDaysInMonth();
        });
      });
    }

    function initRooms(cb) {
      RoomService.getAll(function(rooms) {
        scope.rooms = rooms;
        scope.selectedRoom = rooms[0];
        if (cb) { cb(); }
      });
    }

    function initYearsAvailable(cb) {
      var start = moment().year();
      var end = start + 4;
      scope.year = start;
      for (start; start <= end; start++) {
        scope.years.push(start);
      }
      if (cb) { cb(); }
    }

    function getMonthsInYear(cb) {
      CalendarService.getMonthsInYear(function(months) {
        scope.monthsInYear = months;
        // set selected month to current month using moment.js
        scope.month = moment().month();
        if (cb) { cb(); }
      });
    }

    function getDaysInMonth(cb) {
      scope.month = parseInt(scope.month);
      CalendarService.getDaysInMonth(
        scope.month,
        scope.year,
        scope.selectedRoom.id,
        function(days) {
          scope.daysInMonth = days;
          if (cb) { cb(); }
        }
      );
    }

    function showEvent(event) {
      scope.selectedEvent = event;
      //angular.element('#event-dialog').addClass('is-active');
      $('#event-dialog').addClass('is-active');
    }

    function hideEvent() {
      scope.selectedEvent = {};
      //angular.element('#event-dialog').removeClass('is-active');
      $('#event-dialog').removeClass('is-active');
    }

    function showImage(room) {
      scope.largeImageUrl = scope.imageApi + room.image;
      //angular.element('#image-dialog').addClass('is-active');
      $('#image-dialog').addClass('is-active');
    }

    function hideImage() {
      //scope.selectedRoom = {};
      //angular.element('#image-dialog').removeClass('is-active');
      $('#image-dialog').removeClass('is-active');
    }

    function selectRoom(event, room) {
      angular
        .element(event.target.parentNode.children)
        .removeClass('active');
      //angular.element(event.target).addClass('active');
      $(event.target).addClass('active');
      scope.selectedRoom = room;
      getDaysInMonth();
    }

    /*
     * Sets the current month to the previous or next month,
     * depending on the value of direction.
     */
    function updateMonth(direction) {
      var mom = moment({year: scope.year, month: scope.month});
      if (momentIsInRange(mom, direction)) {
        if (direction < 0) {
          mom.subtract(1, 'month');
        } else {
          mom.add(1, 'month');
        }
        scope.month = parseInt(mom.month());
        scope.year = parseInt(mom.year());;
        scope.getDaysInMonth();
      } else {
        console.log('year out of range');
      }
    }

    function momentIsInRange(mom, direction) {
      var result = true;
      if (direction < 0) {
        if (mom.year() === scope.years[0] &&
          mom.month() === 0) result = false;
      } else {
        if (mom.year() === scope.years[4] &&
          mom.month() === 11) result = false;
      }
      return result;
    }
  }
}

function CutFilter() {
  return function (value, wordwise, max, tail) {
    if (!value) return '';

    max = parseInt(max, 10);
    if (!max) return value;
    if (value.length <= max) return value;

    value = value.substr(0, max);
    if (wordwise) {
      var lastspace = value.lastIndexOf(' ');
      if (lastspace != -1) {
        //Also remove . and , so its gives a cleaner result.
        if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
          lastspace = lastspace - 1;
        }
        value = value.substr(0, lastspace);
      }
    }

    return value + (tail || ' …');
  };
}


})();(function() {

angular.module('fa.public.weather', []);

angular.module('fa.public.weather').directive('faWeatherApp', WeatherApp);

WeatherApp.$inject = ['$http', '$window'];

function WeatherApp($http, $window) {
  return {
    templateUrl: 'public/app/components/weather/weather.component.html',
    restrict: 'E',
    scope: {
      screenWideEnough: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.weather = {};
    var minWidth = 1340;
    if ($window.innerWidth >= minWidth) {
      //angular.element('#tomorrow').show();
      $('#tomorrow').show();
    } else {
      //angular.element('#tomorrow').hide();
      $('#tomorrow').hide();
    }

    //angular.element($window).resize(function() {
    $($window).resize(function() {
      if ($window.innerWidth >= minWidth) {
        //angular.element('#tomorrow').show();
        $('#tomorrow').show();
      } else {
        //angular.element('#tomorrow').hide();
        $('#tomorrow').hide();
      }
    });

    var weatherApi = 'http://api.openweathermap.org/data/2.5/forecast/daily'
      + '?id=2879697&appid=59d5136e267f5ca38816017235b52344'
      + '&units=metric&lang=de';
    $http.get(weatherApi).then(
      function onSuccess(response) {
        // console.log(response.data.list[0]);
        scope.weather.today = response.data.list[0];
        scope.weather.tomorrow = response.data.list[1];
      },
      function onError(response) {
        console.log('Weather request failed:');
        console.log(response);
        scope.weather = {};
    });
  }
}

})();(function() {

angular.module('fa.public.main.banner', []);

angular.module('fa.public.main.banner').directive('faMainBanner', FaMainBanner);

FaMainBanner.$inject = [];

function FaMainBanner() {
  return {
    templateUrl: 'public/app/components/main-banner/main-banner.component.html',
    restrict: 'E',
    scope: {},
    link: link
  };

  function link(scope, element, attrs) {}
}

})();(function() {

angular.module('fa.public.main.footer', []);

angular.module('fa.public.main.footer').directive('faMainFooter', FaMainFooter);

FaMainFooter.$inject = [];

function FaMainFooter() {
  return {
    templateUrl: 'public/app/components/main-footer/main-footer.component.html',
    restrict: 'E',
    scope: {},
    link: link
  };

  function link(scope, element, attrs) {
    scope.showImprint = showImprint;
    scope.hideImprint = hideImprint;

    function showImprint(event) {
      event.preventDefault();
      $('.modal').addClass('is-active');
    }

    function hideImprint() {
      $('.modal').removeClass('is-active');
    }
  }
}

})();(function() {

angular.module('fa.public.main.header', []);

angular.module('fa.public.main.header').directive('faMainHeader', FaMainHeader);

FaMainHeader.$inject = ['$window'];

function FaMainHeader($window) {
  return {
    templateUrl: 'public/app/components/main-header/main-header.component.html',
    restrict: 'E',
    scope: {},
    link: link
  };

  function link(scope, element, attrs) {
    scope.itemsInQuickAccess = [
      {
        text: 'Alte Seite',
        url: 'http://lers051.ofd-h.de/fa/alteseite',
        target: '_blank',
        icon: 'fa-step-backward'
      },
      {
        text: 'Eilmitteilungen',
        url:'http://rzhs057.ofd-h.de/eilmi/aktuell/index.shtml',
        target: '_blank',
        icon: 'fa-clock-o'
      },
      {
        text: 'Geschäftsverteilungsplan',
        url:'http://lers051.ofd-h.de/fa/static/personal/gvpl_aktuell.pdf',
        target: '_blank',
        icon: 'fa-file-pdf-o'
      },
      {
        text: 'Kalender',
        url:'http://lers051.ofd-h.de/fa/AW/#/rooms/calendar',
        target: '_self',
        icon: 'fa-calendar'
      },
      {
        text: 'Mitarbeitersuche',
        url: 'http://bucs051.ofd-h.de/fa/dp-suche.php',
        target: '_blank',
        icon: 'fa-users'
      },
      {
        text: 'Service Desk',
        url:'http://rzhs601.ofd-h.de/CAisd/pdmweb.exe?KEEP.is_comm_login=yes',
        target: '_blank',
        icon: 'fa-medkit'
      },
      {
        text: 'Telefonverzeichnis',
        url: 'http://lers051.ofd-h.de:20505/TELLIST',
        target: '_blank',
        icon: 'fa-phone'
      }
    ];
    scope.selectedItem = scope.itemsInQuickAccess[0];
    scope.quicklinksVisible = false;
    scope.toggleQuicklinks = toggleQuicklinks;
    scope.hideQuicklinksWithDelay = hideQuicklinksWithDelay;


    function toggleQuicklinks() {
      scope.quicklinksVisible = !scope.quicklinksVisible;
    };

    function hideQuicklinksWithDelay() {
      $window.setTimeout(function() {
        scope.quicklinksVisible = false;
        scope.$digest();
      }, 1000);
    }

  }
}

})();/*
 * Version: 05 OCT 2016
 */
(function() {

angular
.module('fa-ler-public')
.directive('faEventList', FAEventList);

FAEventList.$inject = [
  '$document',
  'API_PREFIX',
  'CalendarService'
];

function FAEventList($document, API_PREFIX, CalendarService) {
  return {
    templateUrl: 'public/app/components/eventlist/eventlist.tpl.html',
    scope: {},
    restrict: 'E',
    link: link
  };

  function link(scope, element, attrs) {

    scope.events = [];

    activate();

    function activate() {
      CalendarService.getAllEvents(function(events) {
        var upcomingEvents = events.filter(event =>
            parseInt(event.timeBegin) >= Date.now()
        );
        scope.events = upcomingEvents;
      });
    }

  }
}


})();(function() {
'use strict';

angular
.module('fa-ler-public')
.directive('faRssFeedReader', FaRssFeedReader);

FaRssFeedReader.$inject = ['$http'];

function FaRssFeedReader($http) {
  return {
    restrict: 'E',
    templateUrl: 'public/app/components/rss-feed-reader/rss-feed-reader.tpl.html',
    scope: {
      url: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.articles = [];
    var req = {
      method: 'POST',
      //url: scope.url
      url: 'http://rzhs091.ofd-h.de/steuerberater/stbk/index.php',
      data: 's_gname=aktiva'
    };
    $http(req).then(onSuccess, onError);

    function onSuccess(response) {
      console.log(response);
    }

    function onError(response) {
      console.log(response);
    }
  }
}

})(); 
