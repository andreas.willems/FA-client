#!/bin/bash

DIRECTORY="..."
OUTPUTMIN="$DIRECTORY"shared.bundle.js
CONSTANTS="$DIRECTORY"constants/shared.constants.js
COMPONENTS="$DIRECTORY"components/shared.components.js
SERVICES="$DIRECTORY"services/shared.services.js

function cleanBundleFile {
  if [ -f $OUTPUTMIN ]; then
    rm "$OUTPUTMIN"
  fi
}

function collectFileNames {
  files=$(find $DIRECTORY -name '*.js')
}

function concatExtraFiles {
  cat "$CONSTANTS" >> $OUTPUTMIN
  cat "$COMPONENTS" >> $OUTPUTMIN
  cat "$SERVICES" >> $OUTPUTMIN
}

function concatFiles {
  for f in $files
  do
    if [ $f == $CONSTANTS ]; then
      continue
    fi
    if [ $f == $COMPONENTS ]; then
      continue
    fi
    if [ $f == $SERVICES ]; then
      continue
    fi
    cat "$f" >> $OUTPUTMIN
  done
}

function printLineCount {
  lines=`wc -l "$OUTPUTMIN" | cut -f1 -d' '`
  echo "Wrote $lines lines to $OUTPUTMIN"
}

function changeAccessRights {
  chmod 644 "$OUTPUTMIN"
}

cleanBundleFile
collectFileNames
concatExtraFiles
concatFiles
changeAccessRights
printLineCount