(function() {
'use strict';

var moduleName = 'fa.shared.components';

angular
.module(moduleName)
.directive('faArticleBox', FaArticleBox);


/*angular.module(moduleName).component('faArticleBox', {
  templateUrl: 'shared/components/articlebox/articlebox.tpl.html',
  controller: ['API_PREFIX', ArticleBoxController],
  bindings: {
    article: '='
  }
});*/

FaArticleBox.$inject = [
  'API_PREFIX'
];

function FaArticleBox(API_PREFIX) {
  return {
    restrict: 'E',
    templateUrl: 'shared/components/articlebox/articlebox.tpl.html',
    scope: {
      article: '=',
      controls: '='
    },
    link: link
  };

  function link(scope, element, attrs) {
    scope.fileApi = API_PREFIX + '/files/';
    scope.showMore = false;
    scope.toggleMore = toggleMore;
    scope.toggleAppendices = toggleAppendices;
    scope.getURL = getURL;

    activate();

    function activate() {
      // console.log(scope.article);
    }

    function toggleMore() {
      scope.showMore = !scope.showMore;
    }

    function toggleAppendices() {
      scope.appendicesVisible = !scope.appendicesVisible;
    }

    function getURL(link) {
      if (link.path) return '.' + link.path;
      else return link.url;
    }
  }
}

function ArticleBoxController ($scope) {
  var ctrl = this;
  // ctrl.fileApi = API_PREFIX + '/files/';
  ctrl.showMore = false;
  ctrl.activate = function activate () {
    console.log(ctrl.article)
  }
  ctrl.toggleMore = function toggleMore() {
    ctrl.showMore = !ctrl.showMore;
  }
  ctrl.toggleAppendices = function toggleAppendices() {
    ctrl.appendicesVisible = !ctrl.appendicesVisible;
  }
}

})();