/* bof */

(function() {
'use strict';

var moduleName = 'fa.shared.components';

angular
.module(moduleName)
.directive('faArticleList', FaArticleList);

FaArticleList.$inject = [
  'API_PREFIX'
];

function FaArticleList(API_PREFIX) {
  return {
    restrict: 'E',
    templateUrl: 'shared/components/articlelist/articlelist.tpl.html',
    scope: {
      articles: '='
    }
  };
}

})();

/* eof */
