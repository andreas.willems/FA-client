(function() {
'use strict';

var moduleName = 'fa.shared.constants';
var siteRoot = '...';
angular.module(moduleName, []);

angular
.module(moduleName)
.constant('SITE_ROOT', siteRoot);

angular
.module(moduleName)
.constant('ADMIN_APP_PREFIX', 'admin/app/');

angular
.module(moduleName)
.constant('ROUTE_PREFIX', '/index.php/admin');

angular
.module(moduleName)
.constant('API_PREFIX', siteRoot + '/index.php/api');

})();