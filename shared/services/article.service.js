(function() {

'use strict';

angular
.module('fa.shared.services')
.factory('Articles', ArticlesService);

ArticlesService.$inject = [
  '$http',
  'API_PREFIX'
];

function ArticlesService($http, API_PREFIX) {
  return {
    getAll: getAll,
    getProtocols: getProtocols,
    getLastTenProtocols: getLastTenProtocols,
    getProtocolsForDepartment: getProtocolsForDepartment,
    getNews: getNews,
    getLastTenNews: getLastTenNews,
    getOthers: getOthers,
    create: create,
    read: read,
    update: update,
    remove: remove
  };

  function getAll(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/articles'
    };
    makeRequest(req, cb);
  }

  function getProtocols(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/articles/protocols'
    };
    makeRequest(req, cb);
  }

  function getLastTenProtocols(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/articles/protocols/lastten'
    };
    makeRequest(req, cb);
  }

  function getProtocolsForDepartment(departmentId, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/departments/'+ departmentId + '/protocols'
    };
    makeRequest(req, cb);
  }

  function getNews(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/articles/news'
    };
    makeRequest(req, cb);
  }

  function getLastTenNews(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/articles/news/lastten'
    };
    makeRequest(req, cb);
  }

  function getOthers(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/articles/noprotocols'
    };
    makeRequest(req, cb);
  }

  function create(article) {
    var data = JSON.stringify(article);
    console.log(data);
    var req = {
      //method: 'GET',
      method: 'POST',
      url: API_PREFIX + '/articles',
      data: 'articledata=' + data,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return $http(req);
  }

  function read(id, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/articles/' + id
    };
    makeRequest(req, cb);
  }

  function update(article, cb) {
    var data = JSON.stringify(article);
    var req = {
      method: 'PUT',
      url: API_PREFIX + '/articles/' + article.id,
      data: 'articledata=' + data,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return $http(req);
  }

  function remove(id, cb) {
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/articles/' + id
    };
    makeRequest(req, cb);
  }

  function makeRequest(req, cb) {
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
        cb([]);
      }
    );
  }

  function createParamString(article) {
    var params = 'title=' + article.title
      + '&type=' + article.type
      + '&shortText=' + article.shortText
      + '&longText=' + article.longText
      + '&departmentId=' + article.department.id
      + '&imageId=' + article.imageId;
    return params;
  }
}

})();