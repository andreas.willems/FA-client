(function() {

'use strict';

angular
.module('fa.shared.services')
.factory('ArticleFiles', ArticleFilesService);

ArticleFilesService.$inject = [
  '$http',
  'API_PREFIX'
];

function ArticleFilesService($http, API_PREFIX) {
  return {
    getFilesForArticle: getFilesForArticle,
    //getArticlesForFile: getArticlesForFile,
    appendFiles: appendFiles,
    create: create,
    read: read,
    update: update,
    remove: remove,
    removeArticle: removeArticle
  };

  function getFilesForArticle(articleId, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/articlefiles?article=' + articleId
    };
    makeRequest(req, cb);
  }

  function getArticlesForFile(fileId) {}

  function appendFiles(articleId, files, cb) {
    var results = [];
    files.forEach(function(file) {
      create(articleId, file.id, function(result) {
        console.log(result);
        results.push(result);
      });
    });
    cb(results);
  }

  function create(articleId, fileId, cb) {
    var paramString = createParamString({
      articleId: articleId,
      fileId: fileId
    });
    var req = {
      method: 'POST',
      url: API_PREFIX + '/articlefiles',
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    makeRequest(req, cb);
  }

  function read(id, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/articlefiles/' + id
    };
    makeRequest(req, cb);
  }

  function update(articleFile, cb) {
    var paramString = createParamString(articleFile);
    var req = {
      method: 'PUT',
      url: API_PREFIX + '/articlefiles/' + articleFile.id,
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    makeRequest(req, cb);
  }

  function remove(id, cb) {
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/articlefiles/' + id
    };
    makeRequest(req, cb);
  }

  function removeArticle(id, cb) {
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/articlefiles/article/' + id
    };
    makeRequest(req, cb);
  }

  function makeRequest(req, cb) {
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
        cb([]);
      }
    );
  }

  function createParamString(articleFile) {
    var params = 'articleId=' + articleFile.articleId
      + '&fileId=' + articleFile.fileId;
    return params;
  }
}

})();