(function() {

'use strict';

angular
.module('fa.shared.services')
.factory('ArticleLinks', ArticleLinksService);

ArticleLinksService.$inject = [
  '$http',
  'API_PREFIX'
];

function ArticleLinksService($http, API_PREFIX) {
  return {
    getLinksForArticle: getLinksForArticle,
    appendLinks: appendLinks,
    create: create,
    read: read,
    update: update,
    remove: remove,
    removeArticle: removeArticle
  };

  function getLinksForArticle(articleId) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/articlelinks?article=' + articleId
    };
    // makeRequest(req, cb);
    return $http(req);
  }

  // function getArticlesForFile(fileId) {}

  function appendLinks(articleId, links, cb) {
    /*console.log('append links');
    console.log(articleId);
    console.log(links);*/
    return new Promise(function(resolve, reject) {
      var results = [];
      links.forEach(function(link) {
        create(articleId, link.id, function(result) {
          results.push(result);
        });
      });
      if (results) {
        resolve(results);
      } else {
        reject(results);
      }
    });
  }

  function create(articleId, linkId, cb) {
    var paramString = createParamString({
      articleId: articleId,
      linkId: linkId
    });
    var req = {
      method: 'POST',
      url: API_PREFIX + '/articlelinks',
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    makeRequest(req, cb);
  }

  function read(id, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/articlelinks/' + id
    };
    makeRequest(req, cb);
  }

  function update(articleLink, cb) {
    var paramString = createParamString(articleLink);
    var req = {
      method: 'PUT',
      url: API_PREFIX + '/articlelinks/' + articleFile.id,
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    makeRequest(req, cb);
  }

  function remove(id, cb) {
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/articlelinks/' + id
    };
    makeRequest(req, cb);
  }

  function removeArticle(id, cb) {
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/articlelinks/article/' + id
    };
    makeRequest(req, cb);
  }

  function makeRequest(req, cb) {
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
        cb([]);
      }
    );
  }

  function createParamString(articleLink) {
    var params = 'articleId=' + articleLink.articleId
      + '&linkId=' + articleLink.linkId;
    return params;
  }
}

})();