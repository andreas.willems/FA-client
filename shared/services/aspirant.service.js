(function() {
'use strict';
angular
.module('fa.shared.services')
.factory('Aspirants', AspirantsService);

AspirantsService.$inject = [
  '$http',
  'API_PREFIX'
];

function AspirantsService($http, API_PREFIX) {
  var vm = this;

  return {
    getAll: getAll,
    getWithId: getWithId,
    create: create,
    remove: remove,
    update: update
  };

  function getAll(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/aspirants'
    };
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
        cb([]);
      });
  }

  function getWithId(id, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/aspirants/' + id
    };
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
        cb([]);
      });
  }

  function create(aspirant, callback) {
    var paramString = createParamString(aspirant);
    var req = {
      method: 'POST',
      url: API_PREFIX + '/aspirants',
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    console.log(req);
    $http(req).then(
      function onSuccess(response) {
        aspirant.id = response.data.id;
        callback(aspirant);
      },
      function onError(response) {
        console.log(response);
        callback();
      });
  }

  function remove(aspirant, callback) {
    var id = aspirant.id;
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/aspirants/' + id
    };
    $http(req).then(
      function onSuccess(response) {
        callback();
      },
      function onError(response) {
        console.log(response);
        callback();
      });
  }

  function update(aspirant, callback) {
    var id = aspirant.id;
    var paramString = createParamString(aspirant);
    var req = {
      method: 'PUT',
      url: API_PREFIX + '/aspirants/' + id,
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    $http(req).then(
      function onSuccess(response) {
        callback(response.data); // equals the number of rows changed, i.e. 1
      },
      function onError(response) {
        callback(response.data); // equals -1
      });
  }

  function createParamString(aspirant) {
    var params = 'name=' + aspirant.name
      + '&career=' + aspirant.career
      + '&year=' + aspirant.year
      + '&imageId=' + aspirant.imageId;
    return params;
  }
}

})();