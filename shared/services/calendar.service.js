(function() {
angular
.module('fa.shared.services')
.factory('CalendarService', CalendarService);

CalendarService.$inject = [
  'EventService',
  'RoomService'
];

function CalendarService(EventService, RoomService) {

  var model = this;
  model.rooms = [];
  model.events = [];

  return {
    getAllEvents: getAllEvents,
    getMonthsInYear: getMonthsInYear,
    getDaysInMonth: getDaysInMonth
  };

  function getAllEvents(cb) {
    RoomService.getAll(function(rooms) {
      model.rooms = rooms;
      EventService.getAll(function(events) {
        extendedEvents = addRoomNameToEvent(rooms, events);
        model.events = extendedEvents;
        cb(extendedEvents);
      });
    });
  }

  function addRoomNameToEvent(rooms, events) {
    events.forEach(function(event) {
      var roomId = event.roomId;
      for (var i = 0; i < rooms.length; i++) {
        if (rooms[i].id === roomId) {
          event.roomName = rooms[i].name;
          break;
        }
      }
    });
    return events;
  }

  function getMonthsInYear(cb) {
    var months = [
      {nr: 0, name: 'Januar'},
      {nr: 1, name: 'Februar'},
      {nr: 2, name: 'März'},
      {nr: 3, name: 'April'},
      {nr: 4, name: 'Mai'},
      {nr: 5, name: 'Juni'},
      {nr: 6, name: 'Juli'},
      {nr: 7, name: 'August'},
      {nr: 8, name: 'September'},
      {nr: 9, name: 'Oktober'},
      {nr: 10, name: 'November'},
      {nr: 11, name: 'Dezember'},
    ];
    cb(months);
  }

  function getDaysInMonth(month, year, roomId, cb) {
    var date = moment({year: year, month: month, date: 1});
    var daysInMonth = date.daysInMonth();
    var days = [];
    var events = [];
    var weekday = date.isoWeekday();

    EventService.getByMonthAndRoom(month, year, roomId, function(results) {
      events = results;
      // if first of month is not a monday
      if (weekday !== 1) {
        // add empty padding days
        for (var i = 1; i < weekday; i++) {
          days.push({nr: 0});
        }
      }

      for (var i = 1; i <= daysInMonth; i++) {
        var day = {};
        day.nr = i;
        day.events = [];
        events.forEach(function(event, index) {
          if (parseInt(event.day) === i) {
            day.events.push(event);
          }
        });
        days.push(day);
      }
      cb(days);
    });
  }
}


})();