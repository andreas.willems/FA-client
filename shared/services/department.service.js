(function() {
'use strict';
angular
.module('fa.shared.services')
.factory('Departments', DepartmentsService);

DepartmentsService.$inject = [
  '$http',
  'API_PREFIX'
];

function DepartmentsService($http, API_PREFIX) {
  var vm = this;

  return {
    getAll: getAll,
    getWithId: getWithId,
    create: create,
    remove: remove,
    update: update
  };

  function getAll(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/departments'
    };
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
        cb([]);
      });
  }

  function getWithId(id, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/departments/' + id
    };
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
        cb([]);
      });
  }

  function create(department, callback) {
    var paramString = createParamString(department);
    var req = {
      method: 'POST',
      url: API_PREFIX + '/departments',
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    $http(req).then(
      function onSuccess(response) {
        department.id = response.data.id;
        callback(department);
      },
      function onError(response) {
        console.log(response);
        callback();
      });
  }

  function remove(department, callback) {
    var id = department.id;
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/departments/' + id
    };
    $http(req).then(
      function onSuccess(response) {
        callback();
      },
      function onError(response) {
        console.log(response);
        callback();
      });
  }

  function update(department, callback) {
    var id = department.id;
    var paramString = createParamString(department);
    var req = {
      method: 'PUT',
      url: API_PREFIX + '/departments/' + id,
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    $http(req).then(
      function onSuccess(response) {
        callback(response.data); // equals the number of rows changed, i.e. 1
      },
      function onError(response) {
        callback(response.data); // equals -1
      });
  }

  function createParamString(department) {
    var params = 'numeral=' + department.numeral
      + '&name=' + department.name 
      + '&imageId=' + department.imageId;
    return params;
  }
}

})();