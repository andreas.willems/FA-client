(function() {

angular
.module('fa.shared.services')
.factory('EventService', EventService);

EventService.$inject = [
  '$http',
  'API_PREFIX'
];

function EventService($http, API_PREFIX) {
  var vm = this;
  vm.events = [];

  return {
    getAll: getAll,
    getById: getById,
    getByMonth: getByMonth,
    getByMonthAndRoom: getByMonthAndRoom,
    save: save,
    remove: remove,
    update: update
  };

  function getAll(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/events'
    };
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
        cb([]);
      });
  }

  function getById(id, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/events/' + id
    };
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
        cb([]);
      });
  }

  function getByMonth(month, year, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/events',
      params: {year: year, month: month + 1}
    };
    $http(req).then(
      function onSuccess(response) {
        vm.events = response.data;
        cb(vm.events);
      },
      function onError(response) {
        console.log(response);
        cb([]);
      });
  }

  function getByMonthAndRoom(month, year, room, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/events',
      params: {year: year, month: month + 1, room: room}
    };
    $http(req).then(
      function onSuccess(response) {
        vm.events = response.data;
        cb(vm.events);
      },
      function onError(response) {
        console.log(response);
        cb([]);
      });
  }

  function save(event, callback) {
    var paramString = createParamString(event);
    var req = {
      method: 'POST',
      url: API_PREFIX + '/events',
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    $http(req).then(
      function onSuccess(response) {
        event.id = response.data.id;
        vm.events.push(event);
        callback();
      },
      function onError(response) {
        console.log(response);
        callback();
      });
  }

  function remove(index, event, callback) {
    var id = event.ID || event.id;
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/events/' + id
    };
    $http(req).then(
      function onSuccess(response) {
        vm.events.splice(index, 1);
        callback();
      },
      function onError(response) {
        console.log(response);
      });
  }

  function update(event, callback) {
    var id = event.id;
    var paramString = createParamString(event);
    var req = {
      method: 'PUT',
      url: API_PREFIX + '/events/' + id,
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    $http(req).then(
      function onSuccess(response) {
        callback(response.data); // equals the number of rows changed, i.e. 1
      },
      function onError(response) {
        callback(response.data) // equals -1
      });
  }

  function createParamString(event) {
    var params = 'id=' + event.id
      + '&name=' + event.name 
      + '&contact=' + event.contact
      + '&date=' + event.date.valueOf()
      + '&timeBegin=' + event.timeBegin.valueOf()
      + '&timeEnd=' + event.timeEnd.valueOf()
      + '&day=' + event.day
      + '&month=' + event.month
      + '&year=' + event.year
      + '&roomId=' + event.roomId;
    return params;
  }
}

})();
 
