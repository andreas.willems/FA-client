(function() {
'use strict';

angular
.module('fa.shared.services')
.factory('Files', FilesService);

FilesService.$inject = [
  '$http',
  'API_PREFIX'
];

function FilesService($http, API_PREFIX) {
  var vm = this;
  //vm.files = [];

  return {
    getAll: getAll,
    getById: getById,
    remove: remove
  };

  function getAll(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/files'
    };
    makeRequest(req, cb);
  }

  function getById(fileId, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/files/' + fileId + '/data'
    };
    makeRequest(req, cb);
  }

  function remove(id, cb) {
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/files/' + id
    };
    makeRequest(req, cb);
  }

  function makeRequest(req, cb) {
    $http(req)
    .then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.loog(response);
        cb([]);
      }
    );
  }
}

})();