(function() {
'use strict';

angular
.module('fa.shared.services')
.factory('Images', ImagesService);

ImagesService.$inject = [
  '$http',
  'API_PREFIX'
];

function ImagesService($http, API_PREFIX) {
  var vm = this;
  //vm.files = [];

  return {
    getAll: getAll,
    getById: getById,
    remove: remove
  };

  function getAll(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/images'
    };
    makeRequest(req, cb);
  }

  function getById(imageId, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/images/' + imageId + '/data'
    };
    makeRequest(req, cb);
  }

  function remove(image, cb) {
    var id = image.id;
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/images/' + id
    };
    makeRequest(req, cb);
  }

  function makeRequest(req, cb) {
    $http(req)
    .then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
        cb([]);
      }
    );
  }
}

})();