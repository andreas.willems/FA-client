(function() {

angular
.module('fa.shared.services')
.factory('Links', LinksService);

LinksService.$inject = [
  '$http',
  'API_PREFIX'
];

function LinksService($http, API_PREFIX) {
  var vm = this;

  return {
    getAll: getAll,
    getById: getById,
    create: create,
    remove: remove,
    update: update
  };

  function getAll(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/links'
    };
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
      });
  }

  function getById(id, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/links/' + id
    };
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
        cb();
      }
    );
  }

  function create(link, callback) {
    var paramString = createParamString(link);
    var req = {
      method: 'POST',
      url: API_PREFIX + '/links',
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    $http(req).then(
      function onSuccess(response) {
        link.id = response.data.id;
        callback(link);
      },
      function onError(response) {
        console.log(response);
        callback();
      });
  }

  function remove(link, callback) {
    var id = link.id;
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/links/' + id
    };
    $http(req).then(
      function onSuccess(response) {
        callback();
      },
      function onError(response) {
        console.log(response);
        callback();
      }
    );
  }

  function update(link, callback) {
    var id = link.id;
    var paramString = createParamString(link);
    var req = {
      method: 'PUT',
      url: API_PREFIX + '/links/' + id,
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    $http(req).then(
      function onSuccess(response) {
        callback(response.data); // equals the number of rows changed, i.e. 1
      },
      function onError(response) {
        callback(response.data) // equals -1
      });
  }

  function createParamString(link) {
    var params = 'name=' + link.name 
      + '&url=' + link.url;
    return params;
  }
}

})();