(function() {

'use strict';

angular
.module('fa.shared.services')
.factory('Messages', MessageService);

MessageService.$inject = [
  '$rootScope'
];

function MessageService($rootScope) {
  return {
    broadcast: broadcast,
    listen : listen
  }

  function broadcast(message, data) {
    //console.log('broadcasting message: ' + message);
    $rootScope.$broadcast(message, data);
  }

  function listen(message, cb) {
    $rootScope.$on(message, function(event, data) {
      if (event) {
        //console.log('received message: ' + message);
        //console.log(event);
        cb(data);
      } else {
        cb({});
      }
    });
  }
}

})();