(function() {

angular
.module('fa.shared.services')
.factory('RoomService', RoomService);

RoomService.$inject = [
  '$http',
  'API_PREFIX'
];

function RoomService($http, API_PREFIX) {
  var vm = this;

  return {
    getAll: getAll,
    getById: getById,
    save: save,
    remove: remove,
    update: update
  };

  function getAll(cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/rooms'
    };
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
      });
  }

  function getById(id, cb) {
    var req = {
      method: 'GET',
      url: API_PREFIX + '/rooms/' + id
    };
    $http(req).then(
      function onSuccess(response) {
        cb(response.data);
      },
      function onError(response) {
        console.log(response);
        cb();
      }
    );
  }

  function save(room, callback) {
    var paramString = createParamString(room);
    var req = {
      method: 'POST',
      url: API_PREFIX + '/rooms',
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    $http(req).then(
      function onSuccess(response) {
        room.id = response.data.id;
        vm.rooms.push(room);
        callback();
      },
      function onError(response) {
        console.log(response);
        callback();
      });
  }

  function remove(index, room, callback) {
    var id = room.ID || room.id;
    var req = {
      method: 'DELETE',
      url: API_PREFIX + '/rooms/' + id
    };
    $http(req).then(
      function onSuccess(response) {
        vm.rooms.splice(index, 1);
        callback();
      },
      function onError(response) {
        console.log(response);
      });
  }

  function update(room, callback) {
    var id = room.id || room.ID;
    var paramString = createParamString(room);
    var req = {
      method: 'PUT',
      url: API_PREFIX + '/rooms/' + id,
      data: paramString,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    $http(req).then(
      function onSuccess(response) {
        callback(response.data); // equals the number of rows changed, i.e. 1
      },
      function onError(response) {
        callback(response.data) // equals -1
      });
  }

  function createParamString(room) {
    var params = 'name=' + room.name 
      + '&nr=' + room.nr 
      + '&building=' + room.building
      + '&equipment=' + room.equipment
      + '&image=' + room.image;
    return params;
  }
}

})();
