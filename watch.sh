#!/bin/bash

BUNDLE="$WEBSITE/client/runbundlers.sh"

if [ $# -lt 2 ]; then
  echo "Usage: watch <folder_to_watch> <bundle_file>"
  exit
fi

if [ -d $1 ]
then
    while true
    do
        # get timestamp of bundle file
        LTIME=`stat -c %Z "$2"`
         
        # iterate over files in directory
        for file in "$1"/*
        do
            # get timestamp of current file...
            ATIME=`stat -c %Z "$file"`
            # ...compare it to bundles timestamp
            if [ $ATIME -gt $LTIME ]
            then
                # update bundle file
                bash $BUNDLE
                echo "update"
            fi
        done
        sleep 1
    done

else
   echo $1" is not a directory"
fi
